#ifndef DUNE_TWISTPROVIDER_HH
#define DUNE_TWISTPROVIDER_HH

#include <iostream>
#include <vector>
#include <dune/common/exceptions.hh>
#include <dune/common/helpertemplates.hh>
#include <dune/grid/common/grid.hh>

namespace Dune
{

#include <dune/common/bartonnackmanifcheck.hh>

  /** @brief Twist provider %Interface base class.

  @ingroup twists
  */
  template<class GridImp, class Traits>
  struct TwistProvider
  {
    /** \brief The implementation type of the TwistProvider (BN) */
    typedef typename Traits::TwistProviderImp TwistProviderImp;

    /** \brief The type used for the indices */
    typedef typename Traits::IndexType IndexType;

    template <int c>
    struct Codim
    {
      typedef typename Traits::template Codim<c>::TwistGeometry TwistGeometry;
      typedef typename remove_const<GridImp>::type::
                       Traits::template Codim<c>::Entity Entity;
    };

    /** \brief dimension of the grid */
    static const int dimension = remove_const< GridImp >::type::dimension;

    /** @brief Map entity to a twist index.
        The result of calling this method with an entity for which
        the method contains returns false is undefined.

      \param e Reference to codim cc entity, where cc is the template parameter of the function.
      \return An index in the range 0 ... Max number of available twist geometries-1 for entities of codim cc entities.
    */
    template<int cc>
    IndexType index (const typename Codim<cc>::Entity& e) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().template index<cc>(e)));
      return asImp().template index<cc>(e);
    }
    /** @brief Map entity to index. Easier to use than the above because codimension template
      parameter need not be supplied explicitly.
      The result of calling this method with an entity for which
      the method contains returns false is undefined.

      \param e Reference to codim cc entity. Since
               entity knows its codimension, automatic extraction is possible.
      \return An index in the range 0 ... Max number of available twist geometries-1 for entities of the codim of e.
    */
    template<int cd, template<int,int,class> class EI>
    IndexType index (const Entity<cd,dimension,GridImp,EI>& e) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().template index<cd>(e)));
      return asImp().template index<cd>(e);
    }
    /** @brief Map subentity of codim cc of codim 0 entity to index.
      The result of calling this method with an entity for which
      the method contains returns false is undefined.

      \param[in]  e      reference to codim 0 entity
      \param[in]  i      number subentity of e within the codimension
      \param[in]  codim  codimension of the subentity we're interesed in

      \return An index in the range 0 ... Max number of available twist geometries-1 of entities of codimension codim.
    */
    IndexType subIndex (const typename Codim<0>::Entity& e,
                        int i, unsigned int codim) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().subIndex(e,i,codim)));
      return asImp().subIndex(e,i,codim);
    }
    /** @brief Map intersection twist on inside to a twist index.
        The result of calling this method with an intersection for which
        the method contains returns false is undefined.

      \param inter Reference to an intersection.
      \return An index in the range 0 ... Max number of available twist geometries - 1 for codim-1 entities.
    */
    template<template<class> class II>
    IndexType indexInInside (const Intersection<GridImp,II>& inter) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().indexInInside(inter)));
      return asImp().indexInInside(inter);
    }
    /** @brief Map intersection twist on outside to a twist index.
        The result of calling this method with an intersection for which
        the method contains returns false is undefined.

      \param inter Reference to an intersection.
      \return An index in the range 0 ... Max number of available twist geometries - 1 for codim-1 entities.
    */
    template<template<class> class II>
    IndexType indexInOutside (const Intersection<GridImp,II>& inter) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().indexInOutside(inter)));
      return asImp().indexInOutside(inter);
    }

    /** @brief Return total number of twist geometries for all entities of given
        codim.

      \return    number of twist geometries.
    */
    IndexType size (int codim) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().size(codim)));
      return asImp().size(codim);
    }
    /** @brief Return total number of twist geometries for entities of given
        geometry type.

      \return    number of twist geometries.
    */
    IndexType size (const GeometryType& geo) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().size(geo)));
      return asImp().size(geo);
    }

    /** @brief Return true if a twist geometry exists for the given entity.
     */
    template<int cd, int dim, class GI, template<int,int,class> class EI>
    bool contains (const Entity<cd,dim,GI,EI>& e) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().contains(e)));
      return asImp().contains(e);
    }
    /** @brief Return true if a twist geometry exists for the given subentity
     * of given zero codimension entity.
     */
    bool contains (const typename Codim<0>::Entity& e, int i, unsigned int codim) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().contains(e,i,codim)));
      return asImp().contains(e,i,codim);
    }
    /** @brief Return true if a twist geometry exists for the given
     * intersection.
     */
    template<class GI, template<class> class II>
    bool contains (const Intersection<GI,II>& e) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().contains(e)));
      return asImp().contains(e);
    }

    /** @brief Return the twist geometry corresponding to the given twist
     * index.
     *
     * \return    twist geometries for given index.
     */
    template <int c>
    const typename Codim<c>::TwistGeometry& twistGeometry(const GeometryType& geo,const IndexType &index) const
    {
      CHECK_INTERFACE_IMPLEMENTATION((asImp().template twistGeometry<c>(geo,index)));
      return asImp().template twistGeometry<c>(geo,index);
    }

    // Must be explicitly defined although this class should get a default constructor.
    TwistProvider() {}

    private:
    //! Forbid the copy constructor
    TwistProvider(const TwistProvider&);
    //! Forbid the assignment operator
    TwistProvider& operator=(const TwistProvider&);

    protected:
    //!  Barton-Nackman trick
    TwistProviderImp& asImp () {return static_cast<TwistProviderImp &> (*this);}
    //!  Barton-Nackman trick
    const TwistProviderImp& asImp () const {return static_cast<const TwistProviderImp &>(*this);}
  };

  template <class GridImp>
  struct GridWithTwist
  {
    typedef typename GridImp::TwistProvider TwistProvider;
    const TwistProvider& twistProvider() {
      CHECK_INTERFACE_IMPLEMENTATION(( asImp().twistProvider() ));
      return asImp().twistProvider();
    }
    private:
    //! Forbid the copy constructor
    GridWithTwist(const GridWithTwist&);
    //! Forbid the assignment operator
    GridWithTwist& operator=(const GridWithTwist&);
    protected:
    //!  Barton-Nackman trick
    GridImp& asImp () {return static_cast<GridImp &> (*this);}
    //!  Barton-Nackman trick
    const GridImp& asImp () const {return static_cast<const GridImp &>(*this);}
  };

#undef CHECK_INTERFACE_IMPLEMENTATION
#undef CHECK_AND_CALL_INTERFACE_IMPLEMENTATION

}
#endif
