+++
date = "2018-04-25T10:10:00+02:00"
version = "2.5.2"
major_version = "2"
minor_version = "5"
patch_version = "2"
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-grid-howto", "dune-localfunctions"]
signed = 1
title = "Dune 2.5.2"
doxygen_branch = ["v2.5.2"]
doxygen_url = ["/doxygen/2.5.2"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.5.2"]
doxygen_modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-grid-howto"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

From the DUNE core modules only _dune-common_, _dune-grid_ and _dune-localfunctions_ were updated.

# Download the Dune 2.5.2 staging module sources

- **dune-functions** \[ tarball: [dune-functions-2.5.2.tar.gz](/download/2.5.2/dune-functions-2.5.2.tar.gz), signature: [dune-functions-2.5.2.tar.gz.asc](/download/2.5.2/dune-functions-2.5.2.tar.gz.asc) \]
- **dune-typetree** \[ tarball: [dune-typetree-2.5.2.tar.gz](/download/2.5.2/dune-typetree-2.5.2.tar.gz), signature: [dune-typetree-2.5.2.tar.gz.asc](/download/2.5.2/dune-typetree-2.5.2.tar.gz.asc) \]
- **dune-uggrid** \[ tarball: [dune-uggrid-2.5.2.tar.gz](/download/2.5.2/dune-uggrid-2.5.2.tar.gz), signature: [dune-uggrid-2.5.2.tar.gz.asc](/download/2.5.2/dune-uggrid-2.5.2.tar.gz.asc) \]

# DUNE 2.5.2 - Release Notes

DUNE 2.5.2 is a bugfix release, but also includes some minor adjustments.
See below for a list of changes.

## dune-common

* resolve ambiguous call to `size()` function ([dune-common!285])
* backport CMake support for Python ([dune-common!287])
* install all CMake modules ([dune-common!294])
* allow installing Pyuthon package from build directory ([dune-common!295])
* extract_cmake_pata.py: avoid rave between `os.path.exists` and `os.makedirs` ([dune-common!301])
* cmake: fix regular expression for hidden files ([dune-common!346])
* cmake: fix "vverb" no recognized as MINIMAL_DEBUG_LEVEL ([dune-common!370])

  [dune-common!285]: https://gitlab.dune-project.org/core/dune-common/merge_requests/285
  [dune-common!287]: https://gitlab.dune-project.org/core/dune-common/merge_requests/287
  [dune-common!294]: https://gitlab.dune-project.org/core/dune-common/merge_requests/294
  [dune-common!295]: https://gitlab.dune-project.org/core/dune-common/merge_requests/295
  [dune-common!301]: https://gitlab.dune-project.org/core/dune-common/merge_requests/301
  [dune-common!346]: https://gitlab.dune-project.org/core/dune-common/merge_requests/346
  [dune-common!370]: https://gitlab.dune-project.org/core/dune-common/merge_requests/370

## dune-grid

* UG: allow transferring element data during load balancing ([dune-grid!188])

  [dune-grid!188]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/188

## dune-localfunctions

* Pk3D: fix LocalKey generation for special case k=0 ([dune-localfunctions!47])

  [dune-localfunctions!47]: https://gitlab.dune-project.org/core/dune-localfunctions/merge_requests/47

## dune-uggrid

* allow transferring element data during load balancing ([dune-uggrid!64])
* all dim-indep objects should be only in one library ([dune-uggrid!66])
* fix compatibility issues with CMake 3.10 ([dune-uggrid!107])

  [dune-uggrid!64]: https://gitlab.dune-project.org/staging/dune-uggrid/merge_requests/64
  [dune-uggrid!66]: https://gitlab.dune-project.org/staging/dune-uggrid/merge_requests/66
  [dune-uggrid!107]: https://gitlab.dune-project.org/staging/dune-uggrid/merge_requests/107
