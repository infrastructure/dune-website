+++
modules = ['dune-grid']
version = "2.10.0"
signed = 1
title = "CHANGELOG dune-grid 2.10.0"
+++

# Release 2.10

## Changelog

- Gmsh reader learned to read and export gmsh physical entity names.

- Removed the deprecated method `geomTypes()` from all the grid (indexset) implementations since
  they have not been used nor supported by the grid interface for ages. Use `types()` instead. Note
  that `types()` returns an object of type `IndexSet::Types`, which might not be `std::vector<GeometryType>`.
  Also, the object is returned by value instead of by reference.

- Provide `communicate()` method in `OneDGrid` and `IdentityGrid`

- Change the `IndexSet::Types` type of `OndDGrid` and `YaspGrid` to `std::array<GeometryType,1>`.

- Grid concepts are here! A `Grid` can now be inspected and also its components checked for valid
  interfaces at compilation time using c++20 concepts. Note that, this feature is still in development and
  might be subject to changes.

- A new parameter 'bisectioncompatibility' for DGF block GridParameter was added.

- `MultipleCodimMultipleGeomTypeMapper` is assignable.

## Python

- Improve pickling support (GridViews and some GridFunction objects can now be pickled).

- Add a prototype reader for Paraview directly based on the Python bindings.
  This is added to the tutorial and only demonstrates how a reader could be implemented.
  GridViews and GridFunctions can be pickled and then read into Paraview without going through vtu.
  As an example manipulation the subsampling level can be adjusted directly in Paraview so that higher order functions can be visualized.

## Deprecations and removals

- Deprecated `CommDataHandleIF::fixedsize()` has been removed. Use `fixedSize()` with capital
  S instead.

- Remove deprecated `CollectiveCommunication`, use `Communication` instead.

- Deprecated `update()` member function of mappers have been removed. Use the
  member function `update(gridView)` with a grid view argument when updating
  the mapper after the grid or grid view changes. The interface
  change reflects that grid view has value semantics.

- The deprecated convenience classes `LeafSingleCodimSingleGeomTypeMapper`,
  `LevelSingleCodimSingleGeomTypeMapper`, `LeafMultipleCodimMultipleGeomTypeMapper`,
  `LevelMultipleCodimMultipleGeomTypeMapper` have been removed since they
  don't comply with the new mapper interface.
  Just use `SingleCodimSingleGeomTypeMapper` and `MultipleCodimMultipleGeomTypeMapper`.

- Remove Yasp's deprecated `YLoadBalance` interface. Use `Yasp::Partitioning` instead.

- Remove deprecated `GmshReader`'s default constructor. Either use other constructors or use static
  methods without constructing an object.

- Remove deprecated CMake module `UseUG.cmake`.
