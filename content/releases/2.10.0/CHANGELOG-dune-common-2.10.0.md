+++
modules = ['dune-common']
version = "2.10.0"
signed = 1
title = "CHANGELOG dune-common 2.10.0"
+++

# Release 2.10

## C++: Changelog

- `TupleVector` now implements the standard protocol for tuple-like types.

- There is a new base class `IteratorFacade` that unifies `ForwardIteratorFacade`,
  `BidirectionalIteratorFacade`, `RandomAccessIteratorFacade` by making the iterator
  category a template. Furthermore the new `IteratorFacade`  class allows to specify
  a `pointer` type other than `value_type*` to support proxy-iterators.
  The old facade classes remain unchanged for backward compatibility reasons.

- Add utilities `Std::span`, `Std::mdspan`, `Std::mdarray` and all its related classes into the `Dune::Std` namespace
  for working with multidimensional arrays. See core/dune-common!1334 for further details.

- The construction of `FieldMatrix` and `FieldVector` from `std::initializer_list`
  is now `constexpr`.

- Add concepts directory `dune/common/concepts/` and some fundamental concept definitions using
  c++20-concepts. Those concepts are still experimental and are marked with the new `doxygen`
  command `\experimental`. Additionally, the preprocessor constant `DUNE_ENABLE_CONCEPTS` is
  provided when including `dune/common/concepts.hh` that tells whether the library and compiler
  understand the new c++20-concepts and the concept definitions can be used. This constant can also
  be set manually as a compilerflag to enforce enabling or disabling these features.

- Two concept definitions are added: `Dune::Concept::Hashable` and
  `Dune::Concept::[RandomAccess]Container` in `dune/common/concepts/` directory.

- Add user-defined literals `_ic`, `_uc` and `_sc` to represent integral constants.

- Add "hybrid" functors for basic math operations with integral constant arguments, i.e.,
  `Hybrid::max`, `Hybrid::min`, `Hybrid::plus`, `Hybrid::minus`, and `Hybrid::equal_to`. Operations
  between two integral constants result in an integral constant, whereas operations with at least
  one non integral constant argument is performed on the underlying value type.

- Make `filledArray` compatible with non-default-constructble types.

- Add utility `CopyableOptional` that allows to wrap types that are copy constructible but not
  copy assignable and provide assignment operations based on the constructors.

- Added the methods `checkThrow`,`requireThrow` and the corresponding `checkNoThrow`,
  `requireNoThrow` to the `Dune::TestSuite` to test for throwing and no throwing of exceptions.

- Add the utility `IsCompileTimeConstant` to check for integral constants and anything with
  the same interface.

- Add dedicated includes `dune/common/metis.hh` for METIS and `dune/common/parallel/parmetis.hh`
  for ParMETIS to be used instead of the direct includes `metis.h` and `parmetis.h`.

- Add utilities and algorithms to work with `std::integer_sequences`.

- Fixed MPI implementation of `Communication::isend`.

- Move special MPI-related compile flags, like `MPI_NO_CPPBIND`, from `config.h` into
  the cmake utiltiy `add_dune_mpi_flags` and the related global package registration.

- Add new utility type `IndexedIterator` that extends a given iterator by an `index()`
  method returning a traversal index.

## C++: Deprecations and removals

- Remove deprecated macros `DUNE_VERSION_NEWER` and `DUNE_VERSION_NEWER_REV`, use `DUNE_VERSION_GTE`
  and `DUNE_VERSION_GTE_REV` instead. There was no deprecation compiler warning.

- The deprecated header `dune/common/function.hh` has been removed. Use C++ function
  objects and `std::function` stuff instead!

- The deprecated header `dune/common/std/utility.hh` has been removed. Use `<utility>`
  instead.

- The deprecated header `dune/common/std/variant.hh` has been removed. Use `<variant>`
  instead.

- The deprecated header `dune/common/to_unique_ptr.hh` has been removed. Use
  `std::unique_ptr` or `std::shared_ptr` instead.

- Deprecated `conjunction`, `disjunction`, and `negation` have been removed. Instead,
  use the structs from `<type_traits>` introduced with C++17.

- Remove deprecated `dune/common/std/apply.hh`, use `std::apply` instead.

- Deprecated the file `dune/common/assertandreturn.hh` and the contained utility
  `DUNE_ASSERT_AND_RETURN`. Use `assert()` macro directly in `constexpr` functions.

- Remove deprecated header `power.hh`. Use `Dune::power` from `math.hh` instead.

- Deprecate class `SizeOf`. Use `sizeof...` instead.

- Deprecate header `dune/common/keywords.hh` and the provided macros
  `DUNE_GENERALIZED_CONSTEXPR` and `DUNE_INLINE_VARIABLE`. Use the key words directly.

- Remove deprecated header `dune/python/common/numpycommdatahandle.hh`. Use
  `dune/python/grid/numpycommdatahandle.hh` instead.

- Remove in `dune/python/common/dimrange.hh` the `DimRange` specializations for
  dune-typetree and dune-functions types. Those are moved to dune-functions.


## Python: Changelog

- Python: Add `TupleVector` Python bindings

- Python: The function `cppType` now support Python tuples, which are converted to the C++ type `std::tuple`

- Python: Add a dump/load functions to dune.common.pickle which add support
  for storing and recreating the JIT generated modules required for the
  pickling of dune objects to work. In addition a class for writing time
  series of pickled data is provided.

- Python: Add a new argument class to the generator to add pickling support.
  The approach is similar to adding extra constructors or methods.

- Python: Add a new command to `dune.__main__` to compile existing modules
  in parallel, e.g., python -m dune make -j8 hierarchicalgrid
  Add the option to both 'remove' and 'make' commands to read
  module list from a file.

## Build system: Changelog

- Add a `REQUIRED` parameter to `dune_python_configure_bindings`. If set to
  `TRUE` the functions throws an error instead of a warning if the package
  setup fails. The default behavior (or setting `REQUIRE` to `FALSE`) is to
  show the warning during configuration and to continue.

- Dune package dependencies are now transitively resolved at `find_package(<dune-module>)` calls instead of waiting
  until the call to `dune_project()`. For example, a CMake call to `find_package(dune-grid)` will transitively
  find the dune packages `dune-common`, `dune-geometry` and (if available) `dune-uggrid`. Note that the targets
  provided by those found modules are still being set up at the `dune_project()` call.

- Documentation files in `doc/buildsystem/${ModuleName}.rst` are now only copied.
  Previously, they were configured through the CMake function `configure_file()`
  as a cmake template file.

- Try to find SuiteSparse during configuration.

- The function `dune_add_library(<lib> ...)` now requires to provide `EXPORT_NAME` or `NO_EXPORT`.
  Moreover, a namespace can be specified via the argument `NAMESPACE` which defaults to `Dune::` and is prepended to the export name.
  We recommend to choose an export name with a camel title case matching your
  library name (e.g., `Common`, `ISTL`, and `MultiDomainGrid` will be exported as
  `Dune::Common`, `Dune::ISTL`, and `Dune::MultiDomainGrid`).
  _Warning:_ Both `<lib>` and `Dune::${EXPORT_NAME}` are currently exported. Keep in mind that:
   * Libraries that _consume_ `Dune::${EXPORT_NAME}` will only be forward compatible with Dune 2.10.
   * Libraries that _consume_ `<lib>` will be supported until compatibility with Dune 2.9 is not required anymore.

- Generation of `config.h` is overhauled and split in a public and a private config file. Only
  the public file is installed and consumed by down-stream modules. For compatibility, the
  old single file is created, too.

- The CMake macro `finalize_dune_project` no longer has an optional argument, a config file is
  always created.

- Do not overwrite the `add_test` cmake function with an error message.

- Setting the minimal c++ standard in cmake is now done by a cmake feature-requirement
  `cxx_std_17` on the `dunecommon` library target. This requirement is propagated to all
  other modules by linking against `dunecommon`.

- We have changed the way optional dependencies are activated in the build-system internally.
  The cmake macros `add_dune_xy_flags` do not set the compiler flag `-DENABLE_XY=1` anymore, but instead
  set directly the flag `-DHAVE_XY=1`. Neither `ENABLE_XY` nor `HAVE_XY` should be modified manually
  by the user. Since the `HAVE_XY` flag is now set as a compiler flag, it is not included in the
  `config.h` files anymore.

- Add a policy system to smoothly change behavior in the build-system. This follows the cmake policy
  system but uses own IDs and is connected to dune module version instead of cmake versions.

- Rename `<module>_INTERFACE_LIBRARIES` into `<module>_LIBRARIES` (representing all module-libraries)
  and introduce `<module>_EXPORTED_LIBRARIES` as a list of all libraries exported by the module.


## Build system: Deprecations and removals

- Remove the search of (currently broken) `pkg-config` files for dune packages.

- Remove the `ALLOW_CXXFLAGS_OVERWRITE` configure option. The `CXXFLAGS`
  overload is still turned on for the JIT compiled Python modules. See the
  description of the MR
  https://gitlab.dune-project.org/core/dune-common/-/merge_requests/1251
  for more details on how to use this feature in the source modules and on
  some new feature.

- Remove deprecated `add_directory_test_target` function.

- The cmake options `CXX_MAX_STANDARD`, `CXX_MAX_SUPPORTED_STANDARD` and `DISABLE_CXX_VERSION_CHECK`
  are removed. The cmake function `dune_require_cxx_standard()` is now deprecated.

- Deprecate CMake macro `message_verbose`. Use `message(VERBOSE "message text")` instead.
  This macro will be removed after Dune 2.10.

- Remove deprecated CMake file `DuneCMakeCompat.cmake` that only contained a
  deprecation message.

- Remove deprecated CMake function `inkscape_generate_png_from_svg`. Use
  `dune_create_inkscape_image_converter_target` instead.

- Remove deprecated `rmgenerated.py`. Call `python -m dune remove` with the same
  arguments instead.

- Remove `DunePythonDeprecations.cmake` that was used to ease the overhaul
  of Python CMake integration.

- Remove deprecated CMake function `dune_python_install_package`. Use
  `dune_python_configure_bindings` or `dune_python_configure_package`
  according to the needed behavior.
