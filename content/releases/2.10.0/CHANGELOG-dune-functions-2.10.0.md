+++
modules = ['dune-functions']
version = "2.10.0"
signed = 1
title = "CHANGELOG dune-functions 2.10.0"
+++

# Dune-functions changes

Any version of dune-functions is supposed to be compatible with the
corresponding version of the Dune core modules.

## Release 2.10

- The module dune-uggrid is now officially a hard dependency of dune-functions.
- Added a constructor for `LocalViewWrapper` from a `LocalView` object and added
  accessor to base class for python bindings.
- The class `Polynomial` now allows to explicitly specify the used coefficient container.
  Supported containers are `std::vector` (default, as before), `std::array`, `std::tuple`,
  and `std::integer_sequence`. Class template argument deduction allows to deduce the
  scalar type from homogeneous containers. The scalar type can be explicitly specified
  to be different with the `makePolynomial()` helper function, while the coefficients
  container type is still deduced.
- Make `AnalyticGridViewFunction` copyable
- The method `operator()` of the class `DiscreteGlobalBasisFunction` has been implemented.
  It evaluates a piecewise function on a grid at a point given in global coordinates.
  As this evaluation requires finding the element that the evaluation point is in,
  it can be very slow.
- Reimplement `HasStaticSize` in terms by checking `Hybrid::size()`
- Add utility `StaticSizeOrZero` that defaults to the `integral_constant<size_t,0>` in case
  the container does only have dynamic size.
- Add global coordinate evaluation for `CompositeGridFunction`
- Add new global basis function based on refined Lagrange local-functions. The pre-basis is called
  `template <typename GV, int k, typename R=double> class RefinedLagrangePreBasis` and a corresponding
  pre-basis-factory `refinedLagrange<k,R=double>()` with `k=[1|2]` currently implemented.
- All basis implementations are now copyable and assignable
- Add mixin class `LeafPreBasisMixin` and make available the mixin class `LeafPreBasisMapperMixin` to
  simplify writing of new pre-bases.
- Add a dynamic power basis with a runtime exponent that can be constructed with the factory
  `power(<sub-basis>, k)` or `power(<sub-basis>, k, <index-merging>)`.
- Add mixin class `LFEPreBasisMixin` to create leaf pre-bases based on a local finite-element and a layout.
- The class `LeafPreBasisMapperMixIn` is renamed into `LeafPreBasisMapperMixin`.
- Add data structures for container descriptors in namespace `Dune::Functions::ContainerDescriptors`.
- Introduce a new member-function `.containerDescriptor()` for all pre-bases that return an instance of
  one of the container descriptors to represent a container that can be indexed by the multi-indices of that
  pre-basis.
- Container descriptor for power bases with blocked-interleaved index-merging-strategy added.

### Python

- The Nédélec and Raviart-Thomas function space bases are now accessible via the Python interface.

Deprecations and removals

- The class `DefaultNodeToRangeMap` and the corresponding header `functionspacebases/defaultnodetorangemap.hh`
  have been deprecated. This has been replaced as default by `HierarchicNodeToRangeMap` for some time.
- The `BasisBuilder` namespace is deprecated and will be removed
  after the 2.10 release. Use `BasisFactory` instead.
- The headers `common/callable.hh` and `common/functionfromcallable.hh` providing adaptors
  the the removed `VirtualFunction` interface from dune-common have been removed.
- Remove deprecated `DefaultLocalView::isBound()`, use `bound()` instead.
- The class `HierarchicalVectorWrapper` and the corresponding header `functionspacebases/hierarchicvectorwrapper.hh`
  have been deprecated. Use `istlVectorBackend()` from `backends/istlvectorbackend.hh` instead.
- The class `SizeInfo` and the corresponding header `functionspacebases/sizeinfo.hh`
  have been deprecated. One now can directly use `vectorBackend.resize(globalBasis)`
  instead of `vectorBackend.resize(sizeInfo(globalBasis))`.
- The deprecated header `common/treedata.hh` was removed.
- The deprecated header `common/referencehelper.hh` was removed. Use the corresponding header
  from dune-common instead.
