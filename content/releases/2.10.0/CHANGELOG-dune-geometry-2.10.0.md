+++
modules = ['dune-geometry']
version = "2.10.0"
signed = 1
title = "CHANGELOG dune-geometry 2.10.0"
+++

# Release 2.10

- Quadrature point now allows C++ structured bindings so that
  numerical integrations are less verbose:
  ```c++
  for(auto [position, weight] : Dune::QuadratureRules</*...*/>::rule(/*...*/))
    result += f(position) * weight;
  ```

- `Dune::Geo::ReferenceElement` methods that have returned references, return
  by value. This affects `type()`, `position()`, and `integrationOuterNormal()`.
- Add a meta-geometry type `MappedGeometry` that represents the chaining of another
  geometry and a callable.

- `AffineGeometry` and `AxisAlignedGeometry` are now default constructible. A
  default construction results in an "empty"/invalid geometry that can be assigned
  a valid geometry.

- Add a geometry `LocalFiniteElementGeometry` parametrized by local finite-element
  basis functions.

## Deprecations and removals

- `Dune::Transitional::ReferenceElement` is deprecated and will be removed after
  Dune 2.10. Use `Dune::Geo::ReferenceElement` directly.

- Remove header `deprecated_topology.hh` that was introduced for transition.

- Remove deprecated functions `factorial()` and `binomial()`. Use the
  functions from dune-common's `math.hh`.
