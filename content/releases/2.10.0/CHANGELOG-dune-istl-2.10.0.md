+++
modules = ['dune-istl']
version = "2.10.0"
signed = 1
title = "CHANGELOG dune-istl 2.10.0"
+++

# Release 2.10

- Improve testing support on Laplacian matrices with an optional diagonal regularization parameter.

- Base the implementation of `VariableBlockVector` on `std::vector` as the storage type. Note that
  this prevents from using `bool` as block type that was possible before.

- A method `BCRSMatrix::setIndicesNoSort()` was added. Similar
  to `BCRSMatrix::setIndices()` this allows to insert all indices
  of a row at once, but - incontrast to the latter - does not sort them.
  This allows to speed up insertion if indices are already sorted.

- `UMFPACK` is extended to arbitrary blocked matrix structures. This includes `MultiTypeBlockMatrix`. The external interface is unchanged.
  However, internally the `BCCSMatrixInitializer` is replaced by direct calls of `flatMatrixForEach` similar to `Cholmod`. This requires a
  compatible vector field of "ignored" degrees of freedom. The method `setSubMatrix` with a top-level index set is preserved for compatibility.

- The internal storage in `MatrixIndexSet` was changed from `std::set` to a sorted `std::vector`
  (with a `std::set` fallback for very dense rows) to improve performance. The stored index
  type was changed from `std::size_t` to `uint32_t` to reduce memory consumption and improve
  performance. Hence, `MatrixIndexSet` can no longer be used for very large matrices with more
  than 2^32 columns.

- Added flag 'useFixedOrder' to the coarsen method of AMGs ParallelIndicesCoarsener.
  If set to true, during the creation of the coarser matrix (by accumulation and restriction
  to fewer processes) the coarse matrix rows are ordered in a fixed manner, making parallel runs
  reproducible; the runtime is possibly not ideal though.
  If set to false (which is the default), the row order depends on the order of messages received from the
  processes responsible for the respective parts of the finer grid. Then the indices on the coarser grid
  may differ from run to run.

- Define `field_type` and `real_type` in `MultiTypeBlock[Matrix|Vector]` only if a common type of these
  types exist over all blocks in the container. Otherwise it is defined as `Std::nonesuch`.

## Deprecations and removals

- The deprecated CMake variables `SUPERLU_INCLUDE_DIRS`, `SUPERLU_LIBRARIES`,
  `SUPERLU_DUNE_COMPILE_FLAGS`, and `SUPERLU_DUNE_LIBRARIES` are removed, they are
  replaced by the target `SuperLU::SuperLU`.

- Multiple containers no longer provide the deprecated member function `blocklevel`. Use the free function
  `blockLevel()`.

- Removed deprecated function `initSolverFactories()`, use `initSolverFactories<O>` instead.

- Removed deprecated `MultyTypeBlockMatrix::size()`, use `N()` instead.

- Removed deprecated `MultyTypeBlockVector::count()`, use `N()` instead.

- Deprecated `writeSVGMatrix` with `std::stream` as the second argument. Use the method
  with `std::stream` as the first argument.
