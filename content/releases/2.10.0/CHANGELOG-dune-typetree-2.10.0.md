+++
modules = ['dune-typetree']
version = "2.10.0"
signed = 1
title = "CHANGELOG dune-typetree 2.10.0"
+++

Changes
=======

TypeTree 2.10
----------------

- `HybridTreePath` functions now have `[[nodiscard]]` attribute
- `HybridTreePath` gained several multi-index utilities:
  `max_size`, `join`, `reverse`, and `accumulate_[front|back]`.
- Add user-defined literal `_tp` to generate `HybridTreePath`

Deprecations and removals

- Remove deprecated `TreePath`, use `StaticTreePath` instead.
- Remove deprecated `CHILDREN` from `CompositeNode`, `FilteredCompositeNode`, `LeafNode`,
  `PowerNode`, `ProxyNode`. Use `degree()` instead.
- Remove deprecated `forEachNode`, use `applyToTree` instead.
