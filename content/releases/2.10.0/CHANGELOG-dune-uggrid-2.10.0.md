+++
modules = ['dune-uggrid']
version = "2.10.0"
signed = 1
title = "CHANGELOG dune-uggrid 2.10.0"
+++

# dune-uggrid 2.10 (2024-09-04)

* Remove deprecated `AllocEnvMemory` and `FreeEnvMemory`. They were
  wrappers of standard `malloc` and `free`.

* Remove deprecated `Mark` and `Release`. Use `MarkTmpMem` and
  `ReleaseTmpMem` instead.

* Remove deprecated `HeapAllocMode` and its flags `FROM_BOTTOM` and `FROM_TOP`.
  They lost any influence, as we use the system heap.

* Remove deprecated `DDD_InfoProcList`, use `DDD_InfoProcListRange` instead.

* Remove deprecated `DDD_IFOneway`, use method overload with context as first
  argument instead.
