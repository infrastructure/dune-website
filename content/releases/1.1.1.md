+++
date = "2008-11-06T16:03:37+01:00"
major_version = 1
minor_version = 1
modules = ["dune-common", "dune-istl", "dune-grid", "dune-grid-howto"]
patch_version = 1
title = "Dune 1.1.1"
version = "1.1.1"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 1.1.1 - Release notes

## Changes to previous release DUNE 1.1

The changes to the previous release DUNE 1.1 are the following:

*   **dune-common**

    For a detailed list see the [patch file](http://www.dune-project.org/download/1.1.1/diff-dune-common-1.1.1.patch) generated with diff.

    *   make the code compile with g++-4.3.x
    *   the files lapack.m4 and pardiso.m4 were missing
    *   small documentation bug fixes

*   **dune-grid**

    For a detailed list see the [patch file](http://www.dune-project.org/download/1.1.1/diff-dune-grid-1.1.1.patch) generated with diff.

    *   make the code compile with g++-4.3.x
    *   bug fixes in VTKWriter
    *   bug fix in Grape visualisation
    *   small bug fixes in ALUGrid (inside asserts)

*   **dune-istl**

    For a detailed list see the [patch file](http://www.dune-project.org/download/1.1.1/diff-dune-istl-1.1.1.patch) generated with diff.

    *   make the code compile with g++-4.3.x

## Tested Compilers

The DUNE core developers have tested DUNE with the following compilers.

*   Gentoo Linux, using openmpi and gcc (4.1.2, 4.3.0, 4.3.2)

For known bugs, please take a look at the [bug tracking system](https://gitlab.dune-project.org/flyspray/FS/-/issues) and the documentation!
