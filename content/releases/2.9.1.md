+++
date = "2023-12-16T22:30:00+01:00"
version = "2.9.1"
major_version = "2"
minor_version = "9"
patch_version = "1"
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions"]
signed = 1
title = "Dune 2.9.1"
doxygen_branch = ["v2.9.1"]
doxygen_url = ["/doxygen/2.9.1"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.9.1"]
support = ["linux","windows"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# DUNE 2.9.1 - Release Notes

DUNE 2.9.1 is a bugfix release, but also includes some minor adjustments.
See below for a list of changes.

- Fixed compatibility with more recent compilers.
