+++
git = "https://gitlab.dune-project.org/extensions/dune-grid-glue"
group = ["extension"]
module = "dune-grid-glue"
requires = ["dune-common", "dune-geometry", "dune-grid"]
short = "Provides infrastructure for the coupling of two unrelated Dune grids."

doxygen_url = ["/doxygen/dune-grid-glue/master", "/doxygen/dune-grid-glue/releases2.10", "/doxygen/dune-grid-glue/releases2.9"]
doxygen_name = ["DUNE-GRID-GLUE", "DUNE-GRID-GLUE", "DUNE-GRID-GLUE"]
doxygen_modules = ["dune-grid-glue"]
doxygen_branch = ["master", "releases/2.10", "releases/2.9"]
doxygen_version = ["unstable", "2.10", "2.9"]
+++

{{% render_git_readme %}}
