+++
git = "https://gitlab.dune-project.org/core/dune-common"
group = ["core"]
maintainers = "The Dune Core developers <dune@lists.dune-project.org>"
module = "dune-common"
pythonbindings = "yes"
short = "Basic infrastructure classes for all Dune modules"
title = "dune common"
weight = -1000
+++
