+++
module = "dune-fempy"
group = ["tutorial"]
requires = ["dune-fem" ]
suggests = [ "dune-fem-dg", "dune-vem" ]
title = "dune-fem tutorial"
website = "/sphinx/content/sphinx/dune-fem"
git = "https://gitlab.dune-project.org/dune-fem/dune-fempy"
short = "A tutorial on using the DUNE-FEM discretization module through Python."
+++

### General
This module contains many examples of how to use the DUNE-FEM module
through Python.
[The tutorial](/sphinx/content/sphinx/dune-fem)
contains many script/Jupyter notebooks providing an
overview of the available features and demonstrating how
to solve a wide range of complex problem in Python. These scripts and
notebooks provide a good starting point for own projects.
