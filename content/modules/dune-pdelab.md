+++
title = "dune-pdelab"
group = ["disc"]
module = "dune-pdelab"
requires = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-uggrid", "dune-typetree", "dune-functions"]
short = "A generalized discretization module for a wide range of discretization methods. It allows rapid prototyping for implementing discretizations and solvers for systems of PDEs based on DUNE."
suggests = ["dune-alugrid", "dune-multidomaingrid"]

git = "https://gitlab.dune-project.org/pdelab/dune-pdelab"
tutorial = "[PDELab beginners guide](/doc/gettingstarted/beginners-resources-pdelab/), [PDELab Tutorial](/modules/dune-pdelab-tutorials/)"

# Trigger the generation of doxygen documentation
doxygen_url = ["/doxygen/pdelab/master", "/doxygen/pdelab/releases2.9", "/doxygen/pdelab/releases2.8", "/doxygen/pdelab/releases2.7"]
doxygen_modules = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-typetree", "dune-pdelab", "dune-functions", "dune-uggrid"]
doxygen_name = ["DUNE PDELab", "DUNE PDELab", "DUNE PDELab", "DUNE PDELab"]
doxygen_branch = ["master", "releases/2.9", "releases/2.8", "releases/2.7"]
doxygen_version = ["unstable", "2.9", "2.8", "2.7"]
# PDELab now has a custom main page to guide users to the recipes
doxygen_mainpage = ["mainpage.txt", "mainpage.txt", "mainpage.txt", "mainpage.txt"]
+++

### Beginners Resources

[Go here for installation instructions for beginners](/doc/gettingstarted/beginners-resources-pdelab/).

[Go here for the PDELab Tutorial](/modules/dune-pdelab-tutorials/).

### General
<mark>dune-pdelab</mark> is a discretization module based on [DUNE](/about/dune/) with the following aims:

* Rapid prototyping: Substantially reduce time to implement discretizations and solvers for systems of PDEs based on DUNE
* Simple things should be simple: Suitable for teaching
* Use of different linear solvers: Exchangeable linear algebra backend

Its main abstractions are:

* Flexible discrete function spaces:
  - Conforming and non-conforming
  - hp-refinement
  - general approach to constraints
  - Product spaces for systems
  - Adaptivity and parallel data decomposition
* Operators based on weighted residual formulation:
  - Linear and nonlinear
  - stationary and transient
  - FE and FV schemes requiring at most face-neighbors

The following applications have been realized using PDELab

1. Various elliptic, parabolic and hyperbolic modelproblems.
* Higher-order conforming and discontinuous Galerkin finite element methods.
* Cell-centered finite volume method.
* Mixed finite element method.
* Mimetic finite difference method.
* Incompressible Navier-Stokes equations.
* Two-phase flow.
* Multi-phase flow in porous media.
* Linear acoustics.
* Maxwell's equations.
* [DuMu<sup>x</sup>](http://www.dumux.org/) is a simulator for flow and transport in porous media based on DUNE and PDELab.

---
### Tutorials
The module [dune-pdelab-tutorials](https://dune-project.org/modules/dune-pdelab-tutorials/) contains code examples,
exercises and documentation to get you started with PDELab, split up into a number of separate tutorials. This module
also forms the basic of the yearly PDELab introduction course at Heidelberg University.

Older versions of PDELab instead provided a different module called *dune-pdelab-howto*, but this is not maintained any longer.

### Multiphysics
Currently an extension of PDELab is in development that allows to couple different models in different subdomains. It consist of the following two additional modules

* [dune-multidomaingrid](http://github.com/smuething/dune-multidomaingrid) provides a metagrid that allows to decompose a grid into several (possibly overlapping) subdomains. This module is independent of dune-pdelab.

### Documentation
* Access the online documentation of the 2.7.0 release ([click](https://dune-project.org/doxygen/pdelab/releases2.7/)).
* Access the online documentation of the 2.8.0 release ([click](https://dune-project.org/doxygen/pdelab/releases2.8/)).
* Access the online documentation of the 2.9.0 release ([click](https://dune-project.org/doxygen/pdelab/releases2.9/))
* Access the online documentation of the development version ([click](https://dune-project.org/doxygen/pdelab/master/)).

### Bug Tracking System
The bug tracker for PDELab can be found on [Dune's GitLab server](https://gitlab.dune-project.org/pdelab/dune-pdelab/issues).

### Mailing list
If you really cannot find your answer in either the Howto or the documentation, you might consult the [Dune PDELab mailing list](https://lists.dune-project.org/mailman/listinfo/dune-pdelab).


### Git Access
If you want access to the latest and greatest features of PDELab or you are using the master of the Dune core modules, you can directly check out the development version via anonymous Git and download with :

`git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git`


Alternatively, you can check out a release branch (or an older snapshot branch) if you need compatibility with a specific Dune release, but still want access to some backported fixes that have not made it into release packages yet.

Please note that you need additional software to build the documentation from an Git checkout, and that the documentation is not built automatically. If you want to build it, try running make doc and install any missing software the build process complains about.


<p>You can also browse the Git repositories of <b><a href="https://gitlab.dune-project.org/staging/dune-typetree">dune-typetree</a></b>, <b><a href="https://gitlab.dune-project.org/pdelab/dune-pdelab">dune-pdelab</a></b> and <b><a href="https://gitlab.dune-project.org/pdelab/dune-pdelab-tutorials">dune-pdelab-tutorials</a></b> through a more comfortable web frontend.</p>
