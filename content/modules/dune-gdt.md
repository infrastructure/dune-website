+++
# The name of the module.
module = "dune-gdt"

#For page title in browser
title = "dune-gdt"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "extension", "tutorial"
group = ["disc"]

# List of modules that this module requires
requires = ["dune-xt-common", "dune-xt-grid", "dune-xt-la", "dune-xt-functions", "dune-localfunctions"]

# List of modules that this module suggests
suggests = []

# A string with maintainers to be shown in short description, if present.
maintainers = "[René Fritze](mailto:rene.fritze@wwu.de), [Tim Keil](mailto:tim.keil@wwu.de), [Tobias Leibner](mailto:tobias.leibner@wwu.de), [Felix Schindler](mailto:felix.schindler@wwu.de)"

# Main Git repository, uncomment if present
git = "https://github.com/dune-community/dune-gdt"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "dune-gdt is a DUNE module which provides a generic discretization toolbox for grid-based numerical methods. It contains building blocks - like local operators, local evaluations, local assemblers - for discretization methods and suitable discrete function spaces."
+++

# What is dune-gdt?

dune-gdt is a DUNE module which provides a generic discretization toolbox for grid-based numerical methods. It contains building blocks - like local operators, local evaluations, local assemblers - for discretization methods and suitable discrete function spaces.
dune-gdt's design goals are:

- to closely mirror mathematics making it easy to translate from theory to implementation
- be conveniently usable without hiding C++/DUNE, thus giving the user full control
- code reuse as far as possible to mimize code that needs to be rewritten and maintained

# How to use dune-gdt
We provide a git repository containing dune-gdt and all required dependencies. You can find this super module and installation instructions [here](https://github.com/dune-community/dune-gdt-super).

If you don't want to use the super module, just put all required DUNE modules in a folder together with dune-gdt and configure/build it through dunecontrol (see the documentation of dune-common for details).
