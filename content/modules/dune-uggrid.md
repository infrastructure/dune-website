+++
module = "dune-uggrid"
git = "https://gitlab.dune-project.org/staging/dune-uggrid"
group = ["grid"]
maintainers = "Oliver Sander <oliver.sander@tu-dresden.de>"
requires = ["dune-common"]
short = "This is a fork of the old [UG](https://doi.org/10.1007/s007910050003) finite element software, stripped of everything but the grid data structure. You need this module if you want to use the UGGrid grid implementation from dune-grid."
weight = -5
+++

{{% render_git_readme %}}
