+++
module = "dune-polygongrid"
group = ["grid"]
requires = ["dune-grid"]
pythonbindings = "yes"
maintainers = "Robert Klöfkorn, Martin Nolte"
git = "https://gitlab.dune-project.org/extensions/dune-polygongrid"
short = "PolygonGrid implements a DUNE grid consisting of polygons."
weight = -1
+++

Dune-PolygonGrid
================

The DUNE module dune-polygongrid provides an implementation of the DUNE grid
interface for grids consisting of polygons.
