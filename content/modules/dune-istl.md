+++
git = "https://gitlab.dune-project.org/core/dune-istl"
group = ["core"]
module = "dune-istl"
maintainers = "The Dune Core developers <dune@lists.dune-project.org>"
requires = ["dune-common"]
pythonbindings = "partially"
short = "This is the iterative solver template library which provides generic sparse matrix/vector classes and a variety of solvers based on these classes. A special feature is the use of templates to exploit the recursive block structure of finite element matrices at compile time. Available solvers include Krylov methods, (block-) incomplete decompositions and aggregation-based algebraic multigrid."
title = "dune istl"
weight = -1
+++
