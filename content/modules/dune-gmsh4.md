+++
module = "dune-gmsh4"
group = ["io"]

# List of modules that this module requires
requires = ["dune-grid"]
suggests = ["dune-alugrid", "dune-foamgrid", "dune-vtk", "dune-curvedgrid"]
maintainers = "[Simon Praetorius](mailto:simon.praetorius@tu-dresden.de), [Florian Stenger](mailto:florian.stenger@tu-dresden.de)"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/extensions/dune-gmsh4"

short = "File reader for the Gmsh-4 file format"

+++

{{% render_git_readme %}}
