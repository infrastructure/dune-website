+++
module = "dune-copasi"
group = ["user"]
requires = ["dune-multidomaingrid", "dune-pdelab"]
maintainers = "[Santiago Ospina De Los Ríos](mailto:santiago@dune-project.org)"
git = "https://gitlab.dune-project.org/copasi/dune-copasi"
title = "DuneCopasi"
short = "Solver for reaction-diffusion systems in multiple compartments"
website = "https://dune-copasi.netlify.app/"
+++

## Overview

DuneCopasi is a C++ library designed to simulate how chemical reactions and diffusion processes occur in space, which is crucial for understanding many biological systems. It allows users to model these processes in both simple and complex environments, using grids that represent one, two, or three dimensions. Optimized for modern computers, DuneCopasi can be used as a standalone command-line application, accessed through tools like Docker or a web-based terminal, as a C++ library, or integrated into a graphical interface. One such interface is the [Spatial Model Editor (SME)](https://spatial-model-editor.github.io/), which helps users create and manipulate biological reaction models. The project was initiated to provide the numerical foundation for a spatial modelling tool for biochemical reaction networks, complementing the [COmplex PAthway SImulator (COPASI)](https://copasi.org/) software. This development resulted from a collaboration between parts of the COPASI team and the [Distributed and Unified Numerics Environment (DUNE)](/about/dune/) team.

### Resources

* [Publication in the Journal of Open Source Software (JOSS)](https://doi.org/10.21105/joss.06836)
* [Gallery entry](/gallery/dune-copasi/)
* [Releases](https://gitlab.dune-project.org/copasi/dune-copasi/-/releases)
* [Container Registry](https://gitlab.dune-project.org/copasi/dune-copasi/container_registry/88)
* [License](https://gitlab.dune-project.org/copasi/dune-copasi/-/blob/master/LICENSE.md)