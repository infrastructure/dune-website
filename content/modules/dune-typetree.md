+++
git = "https://gitlab.dune-project.org/staging/dune-typetree"
group = ["extension"]
maintainers = "Simon Praetorius <simon.praetorius@tu-dresden.de>, Christian Engwer<simon.praetorius@tu-dresden.de>, Santiago Ospina De Los Ríos <santiago@dune-project.org>"
module = "dune-typetree"
requires = ["dune-common"]
title = "dune-typetree"

# Trigger the generation of doxygen documentation
doxygen_url = ["/doxygen/dune-typetree/master", "/doxygen/dune-typetree/releases2.10", "/doxygen/dune-typetree/releases2.9", "/doxygen/dune-functions/releases2.8"]
doxygen_name = ["Dune TypeTree", "Dune TypeTree", "Dune TypeTree", "Dune TypeTree"]
doxygen_branch = ["master", "releases/2.10", "releases/2.9", "releases/2.8"]
doxygen_version = ["unstable", "2.10", "2.9", "2.8"]
+++

{{% render_git_readme %}}
