+++
module = "dune-multidomaingrid"
git = "https://gitlab.dune-project.org/extensions/dune-multidomaingrid"
group = ["grid"]
maintainers = "Santiago Ospina De Los Ríos <santiago@dune-project.org>"
requires = ["dune-common","dune-grid"]
short = "MultiDomainGrid is a meta grid that allows you to carve up an existing DUNE grid into subdomains and extract interfaces for e.g. multi-physics simulations with multiple domains."

# Trigger the generation of doxygen documentation
doxygen_url = ["/doxygen/dune-multidomaingrid/master", "/doxygen/dune-multidomaingrid/releases2.10", "/doxygen/dune-multidomaingrid/releases2.9", "/doxygen/dune-multidomaingrid/releases2.8"]
doxygen_branch = ["master", "releases/2.10", "releases/2.9", "releases/2.8"]
doxygen_version = ["unstable", "2.10", "2.9", "2.8"]
doxygen_name = ["DUNE MultiDomainGrid", "DUNE MultiDomainGrid", "DUNE MultiDomainGrid", "DUNE MultiDomainGrid"]
+++

{{% render_git_readme %}}
