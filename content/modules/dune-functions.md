+++
git = "https://gitlab.dune-project.org/staging/dune-functions"
group = ["extension"]
module = "dune-functions"
requires = ["dune-localfunctions", "dune-grid", "dune-typetree", "dune-istl"]
short = "Abstractions for functions and discrete function space bases"
suggests = []
title = "dune-functions"

# Trigger the generation of doxygen documentation
doxygen_url = ["/doxygen/dune-functions/master", "/doxygen/dune-functions/releases2.10", "/doxygen/dune-functions/releases2.9", "/doxygen/dune-functions/releases2.8"]
doxygen_name = ["DUNE-FUNCTIONS", "DUNE-FUNCTIONS", "DUNE-FUNCTIONS", "DUNE-FUNCTIONS"]
# doxygen_modules = ["dune-common", "dune-istl", "dune-localfunctions", "dune-geometry", "dune-grid", "dune-typetree"]
doxygen_branch = ["master", "releases/2.10", "releases/2.9", "releases/2.8"]
doxygen_version = ["unstable", "2.10", "2.9", "2.8"]
+++

{{% render_git_readme %}}

