+++
date = "2016-02-10T15:28:57+01:00"
git = "https://gitlab.dune-project.org/core/dune-grid"
group = ["core", "grid"]
module = "dune-grid"
requires = ["dune-common", "dune-geometry"]
pythonbindings = "yes"
short = "The Dune grid interface and some grid implementations"
maintainers = "The Dune Core developers <dune@lists.dune-project.org>"
title = "dune grid"
weight = -10
+++

Dune-Grid
=========

The dune-grid module contains the implementations of AlbertaGrid, GeometryGrid, OneDGrid and YaspGrid.
