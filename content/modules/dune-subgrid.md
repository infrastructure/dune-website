+++
module = "dune-subgrid"
group = ["grid"]
requires = ["dune-grid"]
maintainers = "Carsten Gräser"
git = "https://gitlab.dune-project.org/extensions/dune-subgrid.git"
short = "dune-subgrid allows you to mark a subset of the elements of a given grid. This subset can then be treated as a grid hierarchy in its own right."
+++
