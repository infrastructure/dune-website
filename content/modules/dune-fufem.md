+++
module = "dune-fufem"
group = ["disc"]
requires = ["dune-common", "dune-geometry", "dune-grid", "dune-istl", "dune-localfunctions", "dune-functions", "dune-matrix-vector"]
short = "The dune-fufem module is a discretization module that emphasizes easy-of-use over flexibility and performance."
maintainers = "The dune-fufem team <dune-fufem@lists.spline.inf.fu-berlin.de>"
git = "https://gitlab.dune-project.org/fufem/dune-fufem"
title = "dune-fufem"
+++

#### dune-fufem

<i style="color:blue">dune-fufem</i> is a set of discretation methods to assemble linear and nonlinear finite element problems.
It emphasizes ease-of-use over performance and flexibility.  In particular, it uses more dynamic polymorphism and less
template metaprogramming than other discretization modules.

dune-fufem used to implement its own function space bases, but is nowadays in the process of transitioning to the function space
bases implemented in the dune-functions module.  Use of the native dune-fufem bases is discouraged.

The string <i style="color:blue">fufem</i> is an acronym for <i style="color:blue">Freie Universität Finite Element Method</i>,
because dune-fufem was originally developed at Freie Universität Berlin.

#### Features
* Assemblers for stiffness matrices and linear functionals
* Support for constrained function space bases, e.g., for conforming
  finite element spaces on nonforming adaptively refined grids
* Abstractions for grid boundary patches, and assembler for spaces on those
* An interface to Python.  In particular, Python functions can be called from C++ code.
  Moreover this provides conversions between various Python and Dune-C++ types
  like, e.g., FieldVector, FieldMatrix, GridFactory.
* Building blocks for the implementation of hierarchical error estimators





#### Download
You can download the current development version using anonymous git:

`git clone https://gitlab.dune-project.org/fufem/dune-fufem.git`
