+++
# The name of the module.
module = "dune-vtk"
group = ["io"]
requires = ["dune-grid"]
suggests = ["dune-functions","dune-spgrid","dune-polygongrid","dune-alugrid","dune-foamgrid","dune-uggrid"]
maintainers = "[Simon Praetorius](mailto:simon.praetorius@tu-dresden.de)"
git = "https://gitlab.dune-project.org/extensions/dune-vtk"
short = "File reader and writer for the VTK Format"
+++

{{% render_git_readme %}}
