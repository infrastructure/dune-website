+++
date = "2016-11-22"
title = "Dune 2.5.0rc1 Released"
+++

The first release candidate for the upcoming 2.5 release is now available.
You can [download the tarballs][], checkout the `v2.5.0rc1`
tag via Git, or get prebuilt packages from Debian experimental. Please go
and test, and report any problems that you encounter.

Included in the release candidate are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-istl, dune-localfunctions) and several
modules from the [staging area][] (dune-functions, dune-typetree,
dune-uggrid).

Please refer to the [recent changes] page for an overview of what has
changed in the new release.

  [core modules]: https://gitlab.dune-project.org/core/
  [staging area]: https://gitlab.dune-project.org/staging/
  [recent changes]: /dev/recent-changes
