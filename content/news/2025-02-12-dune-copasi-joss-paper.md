+++
date = "2025-02-12"
title = "DuneCopasi v2.1.0 released with JOSS Publication"
tags = [ "publications", "releases"]
+++

The application module [DuneCopasi](/modules/dune-copasi/) has recently released [version 2.1.0](https://gitlab.dune-project.org/copasi/dune-copasi/-/releases/v2.1.0), accompanied by a [publication](https://doi.org/10.21105/joss.06836) in the Journal of Open Source Software (JOSS). A [showcase](/gallery/dune-copasi) of this publication is now featured in the [gallery](/gallery/) on the DUNE website.