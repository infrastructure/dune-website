+++
date = "2016-06-28T18:42:37-05:00"
title = "GSoC mid-term"
+++


![](/img/gsoc/GSoC2016Logo.png)
 Google Summer of Code 2016
 [![Creative Commons License](https://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png)](https://creativecommons.org/licenses/by-nc-nd/3.0/)

We just finished the first half of this year's Google Summer of Code program,
called mid-term. It is used to evaluate both the mentors and the student for Google.
Two projects show great progress, one did not take off and we had to fail the student:

* Xinyun's grid with spline geometries reached a milestone, the B-spline and NURBS
  geometry interfaces are almost done. Checkout the
  [screenshots](http://gsoc2016xinyun.blogspot.de/2016/06/b-spline-and-nurbs-geometry-interface.html)
  with according surfaces. Usually we don't see such smooth output with our methods.
  Blog: [http://gsoc2016xinyun.blogspot.de](http://gsoc2016xinyun.blogspot.de)
  Code repository: [https://gitlab.dune-project.org/Xinyun.Li/dune-iga](https://gitlab.dune-project.org/Xinyun.Li/dune-iga)
* Michael's Python bindings for the DUNE grid interface have progressed to far that
  a simple numerical scheme runs. Inspired by the cell-centered finite volume method
  described in the DUNE grid how-to, Michael
  [re-implemented the scheme](http://misg.github.io/gsoc2016/dune/2016/06/05/two-weeks-later)
  in Python using DUNE grids via his bindings.
  Blog: [http://misg.github.io/](http://misg.github.io)
  Code repository: [https://gitlab.dune-project.org/michael.sghaier/dune-corepy](https://gitlab.dune-project.org/michael.sghaier/dune-corepy)
* The third project aiming to implement dynamic load-balancing for UGGrid saw no major
  development and we had to fail the student.
