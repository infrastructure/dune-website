+++
date = "2018-06-28"
title = "dune-functions 2.6.0"
tags = [ "releases", "dune-functions" ]
+++

A new version of the [dune-functions][] module has been released.
The new _dune-functions_ 2.6.0 release is compatible
with the 2.6 releases of the Dune [core modules][]
and the [dune-typetree][] module.

The [dune-functions][] module defines and implements
interfaces for functions and function space bases,
providing a middle layer between the Dune [core modules]
and discretization modules like [dune-pdelab](/modules/dune-pdelab/) and
_dune-fufem_.

You can get the code by cloning the [git repository][dune-functions git]
or downloading the [source archive][archive].
The 2.6.0 release can be obtained by checking out the
`v2.6.0` tag in this repository via git. For changes in this
version please refer to the [CHANGELOG.md][] file contained
in the repository.

[core modules]: https://dune-project.org/groups/core
[dune-typetree]: https://dune-project.org/modules/dune-typetree
[dune-functions]: https://dune-project.org/modules/dune-functions
[dune-functions git]: https://gitlab.dune-project.org/staging/dune-functions
[archive]: https://gitlab.dune-project.org/staging/dune-functions/-/archive/v2.6.0/dune-functions-v2.6.0.tar.gz
[CHANGELOG.md]: https://gitlab.dune-project.org/staging/dune-functions/blob/v2.6.0/CHANGELOG.md
