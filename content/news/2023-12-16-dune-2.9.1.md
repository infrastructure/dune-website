+++
date = "2023-12-16"
title = "DUNE 2.9.1 released"
tags = [ "releases", "core" ]
+++

The 2.9.1 release is now available.

You can download the tarballs (
[dune-common](https://dune-project.org/download/2.9.1/dune-common-2.9.1.tar.gz),
[dune-geometry](https://dune-project.org/download/2.9.1/dune-geometry-2.9.1.tar.gz),
[dune-grid](https://dune-project.org/download/2.9.1/dune-grid-2.9.1.tar.gz),
[dune-localfunctions](https://dune-project.org/download/2.9.1/dune-localfunctions-2.9.1.tar.gz),
[dune-istl](https://dune-project.org/download/2.9.1/dune-istl-2.9.1.tar.gz)
) or checkout the `v2.9.1` tag via Git.
