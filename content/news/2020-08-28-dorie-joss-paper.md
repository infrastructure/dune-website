+++
date = "2020-08-28"
title = "DORiE v2.0 Release and JOSS Publication"
tags = [ "publications"]
+++

The recently released version 2.0 of [DORiE](/modules/dorie) was published in
the [Journal of Open Source Software (JOSS)][joss].
The latest stable version adds a passive transport solver to the existing
Richards solver and includes several options for finite volume (FV) and
discontinuous Galerkin (DG) discretization schemes.

[joss]: https://doi.org/10.21105/joss.02313
