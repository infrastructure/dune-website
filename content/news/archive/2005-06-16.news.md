+++
date = "2005-06-16"
title = "Automated Test Environment"
+++

From today on we also have an Automated Test Environment running. The system is integrated into our Automake/Autoconf infrastructure. Every night an automatic test is run on *hal*. In the future CVS HEAD check and nightly builds on a computer farm can follow.

*Update: we are in a transition and currently the automated tests are not used anymore*
