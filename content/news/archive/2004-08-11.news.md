+++
date = "2004-08-11"
title = "YaspGrid - reference implementation of the parallel grid interface"
+++

Finally the parallel grid interface, as we discussed it on the last
DUNE Developer Meeting, is finished. There is a reference implementation
in YaspGrid.

If you want to use the parallel interface you must have an MPI
implementation available (automake can detect mpich and lam). To learn
how to compile your DUNE project with MPI support have a look at
`dune-tutorial/Makefile.am`.

*Update: This last statement is outdated!*
