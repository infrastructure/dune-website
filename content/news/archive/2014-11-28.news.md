+++
date = "2014-11-28"
title = "The Dune 2.3.1 release is now available in Debian wheezy-backports"
+++

Which makes it much easier to install for those running the current Debian stable distribution. Enjoy!

Download from [Debian wheezy-backports](http://backports.debian.org/Instructions/).
