+++
date = "2012-01-26"
title = "New UG Patch File Brings Partial Support for 2d Edge Communication"
+++

We have released a new version of the [UG patch file][]. The new patch level number is "7". The new patch brings partial support for edge communication in 2d meshes. 'Partial' because so far edge communication for 2d grids only works on unrefined grids. Hopefully, more will be coming in the future.

The new patch fixes a few linker-related problems. If you are using the svn trunk of dune-grid together with UGGrid in parallel, then you have to update your UG installation, or you will experience linker errors.
