+++
date = "2013-07-19"
title = "Dune on imaginary.org"
+++

Check out [dune-ash](http://imaginary.org/program/dune-ash), a simulator for the distribution of vulcano ash in the atmosphere, which made it to [imaginary.org](http://imaginary.org)!
