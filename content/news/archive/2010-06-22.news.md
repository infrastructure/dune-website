+++
date = "2010-06-22"
title = "Packages for Debian available"
+++

Finally we have gotten around to packaging the Dune core modules for Debian. Installation of Dune on Debian machines now gets a lot easier! You can download the modules ~~here~~.

The packages contain the Dune 2.0 release. They are built for Debian Squeeze on a i386 architecture. Please give them some testing. Snapshot packages and packages for other architectures may appear later.
