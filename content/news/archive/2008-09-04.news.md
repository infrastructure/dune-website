+++
date = "2008-09-04"
title = "New ALUGrid Version"
+++

ALUGrid version 1.12 has been released. METIS 5.0 and PARTY 1.99 are now supported. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
