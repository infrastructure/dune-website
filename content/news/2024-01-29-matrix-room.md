+++
date = "2024-01-29"
title = "A Matrix room for DUNE"
tags = [ "users" ]
+++

We have a new communication channel.  There is now a dedicated chat room
on [matrix.org](https://matrix.org) for all topics related to DUNE.  You can join the room
via [this link](https://matrix.to/#/#dune-project:matrix.org), or get
somebody to invite you.

Matrix is an open federated protocol for real-time communication.
[Many institutions](https://doc.matrix.tu-dresden.de/en/why/)
provide their staff with a matrix identity nowadays.
If your institution is not on the list, ask them for a matrix homeserver.
While you wait, you can get free accounts from (e.g.) [matrix.org](https://app.element.io/).
