+++
title = "Building from Source"
toc = 1
[menu.main]
parent = "installation"
weight = 2
name = "Building from Source"
+++

Even if you have installed a number of Dune packages using the package manager
of your system, you may still have to build some further parts of Dune
from source, for a number of reasons:

* You need a recent feature that is not in a packaged release yet.
  Then you will most likely have to build all of Dune from source.
* You need one or more extension modules that do not exist in packaged form.
  Then you can mix a package-based installation of the modules that are packaged
  with an installation from source of the others.
* Dune is not a standalone program, but rather a set of libraries.
  Using Dune (in C++) means writing a standalone C++ program that uses the
  Dune libraries for the FE-specific features.  This program may use any build
  system that you like.  However, it is convenient to set it up as yet another
  Dune module.  Then, even if you have installed everything else from packages,
  you still need to follow the instructions on this page to build your own
  program.

__Note__: The process described below works for Linux/UNIX based systems. On Windows systems
follow the setup of a [WSL](/installation/installation-windows-wsl/) and then proceed as described below.

## Dependencies
In order to build DUNE you need at least the following software:

* A C++ compiler, e.g., [g++](https://gcc.gnu.org/) or [clang](https://clang.llvm.org/)
* [CMake](https://cmake.org)
* [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)

Detailed information on supported compiler and CMake versions can be found in the
[release notes](/releases/) for releases and in the list [recent changes](/dev/recent-changes/)
for the development branch.

The requirements listed above are the bare minimum. A number of features only get
enabled if additional software is present.  Some examples are

* MPI (e.g. [OpenMPI](https://www.open-mpi.org/) or [MPICH](https://www.mpich.org/))
  for distributed grids
* [SuiteSparse](https://people.engr.tamu.edu/davis/suitesparse.html) for efficient
  direct sparse linear solvers
* [Doxygen](https://www.doxygen.nl/index.html) for building documentation locally.
* etc.

During the configuration process, each module prints a list of the optional dependencies
which it has found or not found. Consult this list for further software that may be
of interest to you.

## Getting the sources

First you need to download the DUNE core modules to your computer in one common directory. You can either download [tarballs of the last releases of the core modules](/releases/) or download [directly from our git repositories](/groups/core/).  For the latter you need to have the
[git](https://git-scm.com/) version control software installed.

*Note*: If you download the modules directly from git make sure to checkout a release branch if you don't want to work with the current development version.

For instance, create a directory and clone the core modules:
````
mkdir dune; cd dune
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
````

For convenience Peter Bastian maintains a set of scripts that automates
the downloading and building of a particular set of Dune modules on Linux.
You can get the script by cloning his `git` repository
````
git clone https://conan.iwr.uni-heidelberg.de/git/peter/dune-installer.git
````
See the `README.md` file there for further information.


## Building DUNE modules

To compile the modules DUNE has to check several components of your system and whether prerequisites within the modules are met. For the ease of users we have designed a custom utility on top of CMake, `dunecontrol`. This can be found in the directory `dune-common/bin/`.

The typical way to run `dunecontrol` is using an options file and execute:
````
./dune-common/bin/dunecontrol --opts=config.opts all
````
This will configure and build all modules you have downloaded in the order of resolved dependencies.

The `config.opts` file contains, e.g.,
````
CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release"
````
See below for more details on the options file.


More generally, you can separately run

```
./dune-common/bin/dunecontrol [OPTIONS] cmake [CMAKE_FLAGS]
./dune-common/bin/dunecontrol [OPTIONS] make [MAKE_FLAGS]
```
in order to first configure and then build all modules with the given flags.

From now on, we assume that `dunecontrol` is in your `PATH`, e.g., by running

```
echo 'export PATH=/path/to/dune-common/bin:${PATH}' >> ${HOME}/.bashrc
source ${HOME}/.bashrc
```

### The Python bindings

By default, the Python bindings are built as well when you follow the instructions above.
If you do not want them, pass `-DDUNE_ENABLE_PYTHONBINDINGS=NO` to CMake.
Read the [Enabling Python](/installation/installation-pythonbindings/) page to learn
how to start using the Python bindings.

__Warning:__ By default, the DUNE build system builds a _static library_ for each
module.  This works nicely for C++, but when using the Python bindings you
absolutely want to have _shared libraries_.  Otherwise, you may experience
obscure crashes or wrong results, in particular when using `UGGrid`.
Therefore, when building DUNE from source for use of the Python bindings,
always set the `-DBUILD_SHARED_LIBS=ON` CMake option.


### Building other DUNE modules
Besides the core modules you can find lots of other dune modules:

- [Grid Modules](/groups/grid/)
- [Discretization Modules](/groups/disc/)
- [Extension Modules](/groups/extension/)
- [Tutorial Modules](/groups/tutorial/)

If you want to use one of those modules make sure to download the module and all dependencies in the same common directory as the DUNE core modules. Building your modules is done in the same way as building the core modules by calling `dunecontrol`.

For further information on how to configure and build Dune modules
please refer to the [Build System documentation](/doc/buildsystem/).
