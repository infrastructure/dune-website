+++
title= "External Libraries"
[menu.main]
parent = "installation"
weight = 6
name = "External Libraries"
+++
There are various external libraries that you can use together with DUNE. These include grid managers, file format libraries, fast linear algebra packages and more. On this page we list some information how to configure DUNE to find these packages.

## Finding External Libraries 
For configuring the build environment, we use [CMake](https://cmake.org). Libraries and other external dependencies are configured using the [`find_package`](https://cmake.org/cmake/help/latest/command/find_package.html) mechanism. Here we explain some basic scenarios how external packages can be found.
First of all, when you run `dunecontrol`, see [installation notes](/installation/installation-buildsrc), internally CMake is invoked. At the end of the configuration of each module a list of found and not-found optional and required packages is shown. If something is not found, you will get a first hint what was missing in the CMake output above the summary.

### System packages 
Most of the libraries stated as optional (or required) dependencies to DUNE are available as system packages on many recent Linux distributions. In the nightly system tests we use packages provided by [Debian](https://www.debian.org/distrib/packages) and [Ubuntu](https://packages.ubuntu.com), but also other distributions may provide the relevant libraries. So, if you do not need a special version of a library, just look into the package repository provided by your system.

System libraries should be detected without further configuration by the user. No need to specify any paths or flags to find these packages. If a system package, for some reason, is not found in your distribution, please file a [bug report](/dev/issues/) that includes the information about your system, the DUNE version, how you have obtained it, and the steps you have performed to configure.

Packages available as system libraries in Debian based Linux distributions include: [Alberta](https://gitlab.mathematik.uni-stuttgart.de/ians-nmh/alberta/alberta3), [ARPack(++)](https://github.com/m-reuter/arpackpp), [BLAS](http://www.netlib.org/blas/), [Eigen3](https://eigen.tuxfamily.org/), [GMP(++)](https://gmplib.org/), [LAPACK](http://www.netlib.org/lapack/), [METIS](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview), [MPI](https://www.mpi-forum.org/), [ParMETIS](http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview), [PSurface](https://github.com/psurface/psurface), [(PT)Scotch](https://gitlab.inria.fr/scotch/scotch), [SuiteSparse](https://people.engr.tamu.edu/davis/suitesparse.html), [SuperLU](https://portal.nersc.gov/project/sparse/superlu/), [TBB](https://github.com/oneapi-src/oneTBB), [Vc](https://github.com/VcDevel/Vc), [ZLib](https://zlib.net/)

### Packages Providing a Configuration File
Some external packages provide a configuration file that describes how to use these libraries. In order to configure the packages to be found based on this configuration file, you need to tell CMake where to find the file.

There are packages that provide a CMake config file, typically named either `<Package>Config.cmake` or `<package>-config.cmake`. By passing an argument to CMake one can make the path to this file available. This argument is of the form `-D<Package>_DIR=<path/to/package/config>` where the path must point directly to the directory containing the config file. There are two alternatives to passing directly the directory: 1. passing a prefix path as argument `-D<Package>_ROOT=<path/to/package/prefix>`, 2. adding the prefix path to the environmental variable `CMAKE_PREFIX_PATH`

The CMake arguments can either be passed directly using `dunecontrol`:

```
dunecontrol [options...] cmake -D<Package>_DIR=<path/to/package/config>
```

Or simply be added to the `CMAKE_FLAGS` variable in the options file.

Some packages provide only a [PkgConfig](https://www.freedesktop.org/wiki/Software/pkg-config) configuration in the form of a `<package>.pc` file. This config file can be parsed by CMake to obtain the necessary information about how to configure the package. In order to tell CMake (or pkg-config) where to find that file, you have to add the path to the `.pc` file to the environmental variable `PKG_CONFIG_PATH` or to the `CMAKE_PREFIX_PATH` variable.

### Find Packages using `Find<Package>.cmake` Modules
Some packages neither provide a CMake config file, nor a pkg-config file. For those packages, an inspection of user-provided directories must be performed to extract from the found files the include directories and link flags. This manual extraction is implemented in provided `Find<Package>.cmake` files, typically located within the `cmake/modules/` directory of the DUNE modules. Most of these find modules work similarly, but detailed information about how to provide the paths to the requested libraries are given in the head section of these files as documentation. If the following step do not work for you, have a look into these files, to get further insight.

To tell the find module where to look for your library, typically you have to provide the *root* path of your package, i.e., the path containing the subdirectories `include/` and `lib/`, typically. This path can be provided by setting the `-D<Package>_ROOT=<path/to/package/prefix>` argument. If this does not help, you may set the paths to the include directory and the libraries explicitly. This can often be done by setting `-D<PACKAGE>_INCLUDE_DIRS` and `-D<PACKAGE>_LIBRARIES`. The CMake output about why a package was not found gives a list of variables that are not defined. This is the first hint in what you have to set.
