+++
title = "Installation of Dune from binary packages on Linux systems"
[menu.main]
parent = "installation"
weight = 1
name = "Linux binary packages"
+++

Installing Dune is slightly more complex than installing other finite element
software, because Dune is not a monolithic program.  Instead, it consists
of interdependent components, called _modules_ that have to be installed
separately.  At the very least, you will need the so-called _core modules_,
which are `dune-common`, `dune-geometry`, `dune-grid`, `dune-localfunctions`,
and `dune-istl`. Typically, you will also need the _staging modules_,
which are `dune-uggrid`, `dune-typetree`, and `dune-functions`.
Beyond that, a plethora of further modules offers additional functionality,
e.g., more [grid data structures](/groups/grid/) or [discretization modules](/groups/disc/).

Linux distributions offer various Dune modules as binary packages for convenient download
and installation.  For example, the table on this page shows the packaging status
of `dune-common` in different distributions.
You can expect the other core modules `dune-geometry`, `dune-grid`, `dune-localfunctions`, and
`dune-istl` to be packaged in the same version.

{{< rightpanel noframe >}}
<a href="https://repology.org/metapackage/dune-common">
<img alt="Packaging status"
src="https://repology.org/badge/vertical-allrepos/dune-common.svg" width="100%"></a>
{{< /rightpanel >}}

If you want to use the C++ interface, or if you want to mix binary packages
and modules built from source, then you need to read about how to
[build Dune from source](/installation/installation-buildsrc).

# Package details

The precise naming of the packages differs between distributions.
For example, in Debian-based distributions, the packages are called
`libdune-foo-dev`, where `foo` is a module name like for example
`common`, `grid`, `geometry`, `istl`, or `localfunctions`.
A [complete list of Debian packages](https://packages.debian.org/search?keywords=libdune+dev&searchon=names&suite=all&section=all)
is available.
For development, you may also want the corresponding debug symbols,
which Debian ships in separate packages ending with `-dbgsym`.

Offline documentation is available in the `libdune-foo-doc` packages and installed to `/usr/share/doc/libdune-foo-doc/doxygen/index.html`.


## Installing Dune core modules

To now install the core Dune on a Debian-based system run the following command:

```
sudo apt install libdune-grid-dev libdune-istl-dev libdune-localfunctions-dev
```

This will install the entire set of Dune _core modules_ -- the ones not
explicitly listed will still be installed, because they are dependencies
of the others.

We recommend you also install the packages from the _staging_ set:
```
sudo apt install libdune-uggrid-dev libdune-functions-dev
```

# Helping with packaging

If you want to help with the Debian packaging, you can download the packaging information
for (for example) `dune-common` by

`git clone https://salsa.debian.org/science-team/dune-common.git`
([Browse Repository](https://salsa.debian.org/science-team/dune-common.git))

The Debian package history and the Debian issue tracker can be found in the [Debian Package Tracker](https://tracker.debian.org/pkg/dune-common).

The links for all other available modules are similar.
