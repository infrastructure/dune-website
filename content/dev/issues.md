+++
title = "Issue Tracker"
[menu.main]
parent = "dev"
weight = 4
+++

## GitLab Issue Tracker

All development is coordinated at the [DUNE GitLab instance](https://gitlab.dune-project.org),
which also contains individual issue trackers for each core module:

* [dune-common](https://gitlab.dune-project.org/core/dune-common/issues/)
* [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry/issues/)
* [dune-grid](https://gitlab.dune-project.org/core/dune-grid/issues/)
* [dune-istl](https://gitlab.dune-project.org/core/dune-istl/issues/)
* [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions/issues/)

### Reporting bugs

You may register at [GitLab](https://gitlab.dune-project.org) and open an issue for the tracker of the corresponding module.

Alternatively, if you experience problems with the core modules, you can report issues to the respective modules
by simply sending an email to their service desks:

<table>
    <style>
td {
  text-align:left;
  padding: 5px;
}
th {
  font-weight:bold;
  padding: 5px;
}
    </style>
  <thead>
    <tr>
      <th>Module</th>
      <th>Contact</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>dune-common</td>
      <td><img src="https://img.shields.io/badge/dune--common%40dune--project.org-informational"></td>
    </tr>
    <tr>
      <td>dune-geometry</td>
      <td><img src="https://img.shields.io/badge/dune--geometry%40dune--project.org-informational"></td>
    </tr>
    <tr>
      <td>dune-grid</td>
      <td><img src="https://img.shields.io/badge/dune--grid%40dune--project.org-informational"></td>
    </tr>
    <tr>
      <td>dune-istl</td>
      <td><img src="https://img.shields.io/badge/dune--istl%40dune--project.org-informational"></td>
    </tr>
    <tr>
      <td>dune-localfunctions</td>
      <td><img src="https://img.shields.io/badge/dune--localfunctions%40dune--project.org-informational"></td>
    </tr>
</table>

### Guidelines for bug reporting

If you are new to open-source development, please make yourself
familiar with the [Guidelines for bug reporting](/dev/issues/bug_reporting/).

## Old FlySpray Issue tracker 

DUNE retired its FlySpray bug tracker, we [migrated the old issues to GitLab](https://gitlab.dune-project.org/flyspray/FS/issues).
