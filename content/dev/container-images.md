+++
title = "Container images of Dune installations"
[menu.main]
parent = "dev"
weight = 5
name = "Container Images"
+++

We maintain a collection of [Docker images](https://gitlab.dune-project.org/docker/ci/)
with and without Dune installations.  Their main purpose is to serve as controlled
environments for CI testing of Dune and related downstream modules.
See the [README.md](https://gitlab.dune-project.org/docker/ci/-/blob/master/README.md)
file for further information.
