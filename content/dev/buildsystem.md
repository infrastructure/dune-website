+++
title = "Build System Docs"
layout = "buildsystem"
type = "page"
[menu.main]
parent = "dev"
identifier = "dev/buildsystem"
weight = 5
+++

The Dune Build System documentation is generated with [Sphinx](https://www.sphinx-doc.org/en/master/). This page collects all available documentations.

# DUNE Build System Documentation for Developers

Dune uses [CMake](https://cmake.org/) to configure, build, and install modules in different forms.
To aid developers in setting up their projects and linking them with dune modules we provide a multiple functionalities
in form of CMake functions, macros, and scrpts. The documentation of such utilities can be found in the following
pages:

**Note**: Since version 2.8 we have been having problems with the automatic documentation of CMake modules of the build system.
We are working in a proper solution, in the meantime, we ask you to read the documentation of those modules from the source files located under
the [`cmake/modules/`](https://gitlab.dune-project.org/core/dune-common/-/tree/master/cmake/modules) directory
of the `dune-common` module.