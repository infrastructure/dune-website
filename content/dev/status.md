+++
title = "Build Status"
[menu.main]
parent = "dev"
weight = 6
+++

## Core Modules

<table>
    <style>
td {
  padding: 1px;
}
th {
  text-align:center;
  font-weight:bold;
  padding: 1px;
}
    </style>
  <thead>
    <tr>
      <th>module</th>
      <th>master</th>
      <th>2.10</th>
      <th>2.9</th>
      <th>2.8</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>dune-common</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-common/commits/master"><img src="https://gitlab.dune-project.org/core/dune-common/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-common/commits/releases/2.10"><img src="https://gitlab.dune-project.org/core/dune-common/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-common/commits/releases/2.9"><img src="https://gitlab.dune-project.org/core/dune-common/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-common/commits/releases/2.8"><img src="https://gitlab.dune-project.org/core/dune-common/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
    <tr>
      <td>dune-geometry</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-geometry/commits/master"><img src="https://gitlab.dune-project.org/core/dune-geometry/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-geometry/commits/releases/2.10"><img src="https://gitlab.dune-project.org/core/dune-geometry/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-geometry/commits/releases/2.9"><img src="https://gitlab.dune-project.org/core/dune-geometry/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-geometry/commits/releases/2.8"><img src="https://gitlab.dune-project.org/core/dune-geometry/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
    <tr>
      <td>dune-grid</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid/commits/master"><img src="https://gitlab.dune-project.org/core/dune-grid/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid/commits/releases/2.10"><img src="https://gitlab.dune-project.org/core/dune-grid/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid/commits/releases/2.9"><img src="https://gitlab.dune-project.org/core/dune-grid/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid/commits/releases/2.8"><img src="https://gitlab.dune-project.org/core/dune-grid/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
    <tr>
      <td>dune-istl</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-istl/commits/master"><img src="https://gitlab.dune-project.org/core/dune-istl/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-istl/commits/releases/2.10"><img src="https://gitlab.dune-project.org/core/dune-istl/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-istl/commits/releases/2.9"><img src="https://gitlab.dune-project.org/core/dune-istl/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-istl/commits/releases/2.8"><img src="https://gitlab.dune-project.org/core/dune-istl/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
    <tr>
      <td>dune-localfunctions</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-localfunctions/commits/master"><img src="https://gitlab.dune-project.org/core/dune-localfunctions/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-localfunctions/commits/releases/2.10"><img src="https://gitlab.dune-project.org/core/dune-localfunctions/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-localfunctions/commits/releases/2.9"><img src="https://gitlab.dune-project.org/core/dune-localfunctions/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-localfunctions/commits/releases/2.8"><img src="https://gitlab.dune-project.org/core/dune-localfunctions/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
  </tbody>
</table>

## Staging Modules

<table>
  <thead>
    <tr>
      <th>module</th>
      <th>master</th>
      <th>2.10</th>
      <th>2.9</th>
      <th>2.8</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>dune-functions</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-functions/commits/master"><img src="https://gitlab.dune-project.org/staging/dune-functions/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-functions/commits/releases/2.10"><img src="https://gitlab.dune-project.org/staging/dune-functions/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-functions/commits/releases/2.9"><img src="https://gitlab.dune-project.org/staging/dune-functions/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-functions/commits/releases/2.8"><img src="https://gitlab.dune-project.org/staging/dune-functions/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
    <tr>
      <td>dune-typetree</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-typetree/commits/master"><img src="https://gitlab.dune-project.org/staging/dune-typetree/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-typetree/commits/releases/2.10"><img src="https://gitlab.dune-project.org/staging/dune-typetree/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-typetree/commits/releases/2.9"><img src="https://gitlab.dune-project.org/staging/dune-typetree/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-typetree/commits/releases/2.8"><img src="https://gitlab.dune-project.org/staging/dune-typetree/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
    <tr>
      <td>dune-uggrid</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-uggrid/commits/master"><img src="https://gitlab.dune-project.org/staging/dune-uggrid/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-uggrid/commits/releases/2.10"><img src="https://gitlab.dune-project.org/staging/dune-uggrid/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-uggrid/commits/releases/2.9"><img src="https://gitlab.dune-project.org/staging/dune-uggrid/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-uggrid/commits/releases/2.8"><img src="https://gitlab.dune-project.org/staging/dune-uggrid/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
  </tbody>
</table>

## Extension Modules

<table>
  <thead>
    <tr>
      <th>module</th>
      <th>master</th>
      <th>2.10</th>
      <th>2.9</th>
      <th>2.8</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>dune-grid-glue</td>
      <td><a href="https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/master"><img src="https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/master/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/releases/10.9"><img src="https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/releases/2.9"><img src="https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/releases/2.9/pipeline.svg" alt="status"></a></td>
      <td><a href="https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/releases/2.8"><img src="https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/releases/2.8/pipeline.svg" alt="status"></a></td>
    </tr>
  </tbody>
</table>
