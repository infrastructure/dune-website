+++
title = "Coding Style"
[menu.main]
parent = "guides"
weight = 2
+++

## Coding Style
In order to keep the code maintainable we have decided upon a set of coding rules. Some of them may seem like splitting hairs to you, but they do make it much easier for everybody to work on code that hasn't been written by oneself. We recognize that these rules are not honored in all places. In most cases, these naming rule violations are considered bugs, but there are some exceptions.

So here they are...

### Code Formatting:

 * Indentation: Indentation happens using spaced only, the indentation depth is two per level. As different people have different default settings for this, it is good practice to place editor hints at the beginning of new files. Just start any new file with

    ```
    // -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
    // vi: set et ts=4 sw=2 sts=2:
    ```
    The indentation and many other formatting rules are formalized in a configuration file [dune-uncrustify.cfg](/share/dune-uncrustify.cfg) for the [uncrustify](http://uncrustify.sourceforge.net/) tool. You can format new files using this tool by calling

    ```
    uncrustify -l CPP -c dune-uncrustify.cfg <FILENAME>
    ```

* <b>Trailing Whitespace:</b> Source files may not contain trailing whitespace to reduce the amount of noise in diffs and during merges.
* <b>Automatic Enforcement:</b> In contrast to the remainder of the
  coding style guidelines, these code formatting rules are (partially)
  enforced automatically with a pre-commit hook. Due to the
  distributed nature of Git, this hook can only check your commits
  once they arrive in the central repository, so it is important to
  make your local Git repository check your commits as well. The
  dunecontrol script will automatically install such a pre-commit hook
  for you - see the [Whitespace Rules](/dev/whitespace_hook/) for more information.

### Naming

* <b>Variables:</b> Names for variables should only consist of letters and digits. The first letter should be a lower case one. If your variable names consists of several words, then the first letter of each new word should be capital. As we decided on the only exception are the begin and end methods.
* <b>Acronyms:</b> Capitalization never changes within acronyms. Hence `return ABC()` and `abcSize()` are valid method names, but `return Abc()` is not.
* <b>Private Data Variables:</b> Names of private data variables end with an underscore.
* <b>Typenames:</b> For typenames, the same rules as for variables apply. The only difference is that the first letter should be a capital one. Types exported by other types never end with a Type-suffix.
* <b>Macros:</b> The use of preprocessor macros is strongly discouraged. If you have to use them for whatever reason, please use capital letters only.
* <b>The Single-Inclusion Macro:</b> Every header file traditionally begins with the definition of a preprocessor constant that is used to make sure that each header file is only included once (the single-inclusion macro). To avoid name clashes, this macro should encode the file name together with the entire path of that file within the Dune module. Slashes are replaced by underscores, and capital letters are used exclusively. For example, if your header file is `'dune/mymodule/common/myheaderfile.hh'`, this constant should be DUNE_MYMODULE_COMMON_MYHEADERFILE_HH.
* <b>Files:</b> Filenames should consist of lower case letters exclusively. Header files get the suffix .hh, implementation files the suffix .cc

There are a few classes which are exempt from these rules. Those are classes that directly implement a concept from the standard library (e.g., container classes). Unlike demanded above, such classes are to follow the STL naming style exclusively, to allow to use them directly in STL contexts. To mark such classes, they are to live in a separate namespace `Dune::Std`.

### Documentation
Dune, as any software project of similar complexity, will stand and fall with the quality of its documentation. Therefore it is of paramount importance that you document well everything you do! We use the doxygen system to extract easily-readable documentation form the source code. Please use its syntax everywhere. In particular, please comment all

* Method Parameters
* Template Parameters
* Return Values
* Exceptions thrown by a method

Since we all know that writing documentation is not well-liked and is frequently deferred to some vague 'next week', we herewith proclaim the **Doc-Me Dogma** . It goes like this: Whatever you do, and in whatever hurry you happen to be, please document everything at least with a /** \todo Please doc me! */. That way at least the absence of documentation is documented, and it is easier to get rid of it systematically.

### Exceptions
The use of exceptions for error handling is encouraged. There is a variety of Dune-specific exceptions you can throw. They all derive (possibly indirectly) from the class `Dune::Exception` in dune-common.

### Output
The header `stdstreams.hh` defines several output streams for messages of varying urgence. Depending on the desired verbosity of the code these can be switched on and off by special options of the 'configure' command.

### Debugging Code
Global debugging code is switched off by setting the symbol NDEBUG. In particular, all asserts are automatically removed. Use those asserts freely!

## Name issues that may require non-compatible changes
Over the years we have acquired various inconsistencies in our naming of different things. This makes Dune unprofessionally-looking and more difficult to use for beginners. Here is a list of things that may need to be rectified and codified:

* We need a namespace for each dune module. How should the names for these namespace be?
* In some core modules a mixture of CamelCase and stl-style naming is used. A part of this comes from an attempt to maintain stl compatibility. Can we switch the code that is not intended to be stl compatible over to Dune (i.e., camel-case) naming? Or should we switch over to stl-naming altogether?
* Naming of the files that contain unit tests. Should they be named `testfoo.cc` or `footest.cc` or something else?
* What is the name of directory inside the 'dune' directory of a module? Is it always the module name without the 'dune-' prefix?
