+++
title = "Doxygen Best Practices"
+++

## Doxygen Best Practice
### Useful tips when documenting your code

#### correct #include statements
Doxygen tends to strip the while path from the include statements. In Dune we usually require include statements like

```#include <dune/common/fvector.hh>```

Doxygen will just write

```#include <fvector.hh>```

In order to trick/force Doxygen to write the correct include statement you have to adjust your Doxyfile.in as follows:

1. use @top_srcdir@/dune/... to point to the include directories:

    `INPUT = @srcdir@/mainpage @srcdir@/modules @top_srcdir@/dune/common/modules @top_srcdir@/dune/common`

2. set the STRIP_FROM_INC_PATH to point to your abs_top_srcdir:

    `STRIP_FROM_INC_PATH    = @abs_top_srcdir@`


#### \copydoc \<link-object\>

Use `\copydoc` in your derived class to reuse the documentation from the interface class.

Copies a documentation block from the object specified by `<link-object>` and pastes it at the location of the command. This command can be useful to avoid cases where a documentation block would otherwise have to be duplicated or it can be used to extend the documentation of an inherited member.

The link object can point to a member (of a class, file or group), a class, a namespace, a group, a page, or a file (checked in that order). Note that if the object pointed to is a member (function, variable, typedef, etc), the compound (class, file, or group) containing it should also be documented for the copying to work.

To copy the documentation for a member of a class for instance one can put the following in the documentation

#### Ordering Modules

Use the file `doxygen/modules` to change the ordering of modules on the modules page

Doxygen sorts the modules in the order it finds their definition. This is influenced by the order doxygen reads the source files and by the include statements in these source files.

The modules file is read right after the mainpage, before any source files are parsed. Entering a `@defgroup` statement makes this module known to doxygen so that all modules mentioned in the modules file are listed first and in the order they are defined there.
