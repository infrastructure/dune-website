Dune user meeting 2010
======================

This is a place for stuff concerning the Dune user meeting 2010, which
took place in Stuttgart.

Program
-------

### Wednesday, October 6th

-   **R. Klöfkorn**: *Opening*
    ([slides](/pdf/meetings/2010-10-usermeeting/kloefkorn_robert_opening.pdf))
-   **M. Drohmann**: *Computing reduced basis solutions with an
    implementation interface connecting DUNE with Matlab*
    ([slides](/pdf/meetings/2010-10-usermeeting/dune_rb_martin_drohmann.pdf))
-   **S. Müthing**: *Solving multi-domain problems with PDELab and
    dune-multidomain*
    ([slides](/pdf/meetings/2010-10-usermeeting/dune-multidomain.pdf))
-   **J. Youett**: *An implementation of biomechanics via DUNE*
-   **Discussion**: *User expectations from and contributions to DUNE*
    (results: [slides](/pdf/meetings/2010-10-usermeeting/discussion_dum2010.pdf))
-   **R. Klöfkorn**: *Higher order adaptive and parallel simulations
    with DUNE*
-   **P. Henning**: *The heterogeneous multiscale finite element method
    and its implementation using DUNE*
    ([slides](/pdf/meetings/2010-10-usermeeting/henning_patrick_dum2010.pdf))

### Thursday, October 7th

-   **A. Lauser**: *The DuMux material law framework*
    ([slides](/pdf/meetings/2010-10-usermeeting/lauser_andreas_dum2010.pdf))
-   **B. Flemisch**: *Upscaling of porous media flow with DuMux and
    dune-multidomaingrid*
    ([slides](/pdf/meetings/2010-10-usermeeting/flemisch_bernd_dum2010.pdf))
-   **M. Nolte**: *Performance pitfalls to be avoided in the DUNE grid
    interface*
    ([slides](/pdf/meetings/2010-10-usermeeting/nolte_martin_dum2010.pdf))
-   **C. Waluga**: *Hybridized DG methods: theory and implementation in DUNE*
    ([slides](/pdf/meetings/2010-10-usermeeting/waluga_christian_dum2010.pdf))
-   **C. Grüninger**: *Using DUNE and PDELab for the diploma thesis as a
    student*
    ([slides](/pdf/meetings/2010-10-usermeeting/grueninger_christoph_dum2010.pdf))
-   **W. Giese**: *Use of DUNE on a Linux-Cluster with a biological
    application*
-   **C. Nagaiah**: *Optimal control of the reaction-diffusion equations
    in electrophysiology*
-   **S. Götschel**: *State trajectory compression for optimal control
    with parabolic PDEs*
    ([slides](/pdf/meetings/2010-10-usermeeting/goetschel_sebastian_dum2010.pdf))
-   **A. Dedner**: *Shape functions based on the generic reference elements*
    ([slides](/pdf/meetings/2010-10-usermeeting/dedner_andreas_dum2010.pdf))

### Friday, October 8th

-   **C. Gersbacher**: *The dune-prismgrid module*
    ([slides](/pdf/meetings/2010-10-usermeeting/gersbacher_christoph_dum2010.pdf))
-   **A. Rasmussen**: *Usage of Dune at SINTEF Applied Mathematics*
    ([slides](/pdf/meetings/2010-10-usermeeting/rasmussen_atgeirr_dum2010.pdf))
-   **M. Schenke**: *On the Analysis of Porous Media Dynamics using a
    DUNE-PANDAS Interface*
    ([slides](/pdf/meetings/2010-10-usermeeting/schenke_maik_dum2010.pdf))

Participants
------------

Here is a list of the participants (the corresponding eMail addresses can
be found in the [[dum-2010-pictures:|Private]] project, which is for the
participants only).

-   Albrecht, Felix (*Universität Münster*)
-   Dedner, Andreas (*University of Warwick*)
-   Drohmann, Martin (*Universität Münster*)
-   Flemisch, Bernd (*LH2, Universität Stuttgart*)
-   Flornes, Kristin (*IRIS Bergen*)
-   Gersbacher, Christoph (*Universität Freiburg*)
-   Giese, Wolfgang (*Humboldt-Universität zu Berlin*)
-   Götschel, Sebastian (*Zuse Institute Berlin*)
-   Grüninger, Christoph (*Universität Stuttgart*)
-   Henning, Patrick (*Universität Münster*)
-   Kaulmann, Sven (*Universität Münster*)
-   Klöfkorn, Robert (*Universität Freiburg*)
-   Kronsbein, Cornelia (*Fraunhofer ITWM, Kaiserslautern*)
-   Lauser, Andreas (*LH2, Universität Stuttgart*)
-   Meister, Oliver (*IPVS, Universität Stuttgart*)
-   Müthing, Steffen (*VIS, Universität Stuttgart*)
-   Nagaiah, Chamakuri (*Karl-Franzens-Universität Graz*)
-   Nikolaenko, Dmitry (*Karl-Franzens-Universität Graz*)
-   Nolte, Martin (*Universität Freiburg*)
-   Rahnema, Kaveh (*IPVS, Universität Stuttgart*)
-   Rasmussen, Atgeirr (*SINTEF Oslo*)
-   Sævareid, Ove (*IRIS Bergen*)
-   Schenke, Maik (*MIB, Universität Stuttgart*)
-   Tatomir, Alex (*LH2, Universität Stuttgart*)
-   Waluga, Christian (*RWTH Aachen*)
-   Youett, Jonathan (*Freie Universität Berlin*)
-   Zinatbakhsh, Seyedmohammad (*MIB, Universität Stuttgart*)
