Dune Developer Meeting 2010
===========================

This wiki is about the 2010 Dune developer meeting in Münster.

Protocol
--------

The original protocol can be found [here](protocol_de) (in german),
an english version is available [here](protocol_en).

Agenda
------

### Monday, 22.11.2010

-   **14:00-14:20** Welcome
    -   Agenda [Proposals](proposals)
    -   [Dune User Meeting 2010](/community/meetings/2010-10-usermeeting/) results
    -   Votes on new developers, access rights, permissions.
-   **14:20-19:00** Discussion
    -   [Interface Extensions](proposals#2-interface-extensions)
    -   [Gridfactory Interface](proposals#3-gridfactory-interface)
    -   [Performance Issues](proposals#1-performance-issues)
-   **19:00-open** Dinner

### Tuesday, 23.11.2010

-   **09:00-12:45** Discussion
    -   [dune-localfunctions](proposals#4-dune-localfunctions)
-   **12:45-13:30** Lunch (provided)
-   **13:30-16:00** Discussion
    -   [Flyspray tasks](proposals#5-flyspray-tasks)
    -   [Oranisational](proposals#6-organisation)

Participants
------------

-   **Felix Albrecht**
-   **Peter Bastian**
-   **Markus Blatt**
-   **Andreas Dedner**
-   **Martin Drohmann**
-   **Christian Engwer**
-   **Jorrit Fahlke**
-   **Carsten Gräser**
-   **Olaf Ippisch**
-   **Ole Klein**
-   **Robert Klöfkorn**
-   **Rebecca Neumann**
-   **Mario Ohlberger**
-   **Oliver Sander**
-   **Jonathan Youett**

Date
----

Mon. 22.11.2010 14:00 - Tue. 23.11.2010 17:00

Places
------

### Meeting

[maps.google.de](http://maps.google.de/maps/ms?ie=UTF8&hl=de&t=h&msa=0&msid=108933731868623871043.0004930f3d3977c1efdc5&ll=51.965251,7.601643&spn=0.001501,0.004823&z=18)

The meeting will take place in **Room 230** in our institute *Institut
für Numerische und Angewandte Mathematik*. You can find it on the third
(american) or second (german) floor in building **Orleansring 10, 48149
Münster**.

On the map you can find the entrance to the building (since it is a new
building, it is not yet visible on the satellite pictures). If you are
taking a **cab**, it is probably easier to direct the driver to
**Einsteinstr. 64, 48149 Münster**, the main Math building, which you
will also find on the map.

If you are taking the **bus** from *Münster Hauptbahnhof* (Münster main
station) to *Coesfelder Kreuz* (nearest bus station, see map), take one
of the following buses:

-   **4**, direction *Alte Sternwarte*
-   **5**, direction *Hannaschweg*
-   **12**, direction *Heekweg*
-   **13**, direction *Eissporthalle*
-   **R63**, direction *Nottuln, Rhodeplatz*

There are probably more that i do not know about. You can search for
yourself on the homepage of the
[VRR](http://www.vrr.de/en/fahrplanauskunft/index.html) by entering "ms"
in the *Enter Town* field, "hbf" in the *Enter origin* field, and
"Coesfelder Kreuz" in the *Enter destination* field.

If you are coming by **car**, you should set your navigational system to
**Einsteinstr. 64, 48149 Münster**. Parking can be found directly behind
the building (see map).

### Dinner at La Casa

[maps.google.de](http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=108933731868623871043.0004930f3d3977c1efdc5&ll=51.967843,7.609888&spn=0.003001,0.009645&z=17)

La Casa\
Wilhelmstrae 26\
48149 Münster\
Tel.: 0251 25962

### Hotel am Schlopark

[maps.google.de](http://maps.google.de/maps/ms?ie=UTF8&hl=de&msa=0&msid=108933731868623871043.0004930f3d3977c1efdc5&ll=51.967843,7.609888&spn=0.003001,0.009645&z=17)

Hotel am Schlopark\
Schmale Strae 2\
48149 Münster\
Tel.: 0251 89982
