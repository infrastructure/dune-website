Protokoll des Dune Developer Meetings 2010
==========================================

Dune User Meeting
-----------------

-   Abhängigkeiten von benötigten tools reduzieren
-   alle WML Seiten nach dune web verschieben
-   Ziel soll Paketerstellung sein
    -   evt. Hiwi anstellen
    -   evt. automatisches Services nutzen für verschiedene
        Distributionen
    -   Pakete dürfen gerne auch von Usern eingereicht werden
        (Möglichkeit in der Wiki bereitstellen)
-   Buildsystem prinzipielle unverändert zu lassen, bis jemand
    *wirklich* daran arbeiten möchte
-   Buildsystem liee sich vereinfaachen, falls man eine der beiden
    Alternativen (installiert/nicht installiert verwenden) nicht mehr
    unterstützen würde (wobei Installation für Pakete z.B. nötig)
-   Wiki Eintrag zu nötigen Voraussetzungen, wenn man bugs einreichen
    will
-   evt. deprecated Funktionalität aus fem übernehmen
-   feature list soll im Wiki erarbeitet werden (auch von Usern), dann
    ins doxygen, dann auf der homage verlinken
    -   z.B. auch für localfunctions
    -   zum release auf die offizielle homepage
-   eigene mailingliste für die Entwickler, flyspray dann auch darüber
-   inkrementelle Tests (Gitter) wäre schön als Ausbau des
    grid-dev-howto s
-   es werden keine anstregungen in richtung openmp unternommen (kein
    Performance Gewinn ersichtlich), zusätzliche Informationen bzgl.
    thread safety in die doku
-   Löser auf GPUs dürfen gerne eingereicht werden

Votes on new developers, access rights, permissions
---------------------------------------------------

-   Carsten Gräser wird ohne Gegenstimmen Core Developer
-   Liste aller patches/Mitarbeiter auf die homepage

Performance issues
------------------

### EntityKey

-   soll gemacht werden, neuer Name: **EntitySeed**
-   default Konstruktor der EntityPointer zurück gibt, bis Gegenbeispiel
    kommt
-   Methode zum Bekommen auf der Entity:
    `EntitySeed<Codim, GridImp> Entity.seed()`
-   Methode zur Rückgewinnung auf dem Gitter:
    `Grid.entityPointer<Codim>( seed )`
-   Exportiert Typen:
    -   `Grid`
    -   `Entity`
    -   `codimension`
-   Methode `compactify()` deprecaten

### Entity (pointer)

-   erstmal übersprungen

### Copy vs. Reference

-   soll ab dem nächsten Release schrittweise umgesetzt werden
-   es soll ein Benchmark erstellt werden, der den Performancegewinn
    zeigt (in einem eigenen Branch), bzw. keinen Performanceverlust
    zeigt
    -   z.B. Finite Volumen Beispiel mit den beiden verschiedenen
        Interfaces testen
    -   bisheriger Overhead, ca. 20 Prozent
        -   bei ein drittel Overhead: wird genommen **Martins
            Präsentation ergibt Overhead von 20 statt 40 Prozent**,
            siehe
            [hier](/pdf/meetings/2010-11-devmeeting/cube-geo-rel.pdf)
        -   bei zwei drittel Overhead: genauer angucken
        -   bei leichem Overhead: wird nicht genommen
-   langfristig Refcount für LeafIndexSet
-   Jacobian, etc durch ein MatrixInterface nach dem nächsten Release
    ersetzen *ohne Gegenstimmen angenommen*
    -   alle const Methoden aus DenseMatrix (ohne operator\[\])

### Semantics of method contains()

-   undefined, wenn Entity nicht Element des Grids

Interface cleanup
-----------------

### GenericReferenceElement

-   ab Dune 2.1 nur noch Generic
-   ab Dune 2.2 Generic deprecated, auch neue
-   ab Dune 2.3 keine Generic mehr

### GeometryType vs. topologyId

-   beide sollen in GeometryType vereint werden
-   Methode `id()` liefert topologyId
-   Methode `basicType()` erweitern um *nicht darstellbar*

### Methoden auf dem Referenzelement

-   `initializeTopology()` protected
-   `initialize()` protected
-   `checkInside( const FieldVector< ctype, dim-codim > &local, int i )`
    deprecated
-   alle `global()` deprecated

Interface extensions
--------------------

### Facilitate implementation/testing of new features

-   es wird ein define EXPERIMENTAL\_GRID\_EXTENSIONS o.ä. eingeführt,
    um Erweiterungen zu testen, z.B. um an `getRealImpl()` zu bekommen
-   diese Flags sollen nach Möglichkeit gesammelt dokumentiert werden
-   diese Flags sollen nicht ins Release

### subIndex interface

-   `subIndex`, `subEntity` und `count()` soll auch auf höhere Codims
    übertragen werden

### Persistent container

-   kommt nach `grid/utilities/`

### I/O

-   neue Klasse: `BackupRestoreFacility`
-   je zwei `backup()`, `restore()` Methoden, einmal mit file/path,
    einmal mit stream
    -   `static void backup( const Grid& grid, const std::string& path, const std::string& fileprefix );`
    -   `static void backup( const Grid& grid, std::ostream& stream );`
    -   `static Grid* restore( const std::string& path, const std::string& fileprefix );`
    -   `static Grid* restore( const std::istream& stream );`
-   `restore()` muss sicher stellen, dass `IndexSet`s und `IdSet`s
    vorher und nachher gleich sind
-   nur Funktionalität bei gleicher Anzahl Rechenknoten garantieren

dune-localfunctions
-------------------

-   Namen werden beibehalten
-   VertexOrdering/Twist Entscheidung später
-   Jö checkt nach localfunctions ein
-   `FiniteElement` für die neuen Sachen,
-   `GlobalValuedFiniteElement` ändern
-   `FaceOrientation` Beispiel dokumentieren
-   für Geometry Informationen Interface von `Dune::Geometry`
-   bei den alten lokalen Finiten Elementen nur noch eine Variante (auf
    lange Sicht)

GridFactory interface
---------------------

### Problems with boundary projection and load balancing/checkpointing

-   Oli denkt sich einen Prototypen als Diskussionsgrundlage
    aus (Domain)
-   Parametrisierung von `insertBoundarySegment()` soll weg
-   Reader soll Möglichkeit haben können, die Domain mit zu bekommen
-   alles nur vor `createGrid()`

### ParameterTree interface for the GridFactory

-   Konstruktor kommt raus **JF**

### Parallel grid creation

-   wird angegangen, sobald BoundarySegments/Load balancing geklärt ist

Flyspray tasks
--------------

### 841: Remove dependency to WML

-   alle WML Dateien nach dune-web **CE**
-   WML tests können raus **CE**

### 830: Use svn tags for bugfix releases

-   tags/release branches wie svn Standard in Zukunft
-   neuer branch `branch/release-2.1`
-   releases Unterverzeichnis wird angepasst/verschoben nach tags ab dem
    nächsten Release **OS**

### 827: rbegin of FieldVector and ISTL classes has surprising semantics

-   `rbegin()`, `rend()` deprecated
-   neuer möglicher Name: `beforeBegin()`, `beforeEnd()` **MB**

### 819: Remove enum FieldVector::size

-   `size()` sollte immer eine Methode sein **CG**
    -   `::size` deprecaten
    -   `DUNE_COMMON_RELEASE_COMPATIBILITY`

### 818: Topological embedding and indices of subentities are missing

-   ist durch `subIndex()` gelöst **CG**

### 801: Int2Type vs. integral\_constant

-   `Int2Type` geht nach dune-fem **RK**
-   `Int2Type` wird deprecated in den Core Modulen **JF**
-   nderung in den Core Modulen **JF**

<!-- -->

-   generelle Diskussion über neue Features aus dem Standard
    -   der älteste Compiler aus der Liste der supporteten Compiler muss
        unterstützt werden
    -   die Liste umfasst ca. 4 Jahre alte Compiler (plus/Minus, auch
        abhängig von exotischen Architekturen)
    -   die Liste wird auf jedem Treffen aktualisiert
    -   Liste ab dem nächsten Release:
        -   gcc ab Version 4.1
        -   icc ab Version 10.0
    -   prinzipielle sollen Features aus dem Standard genommen werden
    -   Features in unterstützten Compilern, die noch nicht im Standard
        sind, werden prinzipiell nachimplementiert unter Verwendung von
        CamelCase?

### 791: Unify interface class LevelIterator and LeafIterator

-   `lbegin()` deprecaten
-   {Level,Leaf,Hirarchic}Iterator -&gt; EntityIterator **CE**, **MN?**

### 766: Separate GenericGeometries and the SmallObjectPool

-   in den Geometrien wird ein char\*\* verwendet, Allocator kommt raus
    **CG**

### 746 / 661: Extract 'function from expression' functionality from dgf parser

-   wenn eine gute Implementierung gefunden wird und alle glücklich
    sind, dann nach dune-common **CG**, **FA**
-   längerfristig aus dem dgf-parser raus und das nehmen?

### 743: Add a way to determine whether a grid is hybrid

-   `hasSingleGeometryType` als Capability **RK**
-   dokumentieren: nur für codim 0!
-   wenn nur ein Typ, dann den als **GeometryType** (topologyId der
    codim 0 entity) auch exportieren

### 699: A general dynamic parameter interface for grids

-   siehe Diskussion GridFactory

### 670: Remove obscure "DUNE developer mode"

-   Funktionalität 1 und 2 soll immer an sein
-   `--enable-dunedevel` soll wegfallen

### 532: Entity&lt;0&gt;::count should have a dynamic codim argument

-   keine nderung

### 515: Move method 'contains' from IndexSet to GridView

-   `contains()` auch auf den `GridView`s
    -   PartitionType als Parameter (analog zu begin, end)
-   für alle codims, die der `GridView` hat

### GridFamilyTraits to restrictive

-   wird um den Typ `LocalGeometry` erweitert (dimGrid -&gt; dimGrid)
-   **OS** macht einen Vorschlag

Organisation
------------

### branches

-   sollen benutzt werden
-   gewisse Zeit soll nach Fertigstellung eines branches tum testen
    vergehen, bevor er in den trunk gemergt wird
-   vor einem Merge sollte kommuniziert werden
-   Dokumentation auf der Homepage (Developing Dune) **RK**
-   Namensschema: **develop-rREVISION-BESCHREIBUNG**
-   nach dem Merge entfernt von dem, der ihn erstellt hat

### developer status

-   Meetings aller Developer werden weiterhin nur einmal jährlich
    angestrebt
-   kleinere Meetings jederzeit gerne möglich
-   Diskussionen bleiben im FlySpray
-   Mitarbeitende sollen auf die Homepage (auch Ehemalige, seit wann)
-   neue Stati:
    -   **Developer:** Stimmrecht (ehemaliger Core Developer)
    -   **Alumni:** Ehemalige
    -   **Contributor:** Rest

### Make discretization modules core modules

-   Diskretisierungsmodule mehr in den Vordergrund stellen, von den
    anderen *External Modules* abheben
-   Umstellung der Homepage
    -   Hintergrund: Diskretisierung von PDEs
    -   die Core Module und Diskretisierungsmodule werden gemeinsam auf
        der Homepage promoted
        -   dune-pdelab
        -   dune-fem
        -   dune-grid-glue
        -   dune-subgrid
        -   dune-spgrid (falls **MN** einverstanden ist)
        -   dune-prismgrid (falls Freiburg einverstanden)
        -   dune-common
        -   dune-grid
        -   dune-istl
        -   dune-localfunctions
    -   Rest als External Modules
        -   mit Erwähnung, welches Modul benutzt wird
