+++
title = "Dune Developer Meeting 2020 in Heidelberg"
+++

Date and Location
----

The developer meeting is held on February 12-13, 2020 at [IWR](https://typo.iwr.uni-heidelberg.de/contact/) Room 1/414 in Heidelberg.
We invite all DUNE developers and contributors to participate.

## Tentative Schedule

<table>
<tr><td>Feb. 12&nbsp;</td><td>10:00 - 12:00&nbsp; </td><td>Meeting</td></tr>
<tr><td></td><td>12:00 - 13:00&nbsp; </td><td>Lunch</td></tr>
<tr><td></td><td>13:00 - 18:00&nbsp; </td><td>Meeting</td></tr>
<tr><td></td><td>19:00-N.N.</td><td>Dinner</td></tr>
<tr><td>Feb. 13</td><td>9:00 - 12:00</td><td>Meeting</td></tr>
<tr><td></td><td>12:00 - 13:00&nbsp; </td><td>Lunch</td></tr>
<tr><td></td><td>13:00 - 18:00&nbsp; </td><td>Meeting</td></tr>
</table>

Participants
-------------

- Peter Bastian
- Markus Blatt
- Andreas Dedner (only Wednesday afternoon)
- Nils Dreier
- Christian Engwer
- Jorrit (Jö) Fahlke
- Christoph Grüninger (only Wednesday, Feb 12)
- René Heß
- Lasse Hinrichsen
- Dominic Kempf
- Ole Klein
- Robert Klöfkorn (only Wednesday afternoon)
- Santiago Ospina de los Rios
- Simon Praetorius
- Oliver Sander
- Linus Seelinger
- Michal Toth

Proposed topics
---------------

Please use the edit function to add your topics or write an email to Markus.


## General

### Community
- Christoph: Vote on Simon becoming a Dune core developer
    - In favour: 6
    - Against: 0
    - Abstain: 0

    Welcome Simon!

- Christian: I suggest that we decide on a code-of-conduct. This should ensure that Dune is received as an open welcoming community. See for example the one of [PETSc](https://github.com/petsc/petsc/blob/master/CODE_OF_CONDUCT.md).
  - Need particular proposal, need more information

### Infrastructure
- Jö: What is the state of dune-gitlab?

    Is currently staying in HD, Peter thinks there will be people in ~0.5 year
    to take care of a general IWR gitlab.

    Until then, can we have more people with admin right for core?  -> Rene
    can add people?

- Jö: Seeking co-maintainers for mailing list servers (reduce bus factor)
  -> Markus

### Core
- Christoph: Bump required compiler support from C++11 to C++14, i.e., GCC 4.9
    to GCC 7 (from May 2017) and from ancient Clang 3.8 to Clang 5. Given Dune
    2.8 will be released in 2021 or even later, this would enable us to use
    convenient C++ techniques and reduce the number of feature checks.

    - Actually, support C++17 to the extent that GCC7 supports.
    - Mingw: can live with GCC7
    - Clang? 7 is in Debian buster
      - Dumux using Clang 5
      - Ubuntu Bionic Clang6
    - Clusters not an issue anymore, usually have modules with newer compilers

    **Conclusion:** C++17 to the extent supported by GCC7 and Clang5

    - excludes replacement of class objects containing reference members (not
      supported in Clang5
    - Christoph will make a list of more excluded features
    - Who can update the CI system?  Image building for CI system is broken
      currently.  Simon will give it a try.

- Christoph: Getting rid of dune-grid-howto. It's in an embarrassing state. It could be replaced
    by Oliver's book once this is published.

    Possible alternative: "Recipes" by Linus.

    **Conclusion:** dune-grid-howto won't be part of the next release.  Christoph
    will take care.

- Christoph: I'd like to make a two-hour session to revise old merge request
  and issues. We should either decide what to do about them or close them. -> group work

- Christoph: Reduce number of core modules, cf.
  [mailing list thread](https://lists.dune-project.org/pipermail/dune-devel/2019-June/002452.html)
    - RK: I think the number of core modules is ok. E.g. dune-istl should definitively stay separate from dune-grid.
      The one module I would like to see merged is dune-python into the various core modules as discussed during the last meeting.
    - Christoph: I'd like to see dune-geometry and/or dune-istl merged to dune-common.
      Many aspects of istl and common are difficult to separate (defines of matrices, basic linear algebra).
      Dune-geometry is very specific / fine granular (geometry and quadrature rules) that it is not justified to be a module
      of its own.
      Including Python support sounds like a good idea.
    - Discussion:
      - higher-order geometries might require localfunctions, and localfunctions requires geometry
      - tooling to simplify releases?
      - is there a limit in the number of core modules?
      - merging or moving dune-uggrid? Remove splitting into dune-grid/dune-uggrid?
      - Make dune-functions a core module? (not voted now)
      - Decision:
          - dune-typetree -> dune-common (VOTE: yes) (need some cleanup)
          - Change of namespace in dune-typetree
              1. `Dune::` (VOTES: 2)
              2. `Dune::TypeTree` (VOTES: 14)
              3. `Dune::Common::TypeTree` (VOTES: 10)
              4. `Dune::Common` (VOTES: 2)

              VOTE on 2: 11, VOTE on 3: 6, Decision: `Dune::TypeTree`

- Markus: What ist the status of EXADUNE, what should we integrate back?
    - Status: simd interfaces, vectorization of istl solvers, block-krylov methods, multi-threaded/GPU supported solvers (difficult to integrate), block-structured grids, higher-order methods, pipelined/communication-hiding krylov methods
    - Integration: block-krylov methods (dune-istl), (partially) higher-order DG methods (dune-pdelab)
    - Discussion:
        - backward iteration over grid elements (interface change?)
        - multi-threaded istl solvers? (from kascade? from exadune? tbb-based?)

- Linus: add a `module.hh` header in every module including everything from that module
  - RK: too many dependencies -> lead to recompile everything
  - don't use this header in any other core module?
  - benefits? beginners have to look for the headers to include
  - cmake macro to generate header list?
  - are there incompatible headers?
 
- Simon: We have to update docker images to clang5 & test with c++17

- Simon: Is there a place to check of user dune projects?
  - OS: Modules page lack of structure (what is an extension?)
  - Proposal:
    - OS: Include more descriptions to the modules (OS do it)
    - Simon: Add more structure to the sections
      - Have a web for each tag with the modules including such tag

- Linus: examples should compile and run in the CI
  - Very simple example witch is working and is documented in doxygen and that is also compiled and run in the CI
  - There are enough tools in Doxygen to fully document specific parts of the code (e.g. snippet, link pages, show partial part of a file) 
  - See example: https://www.dune-project.org/doxygen/pdelab/master/group__pdelab__recipes.html
  - This avoids that examples get outdated and give users the idea of how to use the software
  - Use folder /doc/recepies/ so that is easy to link later in the webpage and add files to ´dune_test´ 

- Linus: Is there interest for functionality to write tracing information in
  OTF-2 file-format?
  - It records information the taks it is performing (entring and leaving a section in the code)
    and with the format one can get visualization of the load balance with VTE (there are other possibilities)
  - With the OTF2 is a little bit complicated to instrument, so the proposal is to add a facility to trace easier code.
  - Linus will send a merge request with the feature

- Simon: Several links in the webpage are broken.

### Not yet discussed:
- Jö: Cheat-sheet?
- Rober & Andreas: let's talk about 3.0?
  - Simon: Future plans?

## dune-common

### Workgroups:
- Linus: Parallel communication using information about rank, some extension
  is needed in the data handles for the `ParallelIndexSet`
    * Nils and Robert are working on something already (although in a Private
    issue).
  - Result: 
    - Nils proposes a new interface (See https://gitlab.dune-project.org/core/dune-common/issues/174).

- Closing issues:
  - GMSH reader: 
    - Jo referes to indexing problems with gmsh (See https://gitlab.dune-project.org/flyspray/FS/issues/1698) 
    - Simon: There is some work done to the gmsh format 4.0 (Parametrized geometry) 
    - GMSH 4 is not compatible with gmsh 2 format reader. Parallel information is the main change from it.
    - The reader complains when it finds PhisicalEntities.
      - Christian Use component readers for each section.
  - The class VirtualFunction that fulfills the same purpose as std::function. We agree to deprecate it.
  - viewThread proposal for the grid -> closed.

- FORTRAN:
  - Christian: Can we get rid of FORTRAN? It is needed for LAPACK.
  - Eigen & PETSc uses a LAPACK without fortran so we know it's possible.

### Logging library:
- Dominic: Discuss inclusion of (support for) a logging library
    * The current implementation with streams does not attract much use
    * Steffen left us a library: https://gitlab.dune-project.org/staging/dune-logging
    * Other full fletched systems exist e.g. https://github.com/gabime/spdlog
    * Christoph: Including dune-logging sounds good to me.
- Christian: I'm in favour of an external solution. We have to keep our maintenance effort in mind.
- Need for better logging support?
- Include libfmt (or something similar)?
- Needs more information! Comparison of logging libraries, more arguments for dune-logging, maintainability of dune-logging? hard or soft dependency? hierarchical logging? interface changes necessary to use loggers in dune classes?

## dune-geometry

- OS: The makers of CurvilinearGrid have relicensed their code, and the
  license is now dune-compatible.  Can we import their implementation of a
  curvilinear geometry(!) into dune-geometry?
  - Simon worked some with it
  - They have a different numbering of langrange base nodes
  - Simon is working on a extended geometry

## dune-grid

- OS: ALUGrid implements an extended GridFactory that allows for
distributed grid setup.  Can we make that an official interface?
    - RK: Sounds like a good idea.
    - Christoph: +1
    - Here the interface changes we are talking about:
```
/** \brief insert a vertex into the coarse grid including the vertex's globally unique id
     *
     *  \param[in]  pos       position of the vertex
     *  \param[in]  globalId  globally unique id for vertex (e.g. globally unique vertex index)
     */
    void insertVertex ( const VertexInputType &pos, const VertexId globalId );

    // for ALUGrid it is also beneficial to have but this is not mandatory
    virtual void
    insertProcessBorder ( const std::vector< VertexId >& vertices );
```

    - Additional information about rank sharing the vertices?
    - Decision:
        - Andreas: Submit proposal on GitLab to see the details

- OS: Interface clarification: when the grid changes, do `GridView` objects remain valid?  What about `IdSet`s?
  - Example: YaspGrid: gridview is invalid after grid changes
  - Documentation!
  - Carsten: See decision in dev-meeting 2009

## dune-localfunctions

- Simon: Langrange functions have unclear semantics, different implementations
  using inconsistent ordering of Lagrange nodes.  Ordering should be
  documented.
- Simon: Factory and cache have very different interfaces, you can't easily
  switch from one to the other.

## dune-istl
 - Nils: Parallel SolverFactory
    * Polymorphic communication object
    * User interface
 - OS: I keep hearing from people that the RemoteIndex implementation
 is showing its age.  Can we organize a redesign/rewrite?
    * Christian: The `ParallelIndexSets` are an aimplementation and not an interface. Nils and RK are currently working on a proposal for an interface, for which the `ParallelIndexSets` can be a particular implementation. My goal would be to have a coarse grained dynamic interface (in ISTL), which can then be fed with data from a `ParallelIndexSet` or from a `Grid` or something else...


## dune-functions
- Simon: Parallelization of dune-functions bases. Missing tools and utilities, e.g.
    * Iteration over all DOFs on entities
    * (global) IDs of DOFs
    * Introduction of PartitionSet in several algorithms
    * Something like Twist-Uitlities for uniquely identifying the order of DOFs
      on sub entities (also global unique)
    * ParallelIndexSet for MultiIndices
    * Maybe something like dune-grid:GlobalIndexSet but for the DOFs
    * Properly wrapped local bases for DG discretization (association of local
      DOFs to the element and not to the sub-entities)
- OS: New ideas on how to support non-affine families of finite elements?

## dune-vtk
- Module: https://github.com/spraetor/dune-vtk
- OS: Simon has a module for reading and writing VTK stuff doing much more
  than the stuff integrated in dune-grid, including higher order functions and
  structured grid.
- This module can write specific vtk formats that are more compact. Paraview is much 
  faster when using these formats. 
  - Subsampling is still not implemented.
  - Can write quadratic data (Not so easy to visualize in paraview)
  - Structured and rectangular grids.
  - PolyData not implemented (1D is in dune-grid because it was the only way possible to write 1D data)
- Christian: How does it compress data?
  - Is stored in binary blocks compressed: collect - compress - write - come back and set the position of the data. 
- Why don't we use the VTK library?
  - Is not very easy to use. Huge library. No smooth transitions between versions.
- Reader:
  - Data collector: get grid info and its data, then decide whether to write 
    continuous or discontinuous data. It is very flexible to be combined with other data collectors.
  - Parallel grid format:
    - If grid factory does have the global ID interface then it builds the grid on each processor
    - otherwise it gets serialized in one processor and then load balance.
- Know Bug: When YaspGrid has periodic boundaries, it simply produces wrong results.
- In the long run the idea is to complete the missing features and to merge it into dune-grid. 


## Discretization modules (FEM, PDELab, ...)
