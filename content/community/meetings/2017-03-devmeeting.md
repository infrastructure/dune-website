+++
title = "Dune Developer Meeting 2017 in Heidelberg"
+++

Date
----

The developer meeting is held on March 15th, 2017 in Heidelberg.

The meeting is co-located with the [Dune User Meeting 2017](https://conan.iwr.uni-heidelberg.de/events/user-meeting_2017).

Proposed topics
---------------

Please use the edit function to add your topics or write an email to [the organizers](https://conan.iwr.uni-heidelberg.de/events/user-meeting_2017/#organizers).

## General

### New core developers

There is an application for new core developers:

-   Ansgar Burchardt (TU Dresden)
    -   Carsten: I'll not be at the meeting, but I support this application.

### Bug triage

* OS: Can we close some of the old FlySpray bugs?
  * CGrü: Same for old branches and merge requests.

### Development model / Governance

* DK/SM: How can the current model be improved w.r.t. *big* MRs
  * CGrü: I'd like to see a summary, similar to what CE did in this document for the MRs from SP.
    It would be nice to name one core developer to do a review, I would trust his opinion. The problem
    is to name the core developer if nobody is directly interested in the MR.
* MB: Can we gain from a more open development model?
    Currently we have a semi-open model: open user/dev meetings for core, small development groups working on new features (sometimes without announcement to the rest).
    The former can easily be seen/attended by the public. The latter are very secretive and hard to follow (even for core developers)
    for people not in the loop (gitlab to non-informative, discourse to the rescue?). BTW: the lingering big MRs might have profited
    from a more open discussion upfront.
* MB: Can we (in some part) distribute infrastructure (work) (not only Heidelberg University)? (No proper work for scientists. No fair spread of it either. Single point of failure.)
* MB: Move to a (documented) consent based decision making process?
      Actually, most of the time we are already doing this (IMHO) because core developers do not object without reason? But we decided in the early days to do majority votes
      among the core developers. Therefore this is not granted. Using consent based decision making, decisions are made when there are no "paramount" objections.
      * CGrü: Does this exclude crucial vote (Kampfabstimmungen)? Sometimes these are needed to have any decision.
* MB: Revisit when and how release notes are published
* CGrü: Don't force developers to wait for the CI before merging. (keep the current way)

### Dune Logo

**New Topic**

CE has some money to redesign the logo and wants feedback.

* CGrü: I like our current logo and would like to keep it. But a high-resolution version
  and a rectangular one would be nice. Some design improvements for the website would be welcome.

## Threading model

**New Topic**

* Discuss threading model (threads vs. tasks)
* Decide on an implementation
    * Suggestion CE/SM: TBB


## Python Integration

**New Topic**

* Interactions between the different Python integration approaches


## New Topic: The legendary Parallelism Meeting

## dune-common

* DK: I propose using [a latexmk-based alternative](https://github.com/dokempf/UseLatexMk)
  for building PDFs through the build system
  * CGrü: The current system works, why should we have more code of our own? I have never understood what
    your problem with the given solution was. You have never opened an issue about it, have you?
    * DK: There are two problems: additional sources etc. must be explicitly listed with the current solution,
          while the MKbased version is able to just do it the way you expect (see [this example](https://gitlab.dune-project.org/pdelab/dune-pdelab-tutorials/merge_requests/24)).
          Another caveat is, that if in the current setting you happen to run pdflatex manually in the source, your automated builds fall over.
          There was no need for an issue, as the currect solution cannot overcome this limitation.
    * CGrü: The explicit listing can maybe solved with GLOB. The other issue is a non-issue as we explicitly teach our
     users not to work within the source directory but use out-of-source builds. I think this is not a topic
     for the dev meeting. I'd prefer to have issue where we can discuss this and maybe vote if we reach no consensus.
     And I am not opposed to your proposal, I just have not checked your code.
* DK: Can we deprecate support for relative build directories in dunecontrol?
  * CGrü: No, it is the default and quite some people are using it. First, why should we change the
    default at all? Second, why not continue to allow both ways?

## dune-geometry

* SP (Simon Praetorius): I have a proposal for the redesign and cleanup of the implementation of
  the quadrature rules, i.e. provide a database for rules with support for multi-precision coefficients.
  See [MR !27](https://gitlab.dune-project.org/core/dune-geometry/merge_requests/27).<br>
  *Discussion:*
  * all agree that the proposed changes are nice, as long as Simon 
    does the changes
  * the MR should be split up into smaller parts
  * it is still up for dicussion, which intermediate format one
    should use
  * it is a good idea to only use string representations and generate
    instances of `float` like data from this string representation
  * in a future version it should be possible to honor further
    properties of a quadrature rule

## dune-grid

* Get rid of `DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS` preprocessor macro.
  See [MR !155](https://gitlab.dune-project.org/core/dune-grid/merge_requests/155)
  * Proposal: make `.impl()` public, document that you are on your own if you use it.  
    Vote result: Yes 8, No 0, Abstain 3
  * Proposal: put `boundaryID()` below the new public `.impl()` (a.k.a. remove from interface), remove `DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS`  
    Vote result: Yes 8, No 0, Abstain 3

* SP: I have a proposal for the redesign and cleanup of the different grid readers.
  See [MR !90](https://gitlab.dune-project.org/core/dune-grid/merge_requests/90).<br>
  *Discussion:*
  * two parts o the proposal: 
    * unify and cleanup the reader interfaces
    * implement a facade class for grid readers
  * we agree that a unification is desirable
  * suddenly the discussion comes up whether to completely overhaul
    the reader interface (switching from static methods to instances
    of the reader)
  * switching to `std::unique_ptr<Grid>` is considered a good idea
    and it was proposed to also change the `GridFactory` interface
  * Proposal for transition of return type: 
    * return `Dune::to_unique_ptr<Grid>`, which casts to
      `std::unique_ptr<Grid>`, `std::shared_ptr<Grid>`, `GridPtr` and `Grid*`

## dune-istl

* OS: Can we deprecate some of the BCRSMatrix build modes?
  See dune-istl/#21

   * Random has pathological cases leading to OOM

   * Dominik has ideas how to fix this, possibly with slight performance
     implications.  You'll still get an error when you underestimate the nnz.

* DK/SM: Solver/Preconditioner setup through ParameterTrees: How to proceed?
  [The MR](https://gitlab.dune-project.org/core/dune-istl/merge_requests/5) got very messy.
  I would like to summmarize the proposed feature and gather all the feedback
  needed for merging.
  <br>Discussion:
  * in general we agree that a real dynamic interface would be nice
  * Christian will prepare/cleanup the MR
  * we all agree that we prefer value semantics
  <br>Idea:
  * "type erasure" wrapper around the `shared_ptr<Preconditioner>`
    (scalar product, ...)
  * this wrapper supports deep copy (virtual clone method)
  * r-value-ref constructor as "the way to go"
  * l-value-ref constructor is deprecated
  * `std::ref` constructor for advanced users

## dune-localfunctions

* **New Topic** OS: Global Finite Elements

## dune-website

* DK: How can we collect and maintain a nice application gallery?
* DK/RK: Can we collect a list of Dune-based PhD theses?
* Carsten: Cleanup module categories (see [issue 15](https://gitlab.dune-project.org/infrastructure/dune-website/issues/15)).
* MB: Please host user meetings (schedule/talks) on dune-project.org and not on working group websites without even back-links to dune-project.org.

## Mini-Meetings

Proposed mini meetings:

* Global Finite Elements
* Threading/Task Interface ([WG Minutes](threading-infrastructure/))
* 2.5 release paper
* Python/CI
* MPI Grid interface ([some agenda points](parallel-grid-interface/))
* Intersections

