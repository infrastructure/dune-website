![](/img/dune-dev-meeting-2015.jpg)

Dune Developer Meeting 2015 in Heidelberg
=========================================

Full [Protocol](protocol)

Date
----

September 29-30, 2015\
start on Tuesday after lunch

We plan to have a [Dune User Meeting 2015](../2015-09-usermeeting/)
on September 28-29, 2015.

Location
--------

The meeting will take place close to Heidelberg main station at

Dezernat 16\
Emil-Maier-Straße 16\
69115 Heidelberg\
Germany

Room: to be announced

Markus blocked some rooms for Sept. 27-30 at the [Ibis
hotel](http://www.ibis.com/gb/hotel-1447-ibis-heidelberg-hauptbahnhof/index.shtml)
close to the main station. The rooms are blocked until July 20. Single
room incl. breakfast costs 88 — double room 109 — (both fully flexible
rates). Please reserve your room directly with the hotel either by
telephone, email, or fax. Make sure to mention the keyword “DUNE
Meeting”. Note that this is not a special price but just to make sure
that there are at least 20 rooms available. The street price might be
competitive.

Participants
------------

1.  Martin Allkämper
2.  Peter Bastian
3.  Markus Blatt
4.  Andreas Dedner
5.  Christian Engwer
6.  Jorrit Fahlke
7.  Aleksejs Fomins
8.  Carsten Gräser
9.  Christoph Grüninger
10. Dominic Kempf
11. Eike Müller
12. Steffen Müthing
13. Oliver Sander
14. Robert Klöfkorn
15. Timo Koch (at least Tuesday)
16. Martin Nolte
17. Tobias Malkmus

Topics to be discussed
----------------------

### General

- ~~CGrü: Briefly revisit [2014 meeting protocol](../2014-09-devmeeting/).
  Has everything been implemented satisfactorily? Maybe reassign important
  tasks.~~

- ~~CGrü: What compiler versions should we support in Dune 3.0?~~

  - ~~SM: Additionally: How do we go about supporting newer standards? C+*14
    has been out for quite some time now and is supported in current compilers
    (GCC 4.9 supports quite a bit, GCC 5 is fully compliant (that’s the reason
    for the nasty ABI change), clang is fully compliant since 3.4 and icc will
    support quite a bit of the standard in ICC 16). Apart from that, it has
    now become a lot easier to detect whether the compiler supports a given
    feature using the new feature detection macros. Finally, GCC 6 will
    default to C14, and I can imagine that Clang 3.8 will follow suit. Can we
    change master to enable C14 if found?~~

- ~~CGrü: Rework website (CMake compatible replacement for WML)
  [FS\#1673](https://gitlab.dune-project.org/flyspray/FS/issues/1673)~~

- ~~Infrastructure (Should we switch to gitlab? How to organize votes?) -\>
  see below~~

- ~~CGrä: How to implement finite elements with higher order regularity
  (C1,…)?  Postponed.~~

### Build system

- ~~CGrü: What CMake version should we support in Dune 3.0?
  [FS\#1619](https://gitlab.dune-project.org/flyspray/FS/issues/1619)~~

- ~~DK: Abolition of the magic that builds test during “make test”.
  [FS\#1621](https://gitlab.dune-project.org/flyspray/FS/issues/1621)~~

- ~~DK: I would like to see actual parallel test runs during “make test”
  [FS\#1679](https://gitlab.dune-project.org/flyspray/FS/issues/1679)~~

- ~~DK: In that context, it would be nice to introduce a cmake function
  “dune\_add\_test” with a properly defined interface, that allows to easily
  add tests that include some more sophisticated logic.~~

- ~~DK: Documentation of the CMake build system: I have written a sphinx-based
  documentation generator for cmake, which is already included into the dune
  cmake infrastructure. Check
  [here](http://conan2.iwr.uni-heidelberg.de/cmake-documentation/html/index.html)
  for an example, which documents one of my modules. This file was generated
  from sources, such as
  [this one](http://conan2.iwr.uni-heidelberg.de/git/quality/dune-python/blob/master/cmake/modules/CreateVirtualEnv.cmake)
  I would like to apply this to the core modules.~~

- ~~DK: W.r.t. this documentation, I would like to have all user-facing
  interfaces as functions with named arguments in 3.0~~

### Requests for new features

- ~~CGrä: Introduce namespaces for dune 3.0.~~

### Clarifications of interfaces

- ~~OS: Answer the following question: Do preAdapt and postAdapt **always**
  have to be called, or only if there is data to be transferred?~~

- ~~OS: Semantics of the GridFactory: can I start inserting elements into the
  Factory before having inserted all vertices?~~

- ~~Jö: What guarantees exactly does a viewThreadSafe grid provide? How does
  that relate to common thread-safety expectations? A proposal is attached to
  [FS\#1717](https://gitlab.dune-project.org/flyspray/FS/issues/1717)~~

### Change requests for interfaces

##### dune-common

- ~~OS: dune-typetree implements compile-time numbers \_0, \_1, \_2, etc.,
  e.g.~~
  ```c++
  std::integral_constant<std::size_t,0> _0;
  ```
  ~~(I am paraphrasing here, the actual implementation is a bit longer.) I would
  like to have this in dune-common, because MultiTypeBlockVector also needs
  something like it.~~

- ~~CGrä: Remove VirtualFunction in favor of std::function~~

##### dune-localfunctions

- ~~CGrä: Switch evaluate() method for partial local finite elements to
  classical multiindex formulation. Currently it argument is a vector with
  coordinate direction per partial derivative. This should be switched to a
  multiindex with number derivatives per coordinate direction in accordance
  with global finite elements.~~

- ~~CGrä: Make interpolate() accept operator() notation for its arguments.~~

- ~~CGrä: Define how interpolate() can access derivatives of its argument. This
  is needed for Ck-spaces with k\>0.~~ Postponed.

##### dune-grid

-   ~~OS: Generalize the dune-grid intersection concept for network grids.~~

    - ~~A detailed proposal has been attached to
      [FS\#1714](https://gitlab.dune-project.org/flyspray/FS/issues/1714)~~

    - ~~RK: Along that lines we can also discuss
      [FS\#1591](https://gitlab.dune-project.org/flyspray/FS/issues/1591)~~

    - ~~RK: and
      [FS\#1369](https://gitlab.dune-project.org/flyspray/FS/issues/1369)~~

-    SM: Add an operator bool() for entities and intersections to check
     whether the object was default-constructed (returns false) or contains
     data of an actual entity / intersection (returns true)

-   ~~OS: Currently, the Intersection interface class exports four integer
    numbers:~~

    - ~~codimension: The codimension of the intersection in the
      grid (always 1)~~

    - ~~dimension: The grid(!) dimension~~

    - ~~mydimension: the dimension of the intersection itself~~

    - ~~dimensionworld: The dimension of the world space of the grid~~

    ~~This list is a bit redundant. Also, the fact the Intersection::dimension
    is not the dimension of the intersection has confused quite a number of
    people. Can we do some cleanup?~~

##### dune-istl

  - OS: The BlockVector class has a surprisingly large set of base
    classes. From them it inherits a few methods that should not really be
    part of an istl vector interface. For example, you can write

    ```c++
    size_t n = vector.N();
    ```

    instead of

    ```c++
    size_t n = vector.size();
    ```

    and

    ```c++
    auto c = *(vector.find(42));
    ```

    instead of

    ```c++
    auto c = vector[42];
    ```

    I propose to get rid of these methods. I’d also like to get an answer of
    why this implementation must be so complicated?

  - OS: There are several ways to set up a BCRSMatrix, and they are all
    implemented in the same class and the same file (which is of intimidating
    complexity). Can we split up the setup code in a separate factory class or
    something similar?

      - MB: My initial proposal for the new setup method was to have this in a
        separate class. But this was unfortunately neglected. In general I
        would appreciate using separate setup classes for creating and
        initializing sparse/dense matrices somewhat along the following lines:

        ```c++
        CustomSetup<BCRSMatrix> setup;
        // initialize entries
        setup[i][j]=value;
        //...
        auto matrix = setup.finalize();
        ```

        This should work for all setup routines.

  - OS: Range-based for cannot be used to iterate over the entries of a row of
    a BCRSMatrix. Reason: the iterator contains a vital non-standard method
    (the ’index’ method), which becomes inaccessible when using range-based
    for. Can we rework the interface here to allow for range-based for?

      - MB: But you need to be able to get the column index of a matrix row
        value. If you really want to use range based for then the iterator
        must be over pairs of value and column index. Not sure whether

        ```c++
        for(auto pair: matrix_row)
        {
           col = pair.index();
           rhs[row] = pair.value() * lhs[col];
        }
        ```

        saves us something compared to

        ```c++
        for(auto value=matrix_row.begin(), end=matrix_row.end(); value!= end;*+row)
        {
           col = row.index();
           rhs[row] = (*value) * lhs[col];
        }
        ```

        And we would need to rewrite a lot of code!

### Infrastructure

-   ~~SM: The server in Heidelberg will move to new hardware over the
    next few months. That also involves switching to a newer operating
    system. There are some changes that we will definitely make, and
    some that we should discuss:~~
    -   The new server will not have Subversion support anymore. We will
        keep dumps of the existing repos, though.
    -   ~~The website repository will move to Git, and we will kick out
        wml. We currently have a Hiwi to port the existing website to
        Hugo:http://gohugo.io~~
    -   ~~CE: I think we should discuss other options than Hugo as well.
        Carsten had valid point in proposing ikiwiki, or something
        similar. Online-editing has its advantages.~~
    -   ~~I would really like to get rid of the existing SVNManager
        because it is an ancient blob of scary PHP. In order to have a
        unified interface, my suggestion would be to set up a GitLab
        instance.~~
    -   ~~CE: I agree, we have to unify the user management. But please
        keep this discussion separate from gitlab.~~
    -   ~~Even more heretical: Can we get rid of FlySpray and just
        replace it with GitLab’s internal bug tracker? It has far less
        features, but for PDELab it has been more than adequate.~~
-   ~~CE: more generally we have to discuss or workflow (and I think
    this will then lead us to or away from gitlab.~~
    -   ~~feature branches vs. development branch~~
    -   ~~pull vs push~~
-   ~~CE: we need a new polling system. I suggest to have a system which
    is separate from any other system, in order to keep it running, even
    if a service is not available any more. Possible choices are imho
    https://dudle.inf.tu-dresden.de or http://www.koalatux.ch/du7f/~~
-   ~~CGrä: Despite the polling mechanism I propose to have a short
    status/plans report on this and to postpone discussion and decisions
    to the list. Lets safe our precious time for the more technical and
    controversial topics.~~
