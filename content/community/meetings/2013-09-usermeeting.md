Dune User Meeting 2013
======================

Invitation
----------

We invite all Dune users to participate at the 2nd Dune User meeting, to
be held at RWTH Aachen from 25.09.2013 morning to 26.09.2013 noon.

This is your, the users’, meeting and should be used to showcase how you
are using DUNE, foster future collaborations between the users, and the
users and developers, and provide input for the future development of
DUNE. Therefore we intend to keep the format rather informal. All
participants should indicate if they want to give a presentation. Please
note that the presentation should be prepared such that the presentation
time is flexible (from a few minutes to half an hour). At the meeting we
will divide the total presentation time equally between the prospective
speakers and generate the order of the talks randomly.

Please note that on September 26 there will be an overlap with the
[Dune Developer Meeting 2013](../2013-09-devmeeting/) to
allow a closer interaction of the user community with the developers and
ensure that the further development of DUNE is in everybody’s best
interest.

Location
--------

The workshop will take place in the main building of RWTH Aachen
university

Templergraben 55\
52064 Aachen\
[map](http://www.openstreetmap.org/?lat=50.777797&lon=6.077647&zoom=18&layers=M)\
Room 149

### Getting to RWTH Aachen

RWTH Aachen is in close proximity of several airports, namely Maastricht
Airport (38 km), Cologne/Bonn airport (85 km), Düsseldorf airport (103
km), and Frankfurt airport (250 km). You will find more information
about how to get from the chosen airport to RWTH and accommodation tips
[here](http://www.rwth-aachen.de/cms/root/Studium/Vor_dem_Studium/Planung_des_Aufenthalts_fuer_internationa/~bppk/Anreise/lidx/1/).

People coming from the UK may be interested to hear that you can take
the EuroStar train from London to Brussels, and then take another train
directly to Aachen. This takes about four hours.

### Accommodation

The meeting venue is close to the city center, and there are many hotels
to choose from.\
Here is an incomplete list:

- Hotel Benelux, Franzstraße 21-23, ab 70€/Nacht, 0241/400 03-0\
- Mercure Hotel Aachen Am Dom, Peterstr. 1, ab 79€/Nacht, 0241/18010\
- Novotel Aachen City, Peterstr. 66, ab 89€/Nacht, 0241/51590\
- Concord Hotel Lousberg, Saarstr. 108, ab 62€/Nacht, 0241/20331\
- Aqis Grana Cityhotel, Büchel 32, ab 61€/Nacht, 0241/443-0

You can see the location of the hotels on the
[map](http://goo.gl/maps/S1cGF).

Participants
------------

Please add your name below (ordered by last name) or send the necessary
information (name, institution, and presentation title) via email to
[duneuser13@dr-blatt.de](mailto:duneuser13@dr-blatt.de?subject=Dune%20User%20Meeting%202013)
if you would like to participate and indicate whether you want to give a
presentation. Please also feel free to contribute your ideas below.
Further information about the meeting will also be communicated via the
[DUNE mailinglist](https://lists.dune-project.org/mailman/listinfo/dune).

  Name                 | Institution                              | Country | Tentative Presentation Title
 --------------------- | ---------------------------------------- | ------- | ----------------------------
  Peter Bastian        | Universität Heidelberg                   | Germany | (no presentation)
  Markus Blatt         | Blatt HPC-Simulation-Software & Services | Germany | Developer: [GSoC](/pdf/meetings/2013-09-usermeeting/gitgsoc.svg), [CMake](/pdf/meetings/2013-09-usermeeting/CMakeForDune_blatt_grueninger.pdf), Git
  Andreas Buhr         | WWU Münster                              | Germany | (no presentation)
  Andreas Dedner       | Warwick University                       | UK      | Developer: brief description of the new alugrid features (10min)
  Anne C. Elster       | NTNU Trondheim                           | Norway  | (no presentation)
  Christian Engwer     | University of Münster                    | Germany | (no presentation)
  Bernd Flemisch       | University of Stuttgart                  | Germany | Multi-Point Flux Approximation on Parallel Adaptive DUNE Grids
  Elisa Friebel        | RWTH Aachen                              | Germany | (no presentation)
  Christoph Gersbacher | University of Freiburg                   | Germany | Improvements to the Dune-Common Tuples Library
  Stefan Girke         | WWU Münster                              | Germany | [Modelling of atherosclerotic plaque formation](/pdf/meetings/2013-09-usermeeting/ModellingOfAtherosclerosis_girke.pdf)
  Christoph Grüninger  | University of Stuttgart                  | Germany | Developer: [CMake](/pdf/meetings/2013-09-usermeeting/CMakeForDune_blatt_grueninger.pdf)
  Katja Hanowski       | RWTH Aachen                              | Germany | Simulating a coupled fluid-fracture system
  Didrik Jonassen      | NTNU Trondheim                           | Norway  | (no presentation)
  Guillaume Jouvet     | FU Berlin                                | Germany | Simulating the dynamics of ice sheets with Dune
  Dominic Kempf        | Blatt HPC-Simulation-Software & Services | Germany | (no presentation)
  Tobias Malkmus       | University of Freiburg                   | Germany | Simulation of incompressible non-Newtonian Fluids && Dune-Ash, the interactive volcano simulation program
  Eike Mueller         | University of Bath                       | UK      | [Massively Parallel DUNE Implementation of Tensor-Product Multigrid Solvers in Geophysical Modelling](/pdf/meetings/2013-09-usermeeting/slidesEikeMueller.pdf) (with Andreas Dedner and Robert Scheichl)
  Dieter Moser         | Jülich Supercomputing Centre             | Germany | (no presentation)
  Steffen Müthing      | Universität Heidelberg                   | Germany | Developer: [Lmod or how to protect your sanity from dependency hell](/pdf/meetings/2013-09-usermeeting/lmod.pdf)
  Martin Nolte         | Univertität Freiburg                     | Germany | Developer: Improving the Performance of Multiple Meta Grid Layers (\~25 min., must be on Thursday)
  Jurgis Pods          | Universität Heidelberg                   | Germany | [Electrodiffusion Simulations of Neurons and Extracellular Space](/pdf/meetings/2013-09-usermeeting/lecture-main.pdf)
  Tom Ranner           | University of Leeds                      | UK      | [Unfitted finite element methods for surface partial differential equations](/pdf/meetings/2013-09-usermeeting/unfem-talk.pdf)
  Atgeirr F. Rasmussen | SINTEF                                   | Norway  | [Simulators that write themselves](/pdf/meetings/2013-09-usermeeting/Equelle dune user meeting 2013.pdf)
  Stephan Rave         | WWU Münster                              | Germany | dune-pyMor: Model Order Reduction with Python and Dune
  Alf B. Rustad        | Statoil                                  | Norway  | (tba)
  Amer Syed            | Imperial College                         | UK      | (no presentation)

Tentative Schedule
------------------

### Wednesday, September 25, 2013

  Time        | Title               | Description
 ------------ | ------------------- | ---------------------------------
   9:00-9:30  | Welcome             | Short introduction of participants (Oliver Sander)
   9:30-10:45 | User Presentations  | Chair: Bernd Flemisch
  10:45-11:15 | Coffee Break        |
  11:15-12:30 | User Presentations  | Chair: Christoph Grüninger
  12:30-14:00 | Lunch break         |
  14:00-14:45 | User Polls          | Dune Users statistics (Chair: Bernd Flemisch)
  14:45-15:45 | User Presentations  | Chair: Markus Blatt
  15:45-16:00 | Coffee Break        |
  16:00-17:00 | User Presentations  | Chair: Oliver Sander
  17:00-19:30 | Bug Squashing Party | Help us fix bugs!
  20:00       | Dinner              | At the [Labyrinth](http://www.openstreetmap.org/?mlat=50.78076&mlon=6.07933&way=79513963#map=19/50.78076/6.07933)

Currently 12 presentations in 4:00 h, thus estimated 20 min per person
including questions.

### Thursday, September 26, 2013

  Time        | Title                                           | Description
 ------------ | ----------------------------------------------- | ---------------------------------------------------------------------
  9:00-10:30  | Developer Presentations                         | of interest to both users and devs (Chair: Bernd Flemisch):
              |                                                 | lmod (Steffen), CMake (C. Grüninger), meta grids (Martin)
  10:30-11:00 | Coffee Break                                    |
  11:00-11:30 | Developer Presentations                         | GSoC, git (Markus), ALUGrid (Andreas)
  11:30-12:15 | DUNE Discussion                                 | What users anticipate from further development (Chair: Markus Blatt)
  12:15-12:30 | Farewell                                        |
  12:30-14:00 | Lunch Break                                     |
  14:00       | [Dune Developer Meeting](../2013-09-devmeeting/) |

Pictures from the Meeting
-------------------------

[Group photo A 6.1MB](/img/meetings/2013-09-usermeeting/DUNE-UG-2013-a.jpg)\
[Group photo B 6.4MB](/img/meetings/2013-09-usermeeting/DUNE-UG-2013-b.jpg)\
[Dinner photo 1
6.4MB](/img/meetings/2013-09-usermeeting/DUNE-UG-2013-dinner-1.jpg)\
[Dinner photo 2
6.4MB](/img/meetings/2013-09-usermeeting/DUNE-UG-2013-dinner-2.jpg)\
[Dinner photo small
0.9MB](/img/meetings/2013-09-usermeeting/DUNE-UG-2013-dinner-small.jpg)

Organization
------------

This workshop is organized by

-   Markus Blatt
-   Bernd Flemisch
-   Christoph Grüninger
-   Oliver Sander
