+++
title = "Getting involved"
[menu.main]
identifier = "contributing"
parent = "community"
weight = 1
+++

## How to Contribute
DUNE is a community project and we are happy about all forms of external contributions. There are many easy things that you can do, like

* read the documentation and tell us where and how it should be improved,
* try to install DUNE on your platform and report bugs if it doesn't work,
* fix bugs and send us patches.

If you decide to contribute code please head to our [GitLab Instance](http://gitlab.dune-project.org/) and get an account there. Please read the contribution guide shown by GitLab in all the core modules.

## Google Summer of Code 2025 Project Ideas


Information on how [to apply](/dev/gsoc) as a GSoC student is found [here](/dev/gsoc).

Below is a short list of projects that we think would be useful and
suitable for a google summer of code student. We tried to provide
clear descriptions for the projects in the short list. Projects are
either part of DUNE directly or part of [OPM](https://opm-project.org),
which is based on DUNE. Both projects are collaborating for Google
Summer of Code in 2025 to provide more mentors and projects. Hence
students can work on both projects by applying to DUNE.

Of course you can also think about your own project and prepare an
application based on that. In that case you may use our [project ideas
list](#project-ideas) below for further inspiration. The descriptions
in these projects are little less detailed than in the short
list. Please note that projects should be either contributions to DUNE
or OPM.

### ParallelIndexSet 2.0: Smarter, Faster, and Fully Modernized

#### Description

This project aims to rewrite and modernize the ParallelIndexSet
infrastructure by leveraging the C++ Standard Library and improving
its efficiency with asynchronous communication. The goal is to allow
better parallel performance by enabling computation while
communicating and improving data handling in scatter/gather
operations. The updated implementation will also introduce support for
sending and receiving data within the same container while maintaining
robust testing and clear documentation.

List of expected improvements:

1. Use containers/algorithms from the C++ standard library for implementation.
2. Use asynchronous communication allowing to do something while communicating.
3. Use data handles that expose source/destination rank in scatter/gather.
4. Allow send/receive to the same container.
5. Write tests and documentation.

#### Preparation

Prior to application: Make yourself familiar with the message passing
  interface (MPI) and read the parallelization section of the [DUNE
  paper from
  2021](https://www.sciencedirect.com/science/article/pii/S089812212030256X)

Difficulty
: Medium/Advanced

Duration
: 175h/350h project

Dune/OPM Module
: dune-common

Languages
: C++
: MPI

Mentors
: Peter Bastian
: Nils Friess

### Free GUI of OPMRun from commercial libraries

#### Description

Within the [OPM](https://dune-project.org) project that is based on
DUNE there is a graphical user interface for running reservoir
simulations called OPMRun. This has been developed by David Baxendale,
who sadly passed away in 2023. It is an essential tool for people not
very familiar with using the terminal.

When it was written, it was based on the open source GUI library
[PySimpleGUI](https://github.com/PySimpleGUI). At that time the
project was licensed under LGPL-v3. Unfortunately, the developer closed
the software with version 5 during 2023 and removed all older
free versions from github.com, see [stackexchange
thread](https://opensource.stackexchange.com/questions/14600/pysimplegui-until-now-has-been-distributed-under-the-gplv3-but-just-became-a). As
a result of this users that install
OPMRun are now seeing a note about using a trial version of
PySimpleGUI and are asked to buy a commercial license.

The aim of this project is to find a suitable free (as in
licensed under an [Open Source Initiative
(OSI)](https://opensource.org/) approved license), stable and platform
independent other python GUI library and rewrite OPMRun based on it.

The resulting rewrite should have the same functionality as the
current version and work on Linux and Windows (running OPM under WSL).

#### Preparation

Prior to application: Make yourself familiar with OPMRun by installing
it together with a stable version of OPM and ResInsight. Do this on
Linux as well as Windows. See the [OPM installation
instructions](https://opm-project.org/?page_id=36)
and the [OPM Manual](https://opm-project.org/?page_id=955)

Difficulty
: Basic/Medium

Duration
: 90h/175h project (depending on expertise)

Dune/OPM Module
: [opm-utilities/opm-run](https://github.com/OPM/opm-utilities/tree/master/opmrun)

Languages
: Python
: Cmake

Mentors
: Lisa Julia Nebel
: Markus Blatt

### Investigate accelerating DUNE on the GPU

#### Description

Combine GPU computing with DUNE: Select an example case, identify
critical computation parts suitable for GPU acceleration, e.g.:

- stencil computations
- matrix assembly
- solvers (e.g., https://github.com/NVIDIA/AMGX,
   https://petsc.org/release/overview/gpu_roadmap/, etc),

Replace or enhance these parts with functions that can run on a GPU. An 
additional challenge is to find an effective way to run and profile the 
example case, for instance, using Google Colab. By the end of the 
project, we expect to deliver a GPU-accelerated version of a DUNE-based 
PDE solver (proof of concept), gain a clear understanding of what 
benefits most from GPU acceleration, and provide performance benchmarks 
that indicate when GPU acceleration is worthwhile (e.g., how large must 
the problem be).

##### Preparation

Prior to application: Make yourself familiar with DUNE and some PDE
examples. Choose framework or approach. There has been quite some work
on this in [OPM](https://www.opm-project.org), see e.g., GPU-related
files in 
https://github.com/OPM/opm-common/tree/master/opm/material or 
https://github.com/OPM/opm-simulators/tree/master/opm/simulators/linalg

Difficulty
: Medium

Duration
: 175h

Dune/OPM Module
: DUNE core modules

Languages
: C++
: Opencl
: CUDA/HIP

Mentors
: Lisa Julia Nebel

## History of DUNE in Google Summer of Code

In 2013 Dune participated in GSoC as a mentoring organization for the first time. The projects were:

* Iris worked on Attaching the PSurface Library of DUNE to ParaView.
* Miha Čančula worked on a performance testing framework.

In 2016, we were selected a second time. The 2016 projects were:

* Xinyun worked on a grid with spline geometries using B-spline and NURBS
  geometries.
  Checkout the
  [screenshots](http://gsoc2016xinyun.blogspot.com/2016/06/b-spline-and-nurbs-geometry-interface_19.html)
  with according surfaces.
  Usually we don't see such smooth output with our methods.
  Blog: [http://gsoc2016xinyun.blogspot.de](http://gsoc2016xinyun.blogspot.de)
  Code repository: [https://gitlab.dune-project.org/Xinyun.Li/dune-iga](https://gitlab.dune-project.org/Xinyun.Li/dune-iga)
* Michael developed Python bindings for the DUNE grid interface.
  As an example, he implemented a
  [cell-centered finite volume in Python](http://misg.github.io/gsoc2016/dune/2016/06/05/two-weeks-later),
  inspired by the corresponding example in the DUNE grid how-to, using his bindings.
  Blog: [http://misg.github.io/](http://misg.github.io)
  Code repository: [https://gitlab.dune-project.org/michael.sghaier/dune-corepy](https://gitlab.dune-project.org/michael.sghaier/dune-corepy)

Both 2016 projects were successful. We greatly appreciate the work put into our code base
by the GSoC students.

## Other Project ideas {#project-ideas}

The following is a list of things that we find interesting but that we probably will never have the time to do ourselves.

### dune-geometry

* Introduce a way to have geometries based on higher order grid functions.  
90 h / 175 h / 350 h

* Implement a collection of the latest published quadrature rules. It provides a portable and high-precision data format and an export utility to generate the quadrature code for any library, e.g. for dune-geometry. Additionally, the code should be able to increase the quadrature precision, using existing values as initial data for a nonlinear solver implemented with arbitrary precision arithmetic. The existing quadrature rules are old and outdated. Even for 2d and especially for 3d rules with much lower number of points are available. Additionally, the library could provide extra information about the location and properties of the quadrature points, e.g., all inside the element, or points on the boundary or positive/negative weights, or symmetry properties of the points...  
Prior to application: Write a first quadrature rule.  
Prior to application: Read quadrature rule papers and share your insights.  
175 h / 350 h project: Depending on how many quadrature rules and the actual design of the project, both sizes are possible.

### dune-ISTL

* Write an improved parallel ILU preconditioner, following the approach proposed in http://www.cc.gatech.edu/~echow/pubs/parilu-sisc.pdf  
175 h project

* Implement an incomplete Cholesky preconditioner, to improve efficiency for symmetric matrices.  
Prior to application: Get yourself known with Cholesky decomposition, implement it with GNU Octave or NumPy.  
175 h project

* Design and implement a special matrix class, to store symmetric matrices, reducing the memory footprint.
Prior to application: Implement some sparse or symmetric matrix format using GNU Octave or NumPy.  
175 h project

### dune-grid

* VTK can visualize higher order elements and our export could support this feature.  
Prior to application: Investigate some code for data and geometry [dune-vtk](https://gitlab.dune-project.org/extensions/dune-vtk)  
175 h project: Integrate into dune-grid

* dune-gmsh: During the development of dune-curvedgrid Florian Stenger and Simon Praetorius have written a new GMSH reader for the version 4 format. In this context there was the idea to implement a reader that can read distributed grids including the partitioning information from the gmsh files. The file format provides lots of information and we currently only read a small subset. Additionally, the encoding of boundary data is changed compared to the old format. The project would be about reading as much information as possible from the gmsh file format and provide a way to construct grids from this. Currently it is possible to construct curved grids automatically.  
Prior to application: Investigate existing code from [dune-curvedgrid](https://www.dune-project.org/modules/dune-curvedgrid/)  
175 h or 350 h project: Integrate into dune-grid

### dune-localfunctions

* Reimplementing the generic local finite element by combining that with some code generation so that the local degrees of freedom are prescribed in a python script (perhaps using sympy or UFL) and then the corresponding LFE is generated similar to what is done now on the C++ side. Also https://github.com/mscroggs/symfem could be a starting point.  
175 h / 350 h

### dune-uggrid

* Add way to have a distributed grid setup. Distribute a grid and attached data over multiple nodes. Check what other grid implementations do, come up with a plan, and make an example implementation.  
175 h / 350 h

* Add a way to load and store the grid hierarchy that occurs during dynamic grid refinement and coarsening.
90 h

* The dimension of the grid is tied to its world dimension. In other words, an n-dimensional grid lives inside an n-dimensional world. Overcoming this restriction enabled a 2d grid living in a 3d world with additional geometric feature a pure 2d grid does not offer.  
175 h / 350 h

### Documentation
Tutorials are a good way to get started with Dune. A good tutorial for dune-istl would be of great help.  
Prior to application: Read existing documentation. If you find outdated stuff, propose updates.  
175 h project: Write a tutorial with example programs that cover the different aspects of ISTL.

#### Infrastructure

* Invent a mechanism that extracts all finite element implementations from dune-localfunctions (plus their doxygen documentation), and renders them into a LaTeX document. This document should then in turn become a reference section in the dune-localfunctions tutorial. It seems that the doxygen-LaTeX functionality is too limited for this. You may have to play around with the XML feature of doxygen.

### Additional Grid Backends

* Write a parallel AMR grid manager that works in more than three space dimensions.
* Write a structured simplex grid for rectangular domains. 2d and 3d would be great, the brave can implement Freudenthal's algorithm for a general n-d simplex grid.  
350 h project
* Implementing a serialization for UG grid hierarchies.

### Meta Grids
Meta grids are DUNE grids which are parametrized by another grid. An example is SubGrid, which allows you to select a subset of elements of a DUNE grid and treat this subset as a grid in its own right. Here are a few other ideas for meta grids which may be useful.

* Write a meta grid which, given an affine grid with a parametrized boundary, yields an isoparametric approximation of the boundary.  
Prior to application: Have a look at [dune-curvedgrid](https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgrid) meta grid, that comes handy for this project.  
350 h project: You just need to provide the mapping that results in curved boundaries while leaving the rest untouched. It has be done by the use of Lobatto shape functions, with high polynomial degree only on the boundary faces and order 1 everywhere else.

* A proper periodic grid-wrapper: There are some old implementation that allow to wrap GridParts of dune-fem and also a sketch on a meta-grid based on dune-grid, but nothing is finished yet. The idea is to have a meta-grid plus some association/geometry-transformation that wraps an existing grid and transforms the index-set such that associated entities give the same index. For non-parallel grids this is not so complicated but technical, but for parallel grids it is not so trivial. Either the domain partitioning is controlled to guarantee that associated entities are always on the same partition, or a virtual inner interface must be established to have communication of these entities in a domain-decomposition sense.
