+++
group = "user"
title = "Application Modules"
[menu.main]
parent = "modules"
weight = 7
+++

The following are Dune modules created with specific application areas in mind. Compared to the [Dune core modules](/groups/core/), these user-created modules offer tailored solutions and unique functionalities, extending the versatility of the Dune platform.
