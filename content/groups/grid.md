+++
group = "grid"
title = "Grid Modules"
[menu.main]
parent = "modules"
identifier = "grids"
weight = 2
+++
### Grid managers

The dune-grid module already includes some grid managers. Some are DUNE internal but most require some additional packages. Look at an [overview](/doc/grids/) of the different grid manager provided and their features. In addition to those provided directly with dune-grid, there are further grid managers with different sets of features available - some of these might require the installation of some additional library. Not all the grid managers listed here are available for download but feel free to contact the maintainer to ask for a tar ball.

### Grid features

An overview of the different features is [collected here](/doc/grids/).
