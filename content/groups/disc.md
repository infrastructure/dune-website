+++
group = "disc"
title = "Discretization Modules"
[menu.main]
parent = "modules"
weight = 3
+++
Dune has a modular structure and it is possible to write your own
discretization schemes based directly on the core modules. But there
are also a number of modules maintained by the DUNE developers which
add different discretization methods to the
[Dune core modules](/groups/core/).
