+++
title = "Guides"
[menu.main]
name = "Guides"
identifier = "guides"
parent = "docs"
weight = 0
+++

* [Repository layout](/dev/repository_layout/)
* [Coding style](/dev/codingstyle/)
* [Whitespace hook](/dev/whitespace_hook/)
* [Adding Python support](/dev/adding_python/)
