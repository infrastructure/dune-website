+++
title = "The Dune Build System"
toc = 1
[menu.main]
parent = "docs"
weight = 5
name = "The Dune Build System"
+++

As Dune is a set of separate modules rather than a monolithic piece of software,
building it is a bit more complex than building other finite element programs.
This page will explain the main design ideas of the Dune build system,
and how to use them for your needs.  A description of the Dune build system
of Release 2.7 is also included in the [Dune book](https://link.springer.com/book/10.1007/978-3-030-59702-3).

By itself, each Dune module is a C++ library (many of them with Python bindings),
which uses [CMake](https://cmake.org) as its build system. It is the aim to make
these individual build systems conform to standard CMake practices as much
as possible, but currently they still contain a few Dune-specific quirks.

To build sets of Dune modules that belong together, Dune contains a dedicated
tool called `dunecontrol` (in the `dune-common/bin` directory).  This tool
can track dependencies, determine suitable build orders, and configure, build,
and install ensembles of Dune modules.  The rest of this page mainly explains
how the `dunecontrol` tool works.

From now on, we assume that `dunecontrol` is in your `PATH`, i.e., you can call it
without having to specify its location.  This is given automatically if you have
the `dune-common` module installed globally, e.g, via a Linux package.
Otherwise, you may run

```
echo 'export PATH=/path/to/dune-common/bin:${PATH}' >> ${HOME}/.bashrc
source ${HOME}/.bashrc
```


## Building DUNE modules

To compile the modules DUNE has to check several components of your system and whether prerequisites within the modules are met.

The typical way to run `dunecontrol` is using an options file and execute:
````
./dune-common/bin/dunecontrol --opts=config.opts all
````
This will configure and build all modules you have downloaded in the order of resolved dependencies.

The `config.opts` file contains, e.g.,
````
CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release"
````
See below for more details on the options file.


More generally, you can separately run

```
./dune-common/bin/dunecontrol [OPTIONS] cmake [CMAKE_FLAGS]
./dune-common/bin/dunecontrol [OPTIONS] make [MAKE_FLAGS]
```
in order to first configure and then build all modules with the given flags.


### Building other DUNE modules
Besides the core modules you can find lots of other dune modules:

- [Grid Modules](/groups/grid/)
- [Discretization Modules](/groups/disc/)
- [Extension Modules](/groups/extension/)
- [Tutorial Modules](/groups/tutorial/)

If you want to use one of those modules make sure to download the module and all dependencies in the same common directory as the DUNE core modules. Building your modules is done in the same way as building the core modules by calling `dunecontrol`.

### Building a specific DUNE module (and its dependent modules)

You can instruct `dunecontrol` to build only a certain dune module, using the `--only=<module_name>` switch. Running `dunecontrol` script
```
dunecontrol --only=<module_name> all
```

where `<module_name>` is the name of that particular module given in the `dune.module` file,
will build only the module `<module_name>`.

If you want to build a module and the modules it depends on, you must run:
```
dunecontrol --module=<module_name> all
```

### Changing build mode and optimization flags

The flags passed to the compiler are controlled by CMake. Thus, in order to change these flags, you have to add a `CMAKE_FLAG` responsible for compiler (and/or linker) options.

One can either specify a global build mode:
```
dunecontrol cmake -DCMAKE_BUILD_TYPE=[Debug|MinSizeRel|RelWithDebInfo|Release]
```

(see [CMake documentation](https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html) for a more detailed description of the CMake option)

or you can specify compiler flags explicitly, e.g.,
```
dunecontrol cmake -DCMAKE_CXX_FLAGS="-O3 -DNDEBUG -march=native"
```

This is equivalent to specifying both, the release more and the additional `-march=native` flag:
```
dunecontrol cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-march=native"
```

Both flags can be put into an options file, see below, or can be set by specifying the `CMAKE_FLAGS` environmental variable:
```
CMAKE_FLAGS='-DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-march=native"' dunecontrol cmake
```

Note, if you do not specify anything, the CMake defaults are used which essentially means: no code optimization.

### Speeding up the build process

Building several DUNE modules can be quiet time consuming. Using a concurrent build can speed this up. Either you can instruct `make` to use multiple threads when building,

```
dunecontrol make -jN
```

or you can specify another make-system, like [Ninja build](https://ninja-build.org), by setting a different CMake generator:

```
dunecontrol cmake -GNinja
dunecontrol make
```

If parts of the code must be rebuild multiple times, a utility like [ccache](https://ccache.dev) might be useful and can be specified as a `COMPILER_LAUNCHER` in CMake:
```
dunecontrol cmake -DCMAKE_CXX_COMPILER_LAUNCHER=/path/to/ccache
```

(see [CMake documentation](https://cmake.org/cmake/help/latest/envvar/CMAKE_LANG_COMPILER_LAUNCHER.html) for a more detailed description of the CMake option)

### Storing flags in an options file

As it is often not convenient to specify the desired options after the `dunecontrol` call, one can pass the options via file specified by the `--opts` option:
```
dunecontrol --opts=<file> COMMAND
```

Possible options for the options-file (or the environment) are

* `CMAKE_FLAGS`: arguments passed to the `cmake` command
* `MAKE_FLAGS`: arguments passed to the `make` command
* `BUILDDIR`: build directory for an out-of-source build. default: `build-cmake`
* `CMAKE`: executable to use for `cmake`
* `DUNE_CONTROL_PATH`: A PATH variable to locate dune modules in its subdirectories, directories are separated by colon `:`

An example of an options file is
```
# set build mode and compiler flags,
# install to a custom directory,
# disable the MPI library,
# add a search path for external libraries
# and use Ninja-build instead of make as the build-tool
CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release \
             -DCMAKE_CXX_FLAGS=\"-march=native\" \
             -DCMAKE_INSTALL_PREFIX=/path/where/to/install/dune \
             -DCMAKE_DISABLE_FIND_PACKAGE_MPI=1 \
             -DCMAKE_PREFIX_PATH=\"/path/to/lib1;/path/to/lib2\" \
             -GNinja"
```


## Details about the `dunecontrol` utility

The general structure of a `dunecontrol` call is
```
dunecontrol [OPTIONS] COMMAND
```

### Commands
The following atomic `COMMAND`s can be executed:

* `cmake`: run the CMake system for each module and passes along the directories of the found dune modules
* `make`: run make (or any other build tool that was configured) for each module
* `all`: combination of `cmake` and `make`
* `exec`: execute a command in each module directory
* `bexec`: execute a command in each module's build directory
* `update`: pull the latest version from the Git repository
* `git`: run a specific git command in all module directories

### Options
The following general `OPTIONS` are available:

* `--help`: Show a detailed list of all options with description.
* `--module=<mod>`, `--only=<mod>`, `--current`, `--current-dep`: Control which modules to build, see above.
* `--builddir=<dir>`: Make out-of-source builds in a subdir `<dir>`. This directory is created inside each module. If `<dir>` is an absolute path, the build directory is set to `<dir>/module-name` for each module.
* `--opts=<file>`: load default options from `<file>`


## Creating your own DUNE project module
You can create your own dune project module by using the `duneproject` script available in `dune-common/bin` directory. Running the script will create a directory with supporting files (`CMakeLists.txt` etc.) and a sample .cc file. After creating the module you can build this as explained above under "Building a specific DUNE module".

Note that the [Dune book](/doc/book/)
contains a [chapter about the build system](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf).
For further functionalities of the build system as a developer of a module check out the [build system documentation for developers](/dev/buildsystem/).


## CMake Superbuilds

Since version 2.10, there exists an **experimental** support for super-builds. In this form, a single cmake (super) project
contains other modules as sub-directories, hence, avoiding the need of handling the modules individually or
via other scripts like `dunecontrol`. This can be achieved by simply adding the respective dune module as
sub-directory and by using the provided targets.

```cmake
cmake_minimum_required(VERSION 3.24 FATAL_ERROR)
project(dune-super-build CXX)

# allow dune to see imported targets across modules
set(CMAKE_FIND_PACKAGE_TARGETS_GLOBAL ON)

# let 'find_package' know where modules can be found once configured
list(APPEND CMAKE_PREFIX_PATH ${CMAKE_CURRENT_BINARY_DIR})

# (>=2.10) include DUNE modules as sub-directories
add_subdirectory(dune-common)
add_subdirectory(dune-geometry)
add_subdirectory(dune-grid)

# (>=2.11) use targets from dune modules on cmake targets (e.g. from dune-grid)
add_executable(example example.cc)
target_link_libraries(example PRIVATE Dune::Grid)
```

For example, the snippet uses the [core modules](/groups/core/) as sub-directories of a single project.
In addition, future versions (>=2.11) will allow to compile a simple `example.cc` program using the CMake target from the `dune-grid` module, `Dune::Grid`.
Equivalent targets exists for the other core modules: `Dune::Common`, `Dune::ISTL`, `Dune::Geometry`, and `Dune::LocalFunctions`.
Once the cmake superbuild script is ready, configuring and building the project is as simple as if you were operating in a single
monolithic project:

```bash
# configure super project in the directory 'build'
cmake -DCMAKE_BUILD_TYPE=Release -S . -B build
# build all binaries
cmake --build build --config Release
```

As this is an experimental feature, we would highly appretiate any feedback via the
[mailing list](/community/mailinglists/) or bug reports on the [issue trackers](/dev/issues/).


## Installing DUNE in HPC Clusters
Cluster architectures often have a linux based system that is quiet different from a desktop environment, with its own package management system that needs special handling of the installation such that everything is found properly. However, often it is enough to just load the (build) dependencies and build DUNE using `dunecontrol` the classical way as described above. There are also some *non-official* ways of building DUNE in HPC environments, e.g.

- There is a DUNE package for the [Spack](https://spack.io) package management system, see https://gitlab.dune-project.org/spack/dune-spack for more information
- Installing DUNE in a system using [EasyBuild](https://easybuilders.github.io/easybuild) is available for the
   [DUNE core modules](https://docs.easybuild.io/en/latest/version-specific/Supported_software.html#list-software-dune-core-898) and [DUNE-Fem and dependencies](https://docs.easybuild.io/en/latest/version-specific/Supported_software.html#list-software-dune-fem-785).
  There are also other examples how to write the required description files for the software management system, see
  https://gitlab.mn.tu-dresden.de/spraetor/dune-easybuild for further examples.

Warning: those descriptions and tools are often provided just for a single or a few DUNE releases and are sometimes outdated.
