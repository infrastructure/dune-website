+++
title = "Using Dune FEM and its Python bindings"
[menu.main]
parent = "gettingstarted"
+++

There are a number of ways to use DUNE and it's Python bindings.
The easiest way is to install the required Dune packages from the Python
package index `PyPi` into a virtual environment using `pip`.
This requires at least a working Python and C++ installation on the host
system. For more advanced users the installation from source is recommended.

The pip installation is a straightforward as typing
```
pip install dune-fem
```

## Other options for installation

See the [installation instructions](/sphinx/content/sphinx/dune-fem/installation.html)
which covers a range of installation methods.

## Tutorial

To get started, have a look at the detailed
[tutorial with many examples](/sphinx/content/sphinx/dune-fem).
