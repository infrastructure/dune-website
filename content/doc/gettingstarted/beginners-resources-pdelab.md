+++
title = "Installation of Dune/PDELab (with tutorial) from Source on a Debian-based Linux System for Beginners"
[menu.main]
parent = "gettingstarted"
+++

Here we demonstrate how to install the current stable release 2.9
of  [DUNE](/about/dune/), [PDELab](/modules/dune-pdelab/) and the
[PDELab tutorial](/modules/dune-pdelab-tutorials/) from source on Ubuntu 20.04
LTS. This is the way to go if you want to have access to all of Dune's
source files (e.g. because you want to inspect or modify them) and to
have more control over the build process (e.g. change compiler flags or
enable/disable certain libraries). Consequently this way of
installation is more complex than installation from binary
packages. We will simplify life by providing shell scripts that do all
the work.

This installation includes the module dune-pdelab-tutorial providing a
tutorial for PDELab. Click [here](/modules/dune-pdelab/) for an
overview of the tutorial.

We assume that you are running a freshly installed Ubuntu 20.04 LTS
with no additional packages installed yet. In order to install and run
Dune you first need to install the following packages (for this to
work you need sudo rights):

```
sudo apt install git
sudo apt install cmake
sudo apt install mpich mpich-doc libmpich-dev
sudo apt install libsuitesparse-dev
sudo apt install libgmp-dev
sudo apt install zlib1g zlib1g-dev
sudo apt install texlive
sudo apt install texlive-science
sudo apt install texlive-latex-extra
sudo apt install texlive-bibtex-extra
sudo apt install texlive-math-extra
sudo apt install texlive-fonts-extra
sudo apt install paraview
sudo apt install gmsh
```

In order to create a Dune source installation the following steps are
necessary:

1. Download and install any necessary external libraries such as UG, HDF5 or FFTW
2. Download all dune modules from Dune's git server
3. Create an appropriate opts-file giving compiler name, compiler
options, location of the installed external libraries, etc.
4. Run Dune's build system in all the modules using `dunecontrol`.

In order to automate these steps we provide installer scripts. Just
check out the following repository

```
git clone https://conan.iwr.uni-heidelberg.de/git/peter/dune-installer.git
```

run the installer:

```
./dune-installer/pdelab-2.9/installer.sh my-dune-dir
```

Here `my-dune-dir` is the name of a new directory where you want your
Dune installation in. This will download and install the UG grid
manager, download all Dune modules with PDELab including its tutorial and
create an opts-file and a build-script.

Now run the build-script:

```
cd my-dune-dir
./buildmodules.sh
```

If this does not work for you then you need to edit the installer
script in `dune-installer/pdelab-2.9/installer.sh` and ask some
expert (e.g on the mailing list).

If all went well, where are the compiled files? The build-script sets
up things for a so-called _out-of-source_ build. The command

```
cd release-build
```

gets you to a directory that mirrors all dune-modules but with the
build files in it (the build files contain the executables ready to be run
whereas source files contain "just" the instructions of how to create
those executables).

A good way to proceed from here is to look at the PDELab tutorial:

```
cd dune-pdelab-tutorials
```


[Click here for more information about the PDELab tutorial](/modules/dune-pdelab-tutorials/)
