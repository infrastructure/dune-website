+++
title = "Getting started"
toc = 1
+++

{{< rightpanel >}}
#### More info
... can be be found in the [Dune book](/doc/book/) by Oliver Sander.

<a href="/doc/book/">
<img src="/img/dune-book-cover.png" width="50%" alt="Dune book cover" />
</a>

The chapter on
*How to get started with the C++ code*
[can be read online](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf).

Videos on different aspects of DUNE are available [online](https://dune-pdelab-course.readthedocs.io/).
{{< /rightpanel >}}

*You want to install and use Dune on your machine and you are a complete
novice? Then this is the right place to start!*

We will guide you through the installation process, and show you a first
example program.

## Installing Dune

It is important to note that installation of Dune requires a computer system
with a relatively recent operating system, either
Linux or Apple MacOS. Windows is not officially
supported, but installation can be done via virtualization, see [installation on Windows](/installation/installation-windows-wsl/).
There are a number of different ways how to install and use Dune on
your computer (*click on the links to follow the instructions*):

### Dune C++ binary packages

This is the most convenient way to get started if you do __not__ want to modify the Dune sources.

The details depend on the system you are using:

 * [Installation on Debian and Ubuntu systems](/installation/installation-deb/)
 * [Installation on Windows via WSL+Ubuntu](/installation/installation-windows-wsl/)

### Python packages via PyPI

[Installation of Dune from the Python Package Index (PyPi)](/installation/installation-pip/).
This is another very convenient way to install Dune which does not
require root access and works on a broad range of systems including
MacOS for which binary packages might not be available.

### Full installation from source

Sooner or later you will most likely want to use the latest Dune
features or even add new features to the Dune code modules. This will
require you to [build Dune from source](/installation/installation-buildsrc/).

## Running a first example

### Your first example with C++
Assume you have installed the Dune core modules. A simple example to test your installation is to
create a structured grid and print some information. Therefore, first create your own Dune project with
the command
```bash
duneproject my-first-example  dune-grid  0.1  my-first-example@dune-project.org
```
from within a directory where your Dune installation can be found (either the installed modules are in
subdirectories or installed in the system).
This will create a directory `my-first-example` for a Dune module with dependencies `dune-grid`. Put
the following code into the file `my-first-example/src/my-first-example.cc`:
```
#include <iostream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/printgrid.hh>
int main(int argc, char* argv[]) {
  auto& env = Dune::MPIHelper::instance(argc, argv);  // initialize the environment
  Dune::YaspGrid<2> grid({1.0,1.0}, {10,10});         // structured grid [0,1]x[0,1]
  std::cout << grid.size(0) << std::endl;             // print nr of elements
  Dune::printGrid(grid, env, "my_yaspgrid");          // print grid as png (requires `gnuplot`)
}

```
The code can be compiled and run with `dunecontrol`. From the folder containing the directory
`my-first-example` run the command
```bash
dunecontrol --module=my-first-example all
```
and execute `my-first-example/build-cmake/src/my-first-example`.

You can find a more detailed description of these first steps with some background in the
[first chapter](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf)
of Oliver Sander's [Dune book](/doc/book/).
It describes installation from binary packages,
installation from the source repository, how to create your own Dune module, and also how to
solve a simple PDE with the Dune core modules.


### Your first example with Python

You can test the installation
by constructing a simple structured grid. Call `python` and then execute
```
from dune.grid import structuredGrid
grid = structuredGrid([0,0],[1,1],[10,10])
print( grid.size(0) ) # should return 100
grid.plot() # requires the installation of `matplotlib`
```

For a more detailed description of the Python bindings take a look at the
section on the [DUNE grid interface](/sphinx/content/sphinx/dune-fem/dune-corepy_nb.html) as part of the [dune-fem tutorial](/sphinx/content/sphinx/dune-fem/).

## Working with further Dune modules

### [dune-pdelab](/modules/dune-pdelab/)
[Instructions for installing from the source.](/doc/gettingstarted/beginners-resources-pdelab/) and using the [tutorials](/modules/dune-pdelab-tutorials/).

Recordings of the virtual DUNE/PDELab course 2021 are available for self-study. The [material can be found here](https://dune-pdelab-course.readthedocs.io/).

### [dune-fem](/modules/dune-fem/)
[Detailed instructions on how to install dune-fem  and its Python bindings using `pip`.](/doc/gettingstarted/beginners-resources-dunefem/).
