+++
title = "The DUNE cheat sheet"
[menu.main]
parent = "docs"
identifier = "cheatsheet"
weight = 0
+++

Here is an (incomplete) yet compact
[DUNE Cheat Sheet](/pdf/dune-cheat-sheet.pdf).  It is meant for people who
more-or-less know DUNE but may need a little reminder and
do not want to work their way through all the details of the
[Doxygen documentation](/doxygen/) or
[The Book](https://link.springer.com/book/10.1007/978-3-030-59702-3).
Core only, no MPI, no adaptivity.

You can contribute [here](https://gitlab.dune-project.org/joe/dune-cheat-sheet).


