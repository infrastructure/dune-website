+++
# Our index.html template looks for this file!
home = 1
title = "What is Dune?"
[menu.main]
parent = "about"
weight = 1
+++

### DUNE
DUNE, the Distributed and Unified Numerics Environment is a modular
toolbox for solving partial differential equations (PDEs) with
grid-based methods. It supports the easy, flexible, and efficient
implementation of finite element and finite volume methods.
DUNE is written in C++, but has interfaces to Python as well.

<img src= "/img/dunedesign.png" class="img-responsive pull-right">

The underlying idea of DUNE is to create slim interfaces allowing an
efficient use of legacy and/or new libraries. Modern C++ programming
techniques enable very different implementations of the same concept
using a common interface at a very low
overhead. Thus DUNE ensures efficiency in scientific computations and
supports high-performance computing applications.

Particular highlights are:

 * A generic interface for finite element grids.  This interface allows
   to use a wide range of very different [grid data structures](/doc/grids/)
   from the same code
 * An interface for [finite element bases](/modules/dune-functions/,
   together with a generic mechanism to construct complicated bases
   from simpler ones
 * The [Iterative Solver Template Library](/modules/dune-istl/),
   featuring vector and matrix data structures that allow for many
   general nesting patterns

DUNE is free software licensed under the GPL (version 2) with a so
called "runtime exception" (see [**license**](/about/license/
"License")). This licence is similar to the one under which the
**libstdc++** libraries are distributed. Thus it is possible to use
DUNE even in proprietary software.


<!--more-->

#### DUNE is based on the following main principles:

* Separation of data structures and algorithms by abstract interfaces.
_This provides more functionality with less code and also ensures maintainability and extendability of the framework._

* Efficient implementation of these interfaces using generic programming techniques.
_Static polymorphism allows the compiler to do more optimizations, in particular function inlining, which in turn allows the interface to have very small functions (implemented by one or few machine instructions) without a severe performance penalty. In essence the algorithms are parametrized with a particular data structure and the interface is removed at compile time. Thus the resulting code is as efficient as if it would have been written for the special case._

* Reuse of existing finite element packages with a large body of functionality.
_In particular the finite element codes UG, ALBERTA, and ALUGrid have been adapted to the DUNE framework. Thus, parallel and adaptive meshes with multiple element types and refinement rules are available. All these packages can be linked together in one executable._


### Modules

The framework consists of a number of modules which are downloadable as separate packages. There is a set of
[core modules](/groups/core/) which are used by most other packages. The [dune-grid](/modules/dune-grid/) core module
already contains some grid implementation and further [grid managers](/groups/grid/) are available as extra modules.
Main [discretization modules](/groups/disc/) providing the infrastructure for solving partial differential equations
using DUNE are available as separate modules. The modular structure of DUNE allows to only use a small set of th
 modules (e.g., only the solver module [dune-istl](/modules/dune-istl/) or the module [dune-localfunctions](/modules/dune-localfunctions/)
 containing shape functions without for example using the [dune-grid](/modules/dune-grid/) or a full discretization module).

For further information, have a look at the main [features](/about/features/), read the [documentation for application writers](/groups/tutorial/) or get in touch with the [people actively developing DUNE](/community/people/).
