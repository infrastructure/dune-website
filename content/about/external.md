+++
group = "external"
title = "Frameworks using Dune"
[menu.main]
parent = "about"
weight = 9
+++

### Highlevel Applications & Simulation Frameworks

These are actually not Dune modules, but general software packages that use Dune in one way or another for infrastructure.

* [DuMu<sup>x</sup>](http://www.dumux.org/) is a DUNE based simulator for flow and transport processes in porous media.
* [Kaskade 7](http://www.zib.de/en/numerik/software/kaskade-7.html) uses Dune for the grid and linear algebra infrastructure.
* The [Open Porous Media Simulator (OPM)](http://opm-project.org/) initiative has the goal to develop a DUNE based simulation suite that is capable of modeling industrially and scientifically relevant flow and transport processes in porous media.
* [BEM++](http://www.bempp.org/) is an open source C++/Python library for boundary element methods.
