+++
title = "Publications and Talks"
[menu.main]
parent = "about"
weight = 6
+++

## Publications and Talks

### Main Publications
#### How to cite the DUNE core modules

If you used the DUNE core modules for your publication, we kindly ask you to cite the following
peer reviewed papers ([bibtex entries](/publications/dune-references.bib)). The latest overview
article reflects on recent developments and general changes that happened since the release
of the [first Dune version in 2007](https://doi.org/10.1007/s00607-008-0003-x):

* P. Bastian, M. Blatt, A. Dedner, N.-A. Dreier, C. Engwer, R. Fritze, C. Gräser, C. Grüninger,
  D. Kempf, R. Klöfkorn, M. Ohlberger, O. Sander.
  The DUNE framework: Basic concepts and recent developments. Computers & Mathematics with Applications
  81, 2021, pp. 75-112. [pdf](https://doi.org/10.1016/j.camwa.2020.06.007)

#### dune-grid

* P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, M. Ohlberger, O. Sander. A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part I: Abstract Framework. Computing, 82(2-3), 2008, pp. 103-119. [Preprint](http://nbn-resolving.de/urn:nbn:de:0296-matheon-4162)
* P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, R. Kornhuber, M. Ohlberger, O. Sander. A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part II: Implementation and Tests in DUNE. Computing, 82(2-3), 2008, pp. 121-138. [Preprint](http://nbn-resolving.de/urn:nbn:de:0296-matheon-4176)

#### dune-istl

* M. Blatt, P. Bastian. The Iterative Solver Template Library. In B. Kåström, E. Elmroth, J. Dongarra and J. Wasniewski, Applied Parallel Computing. State of the Art in Scientific Computing. Volume 4699 of Lecture Notes in Scientific Computing, pages 666-675. Springer, 2007. [PDF-Preprint (149K)](/pdf/publications/istl_para06.pdf)
* P. Bastian, M. Blatt. On the Generic Parallelisation of Iterative Solvers for the Finite Element Method In Int. J. Computational Science and Engineering,4(1):56-69, 2008. [PDF - Preprint (229K)](/pdf/publications/parallel_istl_paper_ijcse.pdf)

#### dune-fem

* A. Dedner, R. Klöfkorn, M. Nolte, M. Ohlberger. A generic interface for parallel and adaptive scientific computing: Abstraction principles and the DUNE-FEM module. Computing Vol. 90, No. 3, pp. 165--196, 2011 [Preprint](/pdf/publications/dkno_dunefempreprint.pdf).

### DUNE book

The DUNE book describes the DUNE core modules, dune-typetree, dune-functions, and PDELab. It serves as a good
introduction and covers these modules in depth for release 2.7.

* O. Sander. <a href="https://www.springer.com/gp/book/9783030597016">DUNE — The Distributed and
  Unified Numerics Environment</a>. ISBN 978-3-030-59701-6. Springer, 2020.

<a href="https://www.springer.com/gp/book/9783030597016">
<img src="/img/dune-book-cover.png" width="200" alt="Dune book cover"/>
</a>

### Other publications

* Peter Bastian, Mark Droske, Christian Engwer, Robert Klöfkorn, Thimo Neubauer, Mario Ohlberger, Martin Rumpf. Towards a Unified Framework for Scientific Computing. In Proc. of the 15th International Conference on Domain Decomposition Methods (2005), [PDF 376K](/pdf/publications/TM105-frame.pdf)
* Adrian Burri, Andreas Dedner, Robert Klöfkorn, Mario Ohlberger. An efficient implementation of an adaptive and parallel grid in DUNE. In Proc. of the 2nd Russian-German Advanced Research Workshop on Computational Science and High Performance Computing (2005), [PDF 1.8M](/pdf/publications/paper_bdko.pdf)
* Adrian Burri, Andreas Dedner, Dennis Diehl, Robert Klöfkorn, Mario Ohlberger. A general object oriented framework for discretizing nonlinear evolution equations. In: Proceedings of The 1st Kazakh-German Advanced Research Workshop on Computational Science and High Performance Computing, Almaty, Kazakhstan, September 25 - October 1, 2005., [PDF 0.4M](http://wwwmath1.uni-muenster.de/u/ohlberger/postscript/paper_bddko.pdf)
* Peter Bastian, Markus Blatt, Christian Engwer, Andreas Dedner, Robert Klöfkorn, Sreejith P. Kuttanikkad, Mario Ohlberger, Oliver Sander. The Distributed and Unified Numerics Environment (DUNE). In Proc. of the 19th Symposium on Simulation Technique in Hannover, September 12 - 14, 2006 [PDF 3,5M](/pdf/publications/dune_asim2006.pdf)
* C. Gräser, O. Sander. The dune-subgrid Module and Some Applications. Computing, 8(4), 2009, pp. 269-290. [Preprint](https://opus4.kobv.de/opus4-matheon/files/576/5868_graeser_sander_subgrid.pdf)
* P. Bastian, G. Buse, O. Sander Infrastructure for the Coupling of Dune Grids.  In 'Proceedings of ENUMATH 2009' Springer, 2010, pp. 107-114  [PDF](https://dx.doi.org/10.1007/978-3-642-11795-4_10)
* M. Blatt, P. Bastian C++ components describing parallel domain decomposition and communication. International Journal of Parallel, Emergent and Distributed Systems, 24(6), 2009, pp. 467 - 477.
* M. Blatt, A. Burchardt, A. Dedner, C. Engwer, J. Fahlke, B. Flemisch, C. Gersbacher, C. Gräser,
  F. Gruber, C. Grüninger, D. Kempf, R. Klöfkorn, T. Malkmus, S. Müthing, M. Nolte,
  M. Piatkowski, O. Sander.
  The Distributed and Unified Numerics Environment, Version 2.4.
  Archive of Numerical Software, 4(100), 2016, pp 13 – 29, DOI: 10.11588/ans.2016.100.26526.
  [PDF](https://journals.ub.uni-heidelberg.de/index.php/ans/article/download/26526/24049)

## Talks

* Peter Bastian DUNE - The Distributed Unified Numerics Environment. At Oberseminar Numerical Analysis and Scientific Computing (FU Berlin) (July 1th 2005) [PDF 762K](/pdf/publications/berlinJul2005.pdf).
* Christian Engwer. DUNE - Distributed and Unified Numerics Environment. At SIAM Parallel Processing 06 (San Francisco) (February 2006). [PDF 1.4M](/pdf/publications/SIAM-PP06.pdf).
* Markus Blatt. ISTL - Iterative Solver Template Library. At International Conference on High Performance Computing (Hanoi/Vietnam) (March 2006). [PDF (0.8M)](/pdf/publications/istl_hpsc2006.pdf)
* Christian Engwer. Introduction to the Dune autobuild system. At Oberseminar SGS (Universität Stuttgart) (January 2007). [PDF 324K](/pdf/publications/dune_autobuild.pdf).
* Oliver Sander. The Distributed and Unified Numerics Environment (DUNE). At TU Dresden (January 2008). [PDF 4.3M](/pdf/publications/dune_dresden_08.pdf).
* Christian Engwer. The Distributed and Unified Numerics Environment. At Matheon-Workshop on Data Structures for Finite Element and Finite Volume Computations, FU Berlin (February 2008). [PDF 424K](/pdf/publications/matheon-workshop-08.pdf).
* Andreas Dedner The Distributed and Unified Numerics Environment (DUNE). At the opening workshop for the BEM++ project, UC London, UK (Juli 2011). [PDF 2.2M](/pdf/publications/dednerBEM.pdf).
* Andreas Dedner Construction of Higher Order Finite-Element Spaces. At the 24th Chemnitz FEM Symposium (September 2011). [PDF 1.1M](/pdf/publications/dednerChemnitz.pdf).

There are also quite a few DUNE related talks given at the [Dune User Meetings](/community/meetings/).
