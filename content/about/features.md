+++
title = "Features"
[menu.main]
parent = "about"
weight = 2
+++

### DUNE Features
The following list gives a short overview over the main features of DUNE and refers you to more detailed documentation.

### Grid Implementations
So far seven grid implementations are available through the DUNE grid interface. Each is geared towards a different purpose and thus has a different [set of features](/doc/grids/). Further grid managers can be obtained by downloading [additional modules](/groups/grid/).

So far seven grid implementations are available through the DUNE grid interface. Each is geared towards a different purpose and thus has a different [set of features](/doc/grids/). 

 * [`AlbertaGrid`](https://www.dune-project.org/doxygen/master/group__AlbertaGrid.html): The grid manager of the Alberta toolbox
 * [`ALUConformGrid`, `ALUCubeGrid`, and `ALUSimplexGrid`](/modules/dune-alugrid/): A family of grids in two and three space dimension based on the ALUGrid library.
 * [`GeometryGrid`](https://www.dune-project.org/doxygen/master/group__GeoGrid.html): A metagrid exchanging the host grid's geometry
 * [`OneDGrid`](https://www.dune-project.org/doxygen/master/group__OneDGrid.html): A sequential locally adaptive grid in one space dimension
 * [`UGGrid`](/modules/dune-uggrid/): The grid manager of the UG toolbox
 * [`YaspGrid`](https://www.dune-project.org/doxygen/master/group__YaspGrid.html): A structured parallel grid in n space dimensions

### Linear Algebra

DUNE contains ISTL (the Iterative Solver Template Library) for powerful (parallel) linear algebra. The main features are:
 * Abstractions for block matrices (e.g. compressed row storage `BCRSMatrix`, block diagonal matrices `DiagonalMatrix` and block vectors `BlockVector`, see [doxygen documentation](/doxygen/))
 * Block structure arbitrarily nestable
 * High performance through generic programming
 * Expression templates for BLAS1 routines
 * Several standard solvers (Krylov methods and stationary iterative methods)
 * Highly scalable parallel algebraic multigrid method available as preconditioner.

### Quadrature Formulas

 * Quadrature rules for all common element types
 * Rules for hypercubes up to order 19, for simplices up to order 12
 * Easy access

### Shape Functions

 * Lagrangian shape functions of arbitrary order
 * Monomial shape functions of arbitrary order for Discontinuous Galerkin methods
 * Orthonormal shape functions of arbitrary order
 * Raviart-Thomas shape functions of lowest order for cube and of arbitrary order for simplex elements

### Input/Output

 * Reading grid files in the [gmsh](http://www.geuz.org/gmsh/) format
 * Reading grid files in the grid independent Dune grid format [DGF](https://www.dune-project.org/doxygen/master/group__DuneGridFormatParser.html)
 * Reading simplex grids through DGF constructed using the tools [Tetgen](http://tetgen.berlios.de/) and [Triangle](http://www.cs.cmu.edu/~quake/triangle.html)
 * Reading and writing in the AmiraMesh format
 * Subsampling of high-order functions
 * Write grids and data in the format of the visualization toolkit (vtk)
 * Visualization using [GRAPE](http://www.mathematik.uni-freiburg.de/IAM/Research/grape/GENERAL/)
 * Output in Data Explorer format

### Discretization Module

There are some [discretization modules](/groups/disc/) available which provide
 * Arbitrary order lagrange spaces for different grid structures
 * Discontinuous Galerkin spaces of arbitrary order both with orthonormal and Lagrange basis function
 * Further discrete function spaces include Raviart-Thomas, Nedelec, and others
 * Linear and non-linear solvers
 * Time discretization method, e.g., explicit, implicit, and IMEX Runge-Kutta methods or multistep methods
 * Support for parallelization and adaptivity (both h and p refinement)
