+++
title = "DuneCopasi: Solver for reaction-diffusion systems in multiple compartments"
content = "carousel"
image = ["/img/dune-copasi-cardiac-ep.png", "/img/dune-copasi-yap-taz-liver.png"]
+++

[DuneCopasi](/modules/dune-copasi/) within a computational electrophysiology pipeline allowing to
solve the electrophysiological mono-domain equations with Mitchell-Schaefer cell model using a
medical imaging derived from biventricular geometry (left) and mathematical modelling predicting that
nuclear phosphorylation controls spatial localization of Hippo signalling pathway components YAP and TAZ (right)
[(Ospina De Los Ríos et al. 2024)](https://doi.org/10.21105/joss.06836).

