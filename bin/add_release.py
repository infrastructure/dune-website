#!/usr/bin/python

import argparse
import os
import subprocess


def expand_version(ver):
    # Expand a version string to be of the form major.minor.patch
    spl = ver.split('.')
    assert(len(spl) <= 3)
    for i in spl:
        try:
            int(i)
        except ValueError:
            raise ValueError('Version numbers must consist of numbers only, separated by a dot')

    return "{}.{}.{}".format(spl[0], spl[1] if len(spl) > 1 else 0, spl[2] if len(spl) > 2 else 0)


def get_arg(arg=None):
    # Define command line arguments for this script
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', help='The version number for the newly created release', required=True)
    parser.add_argument('-s', '--source', help='Path to the sources for the release (a directory that contains a all dune modules)', required=True)
    parser.add_argument('which', metavar='module', nargs='*', help='The list of dune modules to build. If omitted and --core not set, build all available')
    parser.add_argument('-c', '--core', action='store_true', help='Whether this is a core module release, superseded by --which')
    parser.add_argument('-H', '--homepage', default='.', help='The root directory of the hugo project. If omitted, assumed to be .')
    parser.add_argument('-k', '--key', help='The GPG key to sign the release tarballs with')
    v = vars(parser.parse_args())

    # Apply defaults
    if len(v['which']) == 0 and v['core']:
        v['which'] = ['dune-common', 'dune-istl', 'dune-geometry', 'dune-grid', 'dune-localfunctions']

    if len(v['which']) == 0:
        raise ValueError("No modules selected for release! Check command line arguments")

    v['version'] = expand_version(v['version'])

    if arg is None:
        return v
    else:
        return v[arg]


def generate_tarball(module, args, compression_flag=''):
    compression_ext = {'': '', 'z': '.gz'}[compression_flag]
    sp = os.path.join(args['source'], module)
    assert(os.path.isdir(sp))
    ap = os.path.join(args['homepage'], 'static', 'download', args['version'])
    if not os.path.isdir(ap):
        os.makedirs(ap)
    ap = os.path.join(ap, '{}-{}.tar{}'.format(module, args['version'], compression_ext))
    ret = subprocess.call('git tag -v v{}'.format(args['version']).split(), cwd=sp)
    if ret>0:
        if args['key']:
            ret = subprocess.call(['git', 'tag', '-s', '-m', 'Release {}'.format(args['version']), '-u', args['key'], 'v{}'.format(args['version'])], cwd=sp)
        else:
            ret = subprocess.call(['git', 'tag', '-s', '-m', 'Release {}'.format(args['version']), 'v{}'.format(args['version'])], cwd=sp)
        assert(ret==0)

    commit = 'v{}'.format(args['version'])
    ret = subprocess.call('git archive --format=tar.gz -o {} --prefix={}-{}/ {}'.format(os.path.abspath(ap), module, args['version'], commit).split(), cwd=sp)
    assert(ret == 0)
    if args['key']:
        ret = subprocess.call('gpg --batch --detach-sign --armor --default-key {} {}'.format(args['key'], ap).split())
    else:
        ret = subprocess.call('gpg --batch --detach-sign --armor {}'.format(ap).split())
    assert(ret == 0)



def package_release(module, args):
    generate_tarball(module, args, 'z')


def write_hugo_content(args):
    # First, create a new content page
    ret = subprocess.call('hugo new releases/{}.md'.format(args['version']).split())
    mdfile = os.path.join(args['homepage'], 'content', 'releases', '{}.md'.format(args['version']))

    # Now collect a dictionary of information to provide to the generation process
    args['majorversion'] = args['version'].split('.')[0]
    args['minorversion'] = args['version'].split('.')[1]
    args['patchversion'] = args['version'].split('.')[2]

    # And replace the placeholders
    lines = []
    f = open(mdfile, 'r')
    for line in f:
        lines.append(line.format(**args))
    f.close()
    f = open(mdfile, 'w')
    for line in lines:
        f.write(line)

    print("Hugo content written to {}. Add any information about this release that you might want to share to that file".format(mdfile))


if __name__ == '__main__':
    args=get_arg()
    # Trigger the actual creation of the release:
    for module in args['which']:
        package_release(module, args)

    # And write the hugo content for this release:
    write_hugo_content(args)
